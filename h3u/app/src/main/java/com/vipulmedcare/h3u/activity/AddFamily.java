package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.AddFamilyAdapter;
import com.vipulmedcare.h3u.base.BaseActivity;

import java.util.ArrayList;

/**
 * Created by vipul on 9/4/2017.
 */

public class AddFamily extends BaseActivity {
   private RecyclerView AddFamilyRv ;
   private AddFamilyAdapter addFamilyAdapter;
    private ArrayList<String> verticalList ;
    private Context mContext;
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.add_family);
        mContext=AddFamily.this;
        initViews();

    }

    public void  initViews(){
        verticalList=new ArrayList<>();
        verticalList.add("verticallist 1");
        verticalList.add("verticallist 2");
        verticalList.add("verticallist 3");
        AddFamilyRv = (RecyclerView) findViewById(R.id.rv_family);
        addFamilyAdapter = new AddFamilyAdapter(verticalList,mContext);
        LinearLayoutManager verticalLayoutmanager
                = new LinearLayoutManager(AddFamily.this, LinearLayoutManager.VERTICAL, false);
        AddFamilyRv.setLayoutManager(verticalLayoutmanager);

        AddFamilyRv.setAdapter(addFamilyAdapter);


    }
}
