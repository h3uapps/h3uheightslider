package com.vipulmedcare.h3u.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.utils.Utils;

import java.text.DateFormatSymbols;
import java.util.Calendar;

/**
 * Created by vipul on 5/31/2017.
 */

public class Activity_process extends AppCompatActivity implements View.OnClickListener {
    Button bt_process;
    TextView tv_date;
    private int cStartMonth, cStartDay, cStartYear;
    LinearLayout lin_group;
    EditText edit_email;
    private Context mContext = Activity_process.this;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process);

       /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setTitle("Map");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);*/

        edit_email= (EditText) findViewById(R.id.edit_email);
        lin_group= (LinearLayout) findViewById(R.id.lin_group);
        tv_date= (TextView) findViewById(R.id.tv_date);
        edit_email.setOnClickListener(this);
        tv_date.setOnClickListener(this);
        bt_process= (Button) findViewById(R.id.bt_process);
        bt_process.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_process:
                Intent mIntent = new Intent(Activity_process.this, Activity_Map.class);
                startActivity(mIntent);
               // Toast.makeText(getApplication(),"Click",Toast.LENGTH_SHORT).show();
               /* if (checkValidation()) {
                    Intent mIntent = new Intent(Activity_Otp.this, Activity_process.class);
                    startActivity(mIntent);
                }*/
                break;
            case R.id.tv_date:
                final Calendar c = Calendar.getInstance();
                cStartYear = c.get(Calendar.YEAR);
                cStartMonth = c.get(Calendar.MONTH);
                cStartDay = c.get(Calendar.DAY_OF_MONTH);

                int mStartYear = cStartYear - 18;
                int mStartMonth = cStartMonth;
                int mStartDay = cStartDay;
                Calendar calendar = Calendar.getInstance();
                calendar.set(mStartYear, mStartMonth, mStartDay, 0, 0, 0);

                //For Maximum Date Picker Logic
                int maxStartYear = cStartYear - 18;
                int maxStartMonth = cStartMonth;
                int maxStartDay = cStartDay;
                Calendar calendar1 = Calendar.getInstance();
                calendar1.set(maxStartYear, maxStartMonth, maxStartDay, 0, 0, 0);
                DatePickerDialog _date;
//                _date = new DatePickerDialog(mContext, datePickerListener, cStartYear, cStartMonth, cStartDay);

                _date = new DatePickerDialog(mContext, datePickerListener, maxStartYear, maxStartMonth, maxStartDay);
                _date.getDatePicker().setMaxDate(calendar1.getTimeInMillis());
                _date.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            //et_to_date.setText("");
                            tv_date.setText("");
                        }
                    }
                });
                _date.show();
                break;

        }

    }
    private final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            tv_date.setText(new StringBuilder().append(Utils.checkDigit(selectedDay)).append("-")
                    .append(new DateFormatSymbols().getShortMonths()[selectedMonth]).append("-").append(selectedYear));
        }
    };

}
