package com.vipulmedcare.h3u.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.vipulmedcare.h3u.activity.HealthCorporate;
import com.vipulmedcare.h3u.activity.HealthPersonal;


/**
 * Created by vipul on 8/24/2017.
 */

public class HealthTabsPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 2;
    private String tabTitles[] = new String[] { "CORPORATE", "PERSONAL"};
    private Context context;

    public HealthTabsPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                // Top Rated fragment activity
                return new HealthCorporate();
            case 1:
                // Games fragment activity
                return new HealthPersonal();

        }

        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return PAGE_COUNT;
    }

    public CharSequence getPageTitle(int position) {
        // Generate title based on item position

        return tabTitles[position];
    }

}
