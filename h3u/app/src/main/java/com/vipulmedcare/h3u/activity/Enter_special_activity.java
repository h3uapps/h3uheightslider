package com.vipulmedcare.h3u.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.view.text.CustomTextView;


/**
 * Created by vipul on 8/3/2017.
 */

public class Enter_special_activity extends Activity implements View.OnClickListener {

    ImageView ent_spcl_cross,ent_spcl_back;
    private Context mContext;
    TextView tv_headertext,tv_hname,tv_honame;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_special_activity);

        mContext = Enter_special_activity.this;
        initViews();

    }

    public void initViews(){

        //    .......  heading .... ................        //
        tv_headertext =(TextView) findViewById(R.id.tv_headertext);
        tv_headertext.setText("OPD CONSULTATION");
        ent_spcl_cross = (ImageView) findViewById(R.id.ent_spcl_cross);
        ent_spcl_back= (ImageView) findViewById(R.id.ent_spcl_back);

        tv_hname= (CustomTextView) findViewById(R.id.tv_hname);
        tv_honame= (CustomTextView) findViewById(R.id.tv_honame);
        ent_spcl_cross.setOnClickListener( this);
        ent_spcl_back.setOnClickListener( this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.ent_spcl_back:
               /* Intent intent1 = new Intent(mContext,OpdConsultActivity.class );
                startActivity(intent1);*/
                break;

            case R.id.ent_spcl_cross:
                finish();
                break;

          /*  case R.id.chs_spl_back:
                *//*Intent intent3 = new Intent(mContext,OpdConsultActivity.class );
                startActivity(intent3);*//*
                break;*/
        }

    }


}
