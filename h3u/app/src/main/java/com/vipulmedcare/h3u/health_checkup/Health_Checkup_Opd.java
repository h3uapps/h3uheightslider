package com.vipulmedcare.h3u.health_checkup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.activity.Activity_Choose_Healthcheckup;
import com.vipulmedcare.h3u.activity.Activity_Enter_TestName;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.view.text.CustomTextView;


/**
 * Created by Andrew pj on 8/23/2017.
 */

public class Health_Checkup_Opd extends BaseActivity implements View.OnClickListener {
ToggleButton tb_yes_checkup,tb_no_checkup;
    ImageView opd_checkup_cross;
    private Context mContext;
    TextView tv_headertext,tv_testname,tv_hname;
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.health_checkup_opd);
        mContext = Health_Checkup_Opd.this;
        initviews();

    }

    private void initviews() {

        tv_hname= (CustomTextView) findViewById(R.id.tv_hname);
        tv_testname= (CustomTextView) findViewById(R.id.tv_testname);



        opd_checkup_cross= (ImageView) findViewById(R.id.opd_checkup_cross);
        tb_yes_checkup = (ToggleButton) findViewById(R.id.tb_yes_checkup);
        tb_no_checkup= (ToggleButton) findViewById(R.id.tb_no_checkup);
        opd_checkup_cross.setOnClickListener(this);
        tb_yes_checkup.setOnClickListener(this);
        tb_no_checkup.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tb_no_checkup:

                tb_yes_checkup.setChecked(false);
                if(tb_no_checkup.isChecked())
                {
                    Intent in =new Intent(Health_Checkup_Opd.this,Activity_Choose_Healthcheckup.class);
                    startActivity(in);
                    Toast.makeText(Health_Checkup_Opd.this, "yes", Toast.LENGTH_LONG).show();
                }
                else
                {
                    tb_yes_checkup.setChecked(false);
                    Toast.makeText(Health_Checkup_Opd.this, "Un-Checked", Toast.LENGTH_LONG).show();
                }

                break;

                  /*  Intent i =new Intent(Health_Checkup_Opd.this, Activity_Choose_Healthcheckup.class);
                    startActivity(i);
                   // Toast.makeText(Health_Checkup_Opd.this, "yes", Toast.LENGTH_LONG).show();
                break;*/




            case R.id.tb_yes_checkup:

                tb_no_checkup.setChecked(false);
                if(tb_yes_checkup.isChecked())
                {
                    Intent in =new Intent(Health_Checkup_Opd.this,Activity_Enter_TestName.class);
                    startActivity(in);
                    Toast.makeText(Health_Checkup_Opd.this, "no suggest one", Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(Health_Checkup_Opd.this, "Un-Checked", Toast.LENGTH_LONG).show();
                }


                   /* Utils.showAlert(mContext, "Work in progress", "Work in progress");*/
                   /* Intent in =new Intent(Health_Checkup_Opd.this,Activity_Enter_TestName.class);
                    startActivity(in);

                break;*/

            case R.id.opd_checkup_cross:
                finish();
                break;
        }
    }
}
