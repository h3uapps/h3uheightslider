package com.vipulmedcare.h3u.view.text;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.vipulmedcare.h3u.model.Fonts;


/**
 * Created by vjangra on 07/12/15.
 */

public class CustomEditText extends EditText {

    public CustomEditText(Context context) {
        super(context);
        setFont();
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    protected void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), Fonts.TYPO_LATTO_REGULAR);
        setTypeface(font, Typeface.NORMAL);
    }
}


