package com.vipulmedcare.h3u.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;


/**
 * Created by ANdrew Pj on 9/4/2017.
 */

public class Personal_Profile extends android.support.v4.app.Fragment {
TextView text_edit;
    LinearLayout ll_basic,ll_identy;
    String userId;
    private Boolean ll_basicBoolean = true;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.personal_profile, container, false);

        text_edit= (TextView) rootView.findViewById(R.id.text_edit);
        ll_identy= (LinearLayout) rootView.findViewById(R.id.ll_identy);
        ll_basic= (LinearLayout) rootView.findViewById(R.id.ll_basic);


        text_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ll_basicBoolean){
                    ll_basic.setVisibility(View.GONE);
                    ll_basicBoolean = false;
                }else
                {
                    ll_basic.setVisibility(View.VISIBLE);
                    ll_basicBoolean = true;
                }



            }
        });
      /*  if(userId!=null){         //logged in
            ll_basic.setVisibility(View.GONE);

        }else{                     // new user
            ll_basic.setVisibility(View.VISIBLE);
            ll_identy.setVisibility(View.GONE);
        }*/
        return rootView;
    }
}