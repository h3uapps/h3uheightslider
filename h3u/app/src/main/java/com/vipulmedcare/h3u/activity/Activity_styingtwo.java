package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.registration.Activity_Selectgender;
import com.vipulmedcare.h3u.ui.ClickToSelectEditText;
import com.vipulmedcare.h3u.ui.myListItem;
import com.vipulmedcare.h3u.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 8/3/2017.
 */

public class Activity_styingtwo extends BaseActivity implements View.OnClickListener {
    private ClickToSelectEditText et_spin_city;
    String user_gender;
    private ArrayList<String> mcityList;
    private Context mContext;
    TextView tv_verify;
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_stayingtwo);

        mContext = Activity_styingtwo.this;
        initviews();

        mcityList = new ArrayList<String>();
        mcityList.add("Gurgaon");
        mcityList.add("Indore");
        mcityList.add("Bhoapl");

        List<myListItem> Genderlist = new ArrayList<>();
        Genderlist.clear();
        for (int i = 0; i < mcityList.size(); i++) {
            myListItem item = new myListItem();
            item.setName(mcityList.get(i).toString());
            Genderlist.add(item);
        }
        et_spin_city = (ClickToSelectEditText)findViewById(R.id.et_spin_city);
        et_spin_city.setText(user_gender);
        et_spin_city.setShowPopupOnClick(true);
        et_spin_city.setSelected(false);
        et_spin_city.setItems(Genderlist);

    }

    private void initviews() {

        tv_verify= (TextView) findViewById(R.id.tv_verify);
        tv_verify.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_verify:
                //if (checkValidation()) {
                    // signup();
                     Utils.showToast(mContext,"verify");
                    Intent I = new Intent(mContext,Activity_Selectgender.class);
                    startActivity(I);
               // }
                break;
        }
    }
    
}
