package com.vipulmedcare.h3u.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.utils.Validation;


/**
 * Created by vipul on 5/29/2017.
 */

public class Activity_Otp extends Activity implements View.OnClickListener {
    EditText et_otp;
    TextInputLayout til_otp;
    Button btn_veify;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("OTP");
        toolbar.setTitleTextColor(Color.WHITE);

        et_otp= (EditText) findViewById(R.id.et_otp);
        til_otp= (TextInputLayout) findViewById(R.id.til_otp);
        btn_veify= (Button) findViewById(R.id.btn_veify);
        btn_veify.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_veify:

                if (checkValidation()) {
                    Intent mIntent = new Intent(Activity_Otp.this, Activity_process.class);
                    startActivity(mIntent);
                    finish();
                }

                break;
        }
}
    private boolean checkValidation() {
        boolean result = true;

        if (!Validation.isNull(til_otp, et_otp, true))
            result = false;

        return result;
    }
}
