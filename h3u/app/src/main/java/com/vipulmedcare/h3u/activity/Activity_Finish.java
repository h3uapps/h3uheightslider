package com.vipulmedcare.h3u.activity;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;


/**
 * Created by vipul on 8/9/2017.
 */

public class Activity_Finish extends Activity {
    TextView tv_onetouch;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.finish_activity);

        tv_onetouch= (TextView) findViewById(R.id.tv_onetouch);
        Typeface m= Typeface.createFromAsset(getAssets(),"fonts/HelveticaLTStd-Bold.otf");
        tv_onetouch.setTypeface(m);
    }
}
