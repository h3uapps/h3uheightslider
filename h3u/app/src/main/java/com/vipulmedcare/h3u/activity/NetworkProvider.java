package com.vipulmedcare.h3u.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.view.text.CustomTextView;


/**
 * Created by vipul on 9/1/2017.
 */

public class NetworkProvider extends BaseActivity {

    private TextView dialogTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.network_provider);
        dialogTextView = (TextView) findViewById(R.id.dialogTextView);
        dialogTextView.setOnClickListener(clickListener);
    }
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showDialog();
        }
    };

    public void showDialog(){
        final Dialog dialog = new Dialog(NetworkProvider.this, android.R.style.DeviceDefault_Light_ButtonBar);
        dialog.setContentView(R.layout.full_screen_dialog_layout);
        dialog.getWindow().setBackgroundDrawableResource(R.color.dialog_transparent);
        CustomTextView change_city_dialog_close = (CustomTextView) dialog.findViewById(R.id.change_city_dialog_close);
        change_city_dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });





        dialog.show();
    }
}
