package com.vipulmedcare.h3u.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.pojo.listitem;
import com.vipulmedcare.h3u.view.text.CustomTextView;

import java.util.List;

/**
 * Created by Andrew on 8/23/2017.
 */

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyViewHolder> {

    private List<listitem> moviesList;
    Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_heading, tv_dob, tv_title,tv_share,tv_caseid,tv_name_opinion,tv_name_status,tv_response;


        public MyViewHolder(final View view) {
            super(view);

            tv_dob= (CustomTextView) view.findViewById(R.id.tv_dob);
            tv_caseid =(CustomTextView) view.findViewById(R.id.tv_caseid);
            tv_name_opinion=(CustomTextView) view.findViewById(R.id.tv_name_opinion);
            tv_name_status=(CustomTextView) view.findViewById(R.id.tv_name_status);
            tv_response=(CustomTextView) view.findViewById(R.id.tv_response);

        }
    }

    public MyRecyclerAdapter(List<listitem> moviesList) {
        this.moviesList = moviesList;

        // this.mContext = mcontext;
    }

    @Override
    public MyRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.onlineopinion_view, parent, false);


        return new MyRecyclerAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyRecyclerAdapter.MyViewHolder holder, int position) {
        listitem movie = moviesList.get(position);

      /*  holder.tv_heading.setText(movie.getTitle());
        holder.tv_dob.setText(movie.getYear());
        holder.tv_title.setText(movie.getTitle());*/

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}