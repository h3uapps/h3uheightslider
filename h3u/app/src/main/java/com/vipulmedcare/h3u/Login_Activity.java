package com.vipulmedcare.h3u;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by prem mali on 7/13/2016.
 */
public class Login_Activity extends Activity {
    Button btn_login;
    EditText edit_name,edit_mobile;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_activity);

      /*  edit_name= (EditText) findViewById(R.id.edit_name);
        edit_mobile= (EditText) findViewById(R.id.edit_mobile);
        btn_login= (Button) findViewById(R.id.btn_login);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             *//*   Intent i=new Intent(Login_Activity.this,Otp_Activity.class);
                startActivity(i);*//*

                if (btn_login.getText().toString().equals("Login")) {
                    if (edit_name.getText().toString().trim().equals("") || edit_mobile.getText().toString().trim().equals("")) {
                        Toast.makeText(getApplicationContext(), "Fields cannot be blank", Toast.LENGTH_SHORT).show();
                    } else {

                          LoginApi();

                      *//*  Intent i=new Intent(Login_Activity.this,Otp_Activity.class);
                        startActivity(i);
                        finish();*//*

                    }}}
        });
    }
    String username;
    private void LoginApi() {
        mProgressDialog = new ProgressDialog(Login_Activity.this,
                AlertDialog.THEME_HOLO_LIGHT);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        username = edit_name.getText().toString().trim();
        final String number = edit_mobile.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.appurl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        System.out.println(response);
                        mProgressDialog.dismiss();
                        try {
                            JSONObject json = new JSONObject(response);
                            if (json.getInt("status") == 1) {
                                Toast.makeText(Login_Activity.this, "Login Successful! Location would be updated!", Toast.LENGTH_LONG).show();
                                SharedPreferences.Editor editor = getSharedPreferences("MyPrefsFile", MODE_PRIVATE).edit();
                                editor.putString("name", json.getJSONObject("data").getString("name"));
                                editor.putString("number", json.getJSONObject("data").getString("phone"));
                                editor.commit();

                                Intent i = new Intent(Login_Activity.this,Otp_Activity.class);
                                i.putExtra("otp",json.getJSONObject("data").getString("otp"));
                                startActivity(i);
                                finish();

                            } else {
                                Toast.makeText(Login_Activity.this, "Invalid Mobile or Password", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();
                        Toast.makeText(Login_Activity.this, "Error, Please try again", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("actionName", "login");
                params.put("name", username);
                params.put("mobile", number);

                System.out.println("Login SendData" + params);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    */}
}
