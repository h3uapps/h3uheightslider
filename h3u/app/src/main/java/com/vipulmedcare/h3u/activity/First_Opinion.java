package com.vipulmedcare.h3u.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.base.BaseActivity;


/**
 * Created by vipul on 8/31/2017.
 */

public class First_Opinion extends BaseActivity {

    RelativeLayout rl_select_member,selectmtwo;
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.first_opinion);

        rl_select_member= (RelativeLayout) findViewById(R.id.rl_select_member);
        selectmtwo= (RelativeLayout) findViewById(R.id.selectmtwo);
        rl_select_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(First_Opinion.this,FirstOpinion_Form.class);
                startActivity(i);
            }
        });

    }
}
