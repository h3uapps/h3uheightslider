package com.vipulmedcare.h3u.view.text;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.vipulmedcare.h3u.model.Fonts;


/**

 */

public class CustomEditTextLight extends EditText {

    public CustomEditTextLight(Context context) {
        super(context);
        setFont();
    }

    public CustomEditTextLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public CustomEditTextLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    protected void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), Fonts.TYPO_LATTO_LIGHT);
        setTypeface(font, Typeface.NORMAL);
    }
}


