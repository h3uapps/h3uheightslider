package com.vipulmedcare.h3u.registration;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.utils.Utils;
import com.vipulmedcare.h3u.view.text.CustomTextView;
import com.vipulmedcare.h3u.view.text.CustomTextViewBold;


/**
 * Created by Andrew Pj on 8/3/2017.
 */

public class Activity_Selectgender extends BaseActivity implements View.OnClickListener {
    Button btn_next;

    TextView tv_back,tv_gname,tv_tellaboutus;
    private Context mContext;
    ToggleButton tb_male,tb_female,tb_other;
    public static final String EXTRA_MESSAGE = "username";
    public boolean clickmale=false;
    String Selected = "";
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_selectgender);

        mContext = Activity_Selectgender.this;
        initviews();

    }

    private void initviews() {


        Intent intent = getIntent();
        String message = intent.getStringExtra(Activity_Selectgender.EXTRA_MESSAGE);

        tv_gname= (CustomTextView) findViewById(R.id.tv_gname);
        tv_gname.setText(message);
        tv_tellaboutus= (CustomTextViewBold) findViewById(R.id.tv_tellaboutus);


        /* Typeface m3= Typeface.createFromAsset(getAssets(),"fonts/HelveticaLTStd-Bold.otf");
        tv_tellaboutus.setTypeface(m3);*/

      /*  Typeface m= Typeface.createFromAsset(getAssets(),"fonts/Lato-Regular.ttf");
        tv_gname.setTypeface(m);*/

        tb_male= (ToggleButton) findViewById(R.id.tb_male);
        Typeface m1= Typeface.createFromAsset(getAssets(),"fonts/Lato-Regular.ttf");
        tb_male.setTypeface(m1);
        tb_female= (ToggleButton) findViewById(R.id.tb_female);
        Typeface m2= Typeface.createFromAsset(getAssets(),"fonts/Lato-Regular.ttf");
        tb_female.setTypeface(m2);
        tb_other= (ToggleButton) findViewById(R.id.tb_other);
        Typeface m4= Typeface.createFromAsset(getAssets(),"fonts/Lato-Regular.ttf");
        tb_other.setTypeface(m4);

        tv_back= (TextView)findViewById(R.id.tv_back);
        btn_next= (Button) findViewById(R.id.btn_next);
        btn_next.setOnClickListener(this);
        tv_back.setOnClickListener(this);
        tb_male.setOnClickListener(this);
        tb_female.setOnClickListener(this);
        tb_other.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                if(clickmale) {
                    if (Selected == "") {
                        Utils.showAlert(mContext, "Please select a Gender", "Select Gender");
                    }
                    else {

                            Intent I = new Intent(mContext,Activity_number.class);
                            startActivity(I);

                      /*  String male = tb_male.getText().toString();
                        String famale = tb_female.getText().toString();
                        String other = tb_other.getText().toString();
                     */
                      //  String message = famale + " " + male;
                        I.putExtra(EXTRA_MESSAGE, Selected);

                        }
                    }
                    else {
                    Utils.showAlert(mContext, "Please select a Gender", "Select Gender");
                }

                break;

            case R.id.tb_male:

                clickmale=true;
                if (tb_female.isChecked()){
                    tb_female.setChecked(false);
                    tb_other.setChecked(false);
                }
                if (tb_male.isChecked()){
                    tb_other.setChecked(false);
                    tb_female.setChecked(false);
                }else {
                    tb_male.setChecked(true);
                    tb_other.setChecked(false);
                    tb_female.setChecked(false);
                }
                Selected ="tb_male";

                btn_next.setEnabled(true);
                break;

            case  R.id.tb_female:

                clickmale=true;
                if (tb_male.isChecked()){
                    tb_male.setChecked(false);
                    tb_other.setChecked(false);
                }
                if (tb_female.isChecked()){
                    tb_other.setChecked(false);
                    tb_male.setChecked(false);
                }else {
                    tb_female.setChecked(true);
                    tb_other.setChecked(false);
                    tb_male.setChecked(false);
                }
                Selected ="tb_female";
                btn_next.setEnabled(true);
                break;

            case R.id.tb_other:
                clickmale=true;
                if (tb_female.isChecked()){
                    tb_female.setChecked(false);
                    tb_male.setChecked(false);
                }
                if (tb_other.isChecked()){
                    tb_female.setChecked(false);
                    tb_male.setChecked(false);
                }else {
                    tb_other.setChecked(true);
                    tb_female.setChecked(false);
                    tb_male.setChecked(false);
                }
                Selected="tb_other";
                btn_next.setEnabled(true);
                break;

            case R.id.tv_back:
                finish();
                break;
        }
    }
}
