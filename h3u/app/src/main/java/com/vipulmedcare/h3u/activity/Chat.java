package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.ChatAdapter;
import com.vipulmedcare.h3u.adapter.VerticalAdapter;
import com.vipulmedcare.h3u.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import static com.vipulmedcare.h3u.R.id.chatrv;

/**
 * Created by vipul on 9/12/2017.
 */

public class Chat extends BaseActivity {
    private RecyclerView  chatrv;
    private ChatAdapter chatAdapter ;
    private Context mContext ;
    List ChatOptions;



    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.chat);
        chatrv = (RecyclerView) findViewById(R.id.chatrv);
        mContext = Chat.this;
        ChatOptions = new ArrayList();
        ChatOptions.add("option");

        ChatOptions.add("option");

        ChatOptions.add("option");

        ChatOptions.add("option");

        ChatOptions.add("option");

        ChatOptions.add("option");

        ChatOptions.add("option");

        chatAdapter=new ChatAdapter(ChatOptions,mContext);
        LinearLayoutManager verticalLayoutmanager
                = new LinearLayoutManager(Chat.this, LinearLayoutManager.VERTICAL, false);
        chatrv.setLayoutManager(verticalLayoutmanager);

        chatrv.setAdapter(chatAdapter);


    }
}
