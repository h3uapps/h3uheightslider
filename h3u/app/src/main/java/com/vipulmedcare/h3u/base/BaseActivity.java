package com.vipulmedcare.h3u.base;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.network.UpdateListener;
import com.vipulmedcare.h3u.utils.PermissionsManager;

import java.util.HashMap;
import java.util.Map;


public class BaseActivity extends Activity implements
		UpdateListener.onUpdateViewListener {


	boolean  isFirstTime=true;
	private ProgressDialog mProgressDialog;

	//DataModel dataModel = DataModel.getDataModelInstance();

	@Override
	public void onStart() {
		super.onStart();

		//onStart is called again, when switched between apps (like camera, gallery or minimised)
		// so stop re-initiazed the form/assets
		if(this.isFirstTime){
			this.isFirstTime = false;
			this.initialize();
		}
	}
	protected void initialize() {

		this.getWindow().setGravity(Gravity.BOTTOM);

		//this.setupToolbar();

		this.onInitialize();

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			//this.sharedActivityTransitions();
		}
	}
	protected void onInitialize() {
		//override this method in child classes to initialise the layout elements
	}
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);

	}

	/*@SuppressLint("NewApi")
	protected void sharedActivityTransitions() {
		//override this method in child classes to put the shared transitions between two activities.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			//options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, logo, getString(R.string.activity_logo_trans));
		}
	}*/
	public void showProgressDialog(String bodyText) {
		if (isFinishing()) {
			return;
		}
		if (mProgressDialog == null) {
			mProgressDialog = new ProgressDialog(BaseActivity.this);
			mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			mProgressDialog.setCancelable(false);
			mProgressDialog.setOnKeyListener(new Dialog.OnKeyListener() {
				@Override
				public boolean onKey(DialogInterface dialog, int keyCode,
                                     KeyEvent event) {
					if (keyCode == KeyEvent.KEYCODE_CAMERA
							|| keyCode == KeyEvent.KEYCODE_SEARCH) {
						return true; //
					}
					return false;
				}
			});
		}

		mProgressDialog.setMessage(bodyText);

		if (!mProgressDialog.isShowing()) {
			mProgressDialog.show();
		}
	}

	/**
	 * Removes the simple native progress dialog shown via showProgressDialog <br/>
	 * Subclass can override below two methods for custom dialogs- <br/>
	 * 1. showProgressDialog <br/>
	 * 2. removeProgressDialog
	 */
	public void removeProgressDialog() {
		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
	}

	public void hideSoftKeyBoard11() {
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}

	public void hideSoftKeyBoardWhenOnFocus() {

		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(getCurrentFocus()
				.getWindowToken(), 0);
	}

	public Map<String, String> getBaseParams() {
		HashMap<String, String> params = new HashMap<String, String>();
		// params.put(ApiConstants.PARAM_RESULT,
		// ApiConstants.VALUE_RESULT_TYPE);
		// params.put(ApiConstants.PARAM_VENDOR_KEY,
		// ApiConstants.VALUE_VENDOR_KEY);
		// params.put(ApiConstants.PARAM_VENDOR_NAME,
		// ApiConstants.VALUE_VENDOR_NAME);
		return params;
	}

	@Override
	public void updateView(String responseString, boolean isSuccess, int reqType) {
		Toast.makeText(this, "BaseActivity", Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		removeProgressDialog();
	}


	public static void showToast(Context context, String message) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View layout = inflater.inflate(R.layout.toast_layout,null);
		TextView tv_toast = (TextView) layout.findViewById(R.id.tv_toast);
		tv_toast.setText(message);
		Toast toast = new Toast(context);
		toast.setGravity(Gravity.BOTTOM, 0,60);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(layout);
		toast.show();
	}


	public static void showErrorAlert(final Context context, String Message) {
		final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle("Mandatory Fields");
		alertDialog.setMessage(Message);
		alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {

			}
		});
		AlertDialog alert = alertDialog.create();
		alert.show();
		Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
		pbutton.setBackgroundColor(Color.GRAY);

		pbutton.setTextColor(Color.CYAN);
	}

	public static void showAlert(final Context mContext, String Message, String Title) {
		final Dialog myDialog = new Dialog(mContext);
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.dialog_myalert);
		final TextView tv_title = (TextView) myDialog.findViewById(R.id.tv_title);
		tv_title.setText(Title);
		final TextView tv_message = (TextView) myDialog.findViewById(R.id.tv_message);
		tv_message.setText(Message);
		final Button btn_security_submit = (Button) myDialog.findViewById(R.id.btn_ok);
		btn_security_submit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}});
		myDialog.show();
	}


	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		switch (requestCode) {
			case PermissionsManager.MULTI_PERMISSIONS_REQUEST_CODE:
				onMultiPermissionResponse(permissions, grantResults);
				break;
			default:
				onPermissionResponse(requestCode, (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED));
		}
		return;
	}

	protected void onPermissionResponse(int requestCode, boolean denied){
		//        switch (requestCode) {
		//            case REQUEST_CODE: {
		//
		//            }
		//        }
	}

	protected void onMultiPermissionResponse(String[] permissions, int[] grantResult) {
		// override this method in the child class to get response of the permissions
		// request code is already checked
	}
}
