package com.vipulmedcare.h3u.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.pojo.listitem;
import com.vipulmedcare.h3u.utils.Utils;
import com.vipulmedcare.h3u.view.text.CustomTextView;
import com.vipulmedcare.h3u.view.text.CustomTextViewBold;

import java.util.List;

/**
 * Created by Andrew pj on 8/21/2017.
 */

public class myTestAdapter extends RecyclerView.Adapter<myTestAdapter.MyViewHolder> {

    private List<listitem> moviesList;
    private Context mContext;

    public myTestAdapter(Context mcontext,List<listitem> movieList) {
       this.mContext = mcontext;
        this.moviesList = movieList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_heading, tv_dob, tv_title,tv_share,tv_view;


        public MyViewHolder(final View view) {
            super(view);

            tv_heading = (CustomTextView) view.findViewById(R.id.tv_heading);
            tv_dob = (CustomTextView) view.findViewById(R.id.tv_dob);
            tv_share= (CustomTextView) view.findViewById(R.id.tv_share);
            tv_view= (CustomTextView) view.findViewById(R.id.tv_view);

            tv_title= (CustomTextViewBold) view.findViewById(R.id.tv_title);


        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.testresult_view, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final listitem movie = moviesList.get(position);
        holder.tv_heading.setText(movie.getTitle());
        holder.tv_dob.setText(movie.getYear());
        holder.tv_title.setText(movie.getTitle());


        holder.tv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String shareBody = " H3u smart health care";
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                mContext.startActivity(Intent.createChooser(sharingIntent, "SHARE"));
            }
        });

        holder.tv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showAlert(mContext,"Work in progress","view");
            }
        });

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
