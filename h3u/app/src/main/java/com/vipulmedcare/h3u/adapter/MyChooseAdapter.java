package com.vipulmedcare.h3u.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.pojo.listitem;
import com.vipulmedcare.h3u.view.text.CustomTextView;

import java.util.List;

/**
 * Created by vipul on 8/22/2017.
 */

public class MyChooseAdapter extends RecyclerView.Adapter<MyChooseAdapter.MyViewHolder> {

    private List<listitem> moviesList;
    Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_name_service;


        public MyViewHolder(final View view) {
            super(view);


            tv_name_service= (CustomTextView) view.findViewById(R.id.tv_name_service);
           /* tv_share= (TextView) view.findViewById(R.id.tv_share);*/

           /* Typeface a = Typeface.createFromAsset(mContext.getAssets(),"fonts/Lato-Regular.ttf");
            tv_name_service.setTypeface(a);*/
        }
    }

    public MyChooseAdapter(Context mcontext,List<listitem> moviesList) {
        this.mContext = mcontext;
        this.moviesList = moviesList;
    }

    @Override
    public MyChooseAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.choose_special_view, parent, false);





        return new MyChooseAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyChooseAdapter.MyViewHolder holder, int position) {
        listitem movie = moviesList.get(position);

        holder.tv_name_service.setText(movie.getTitle());


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
