package com.vipulmedcare.h3u.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;


import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.SubmitForm_Adapter;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.pojo.listitem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew Pj on 9/1/2017.
 */

public class Submit_Form extends BaseActivity {
    RecyclerView list_submit;
    ProgressDialog pd;
    Context mContext;
    private SubmitForm_Adapter adapter;
    private List<listitem> movieList = new ArrayList<>();
    TextView tv_headertext;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.submit_form);

        list_submit= (RecyclerView) findViewById(R.id.list_submit);

        adapter = new SubmitForm_Adapter(mContext,movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        list_submit.setLayoutManager(mLayoutManager);
        list_submit.setItemAnimator(new DefaultItemAnimator());
        list_submit.setAdapter(adapter);

        prepareMovieData();
    }

    private void prepareMovieData() {
        listitem movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

      /*  movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);*/
      /*  movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);*/

        adapter.notifyDataSetChanged();
    }
}
