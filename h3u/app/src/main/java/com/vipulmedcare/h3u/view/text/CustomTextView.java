package com.vipulmedcare.h3u.view.text;

/**
 * Created by vjangra on 04/10/15.
 */

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.vipulmedcare.h3u.model.Fonts;


public class CustomTextView extends TextView {
    public CustomTextView(Context context) {
        super(context);
        setFont();
    }
    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }
    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    protected void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), Fonts.TYPO_LATTO_LIGHT);
        setTypeface(font, Typeface.NORMAL);
    }


}
