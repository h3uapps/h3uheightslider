package com.vipulmedcare.h3u.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.PopupMenu;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class ClickToSelectEditText<T extends Listable> extends AppCompatEditText {

    private List<T> mItems;
    private String[] mListableItems;
    private CharSequence mHint;
    private List<MenuItem> menuItems;
    private boolean showPopupOnClick;

    private OnItemSelectedListener<T> onItemSelectedListener;

    public ClickToSelectEditText(Context context) {
        super(context);

        mHint = getHint();
    }

    public ClickToSelectEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        mHint = getHint();
    }

    public ClickToSelectEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mHint = getHint();
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ClickToSelectEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);

        mHint = getHint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        setFocusable(false);
        setClickable(true);
    }

    public void setShowPopupOnClick(boolean showPopupOnClick) {
        this.showPopupOnClick = showPopupOnClick;
    }

    public void setItems(List<T> items) {
        this.mItems = items;
        this.mListableItems = new String[items.size()];

        int i = 0;

        for (T item : mItems) {
            mListableItems[i++] = item.getLabel();
        }

        configureOnClickListener(mItems);
    }

    private void configureOnClickListener(final List<T> items) {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (showPopupOnClick) {
                    PopupMenu popup = new PopupMenu(view.getContext(), view);

                    for (int i = 0; i < items.size(); i++) {
//                        popup.getMenu().add(items.get(i).getLabel());
                        popup.getMenu().add(i, i, i, items.get(i).getLabel());
                    }

                    menuItems = new ArrayList<>();
                    for (int i = 0; i < items.size(); i++) {
                        menuItems.add(popup.getMenu().getItem(i));
                    }

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            int position = menuItems.indexOf(item);

                            setText(mListableItems[position]);
                            return true;
                        }
                    });
                    popup.show();

                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setTitle(mHint);
                    builder.setItems(mListableItems, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int selectedIndex) {
                            setText(mListableItems[selectedIndex]);

                            if (onItemSelectedListener != null) {
                                onItemSelectedListener.onItemSelectedListener(mItems.get(selectedIndex), selectedIndex);
                            }
                        }
                    });
                    builder.create().show();

                }

            }
        });
    }

    public void setOnItemSelectedListener(OnItemSelectedListener<T> onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }

    public interface OnItemSelectedListener<T> {
        void onItemSelectedListener(T item, int selectedIndex);
    }
}