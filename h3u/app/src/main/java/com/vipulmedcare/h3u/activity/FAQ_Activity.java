package com.vipulmedcare.h3u.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.vipulmedcare.h3u.R;

import java.util.ArrayList;

/**
 * Created by Andrew on 5/12/2016.
 */
public class FAQ_Activity extends Activity {
    TextView toolbarText;
    ImageView back,iv_home;

    private ArrayList<Product> pProductArrayList;
    private ArrayList<Product.SubCategory> pSubItemArrayList;
    private ArrayList<Product.SubCategory> pSubItemArrayList2;
    private ArrayList<Product.SubCategory> pSubItemArrayList3;
    private ArrayList<Product.SubCategory> pSubItemArrayList4;
    private ArrayList<Product.SubCategory> pSubItemArrayList5;
    private ArrayList<Product.SubCategory> pSubItemArrayList6;
    private ArrayList<Product.SubCategory> pSubItemArrayList7;
    private ArrayList<Product.SubCategory> pSubItemArrayList8;
    private ArrayList<Product.SubCategory> pSubItemArrayList9;
    private ArrayList<Product.SubCategory> pSubItemArrayList10;
    private ArrayList<Product.SubCategory> pSubItemArrayList11;
    private ArrayList<Product.SubCategory> pSubItemArrayList12;
    private LinearLayout mLinearListView;
    boolean isFirstViewClick=false;
    boolean isSecondViewClick=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faq_activity);
        //    .......  heading .... ................        //
        toolbarText =(TextView) findViewById(R.id.tv_toolbarText);
        toolbarText.setText("FAQ");
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
               // overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });

        mLinearListView = (LinearLayout) findViewById(R.id.linear_listview);
        ArrayList<Product.SubCategory.ItemList> mItemListArray=new ArrayList<Product.SubCategory.ItemList>();

        ArrayList<Product.SubCategory.ItemList> mItemListArray2=new ArrayList<Product.SubCategory.ItemList>();


        pSubItemArrayList=new ArrayList<Product.SubCategory>();
        pSubItemArrayList2=new ArrayList<Product.SubCategory>();
        pSubItemArrayList3=new ArrayList<Product.SubCategory>();
        pSubItemArrayList4=new ArrayList<Product.SubCategory>();
        pSubItemArrayList5=new ArrayList<Product.SubCategory>();
        pSubItemArrayList6=new ArrayList<Product.SubCategory>();
        pSubItemArrayList7=new ArrayList<Product.SubCategory>();
        pSubItemArrayList8=new ArrayList<Product.SubCategory>();
        pSubItemArrayList9=new ArrayList<Product.SubCategory>();
        pSubItemArrayList10=new ArrayList<Product.SubCategory>();
        pSubItemArrayList11=new ArrayList<Product.SubCategory>();
        pSubItemArrayList12=new ArrayList<Product.SubCategory>();


        pSubItemArrayList.add(new Product.SubCategory("H3U (smart healthcare) is a digital initiative that is dedicated to the idea of preventive management of health and provides services that facilitate everything from health awareness and self assessment to pre-hospitalization which includes health camps, health talks, health check-ups with report management, immunization at group level apart from cashless OPD and discounted pharmacy and diagnostic services.\n" +
                "\n" +
                "For Doing Daily Tasks: We believe in making your life simpler. Run errands, do chores, deposit bills/cheques and so much more without having to go do any of these yourself.\n" +
                "\n" + "We Do It All!\n", mItemListArray));

        pSubItemArrayList2.add(new Product.SubCategory("H3U is true to its belief in preventive healthcare which truly means being able to maintain and improve your health whether healthy or otherwise irrespective of age, race, gender, economic strata, or any other criteria. Good health is truly deserved by all and H3U is for all who believe in that idea for themselves and the people around them.", mItemListArray2));
        pSubItemArrayList3.add(new Product.SubCategory("There are no pre-requisites to becoming an H3U member currently.", mItemListArray2));
        pSubItemArrayList4.add(new Product.SubCategory("H3U harnesses better pricing through the use of technology and bulk purchasing power driven through its strong base of customers which it passes back to the users to truly deliver on the first pillar of its mission, i.e. Affordable healthcare.", mItemListArray2));
        pSubItemArrayList5.add(new Product.SubCategory("To deliver on a key Mission pillar, i.e. Accessible healthcare, H3U has forged strong relationships with over 5000 provider outlets spread over 22 states of India covering Hospitals, Clinics, Diagnostic Centers, Pharmacies and other Wellness Centers for services like OPD Consultations, Diagnostic Tests, Pharmacy Purchases as well as other wellness services like discounted gym memberships, etc on a cashless basis through the H3U app services. The H3U network is dynamic and is getting constantly upgraded and you can click on the network icon on the top right corner of your H3U app after you have signed in to access the most updated H3U network provider network near you.", mItemListArray2));

        pSubItemArrayList6.add(new Product.SubCategory("OPD means Out Patient Department or services which includes day care services like doctor's consultations, pharmacy purchases, day care procedures like immunizations, dental procedures,etc, diagnostic services, other wellness services.", mItemListArray2));
        pSubItemArrayList7.add(new Product.SubCategory("The H3U tools and trackers are self assessment widgets that help the user understand the current status of their health and well-being. These tools include a detailed Health Risk Assessment, Salt Meter, Diet Meter, Smoke Meter, BMI tool, etc. Attempt one today to know how you are doing.", mItemListArray2));
        pSubItemArrayList8.add(new Product.SubCategory("Affordability, reliability and accessibility are assured when you book your health check up on your H3U app. Book one today to know about your health at the best discounts.", mItemListArray2));
        pSubItemArrayList9.add(new Product.SubCategory("The Medhealth card is a unique platform for managing health and health transactions where unique customized products can be structured along with multiple tools for overall health management." + "\n" + "The key features of the service include:\n"+ "\n" + "Cashless doctor consultations\n"+ "\n" + "Discounted Health Check-ups with report management\n"+ "\n" + "Discounts at Pharmacies\n"+ "\n" + "Online Opinions "+ "\n" + "Tools & Trackers "+ " "+ "\n" + "There are multiple membership categories of H3U Medhealth Cards. To know how to become a medhealth card member, click here.\n", mItemListArray));

        pSubItemArrayList10.add(new Product.SubCategory("", mItemListArray2));
        pSubItemArrayList11.add(new Product.SubCategory("", mItemListArray2));
        pSubItemArrayList12.add(new Product.SubCategory("", mItemListArray2));


       /* pSubItemArrayList7.add(new Product.SubCategory("We are operational between 9 AM & 11 PM. Though you can still place an order for a later date & time even in the non-operational hours.", mItemListArray2));
        pSubItemArrayList8.add(new Product.SubCategory("Yes, you can schedule a request for a time of your convenience within our standard operating hours.", mItemListArray2));
        pSubItemArrayList9.add(new Product.SubCategory("No, we do not charge any premium on any of the products ordered. Our policy is to try and deliver all the products at or below MRP. You are also welcome to inform us of any discount offers or coupons applicable at the stores of your choice", mItemListArray2));
        pSubItemArrayList10.add(new Product.SubCategory("An order/service-request can be cancelled without any charges before it has been picked-up by our foot soldiers.", mItemListArray2));
        pSubItemArrayList11.add(new Product.SubCategory("We deliver “Anything-Anytime!” from any store of your preference that falls within the geographical limits of Gurgaon.", mItemListArray2));
        pSubItemArrayList12.add(new Product.SubCategory("Our policy is to complete requests within 90 minutes or less. If a particular request is taking longer than usual than our team will inform you in advance.", mItemListArray2));
*/
        //pSubItemArrayList.add(new Product.SubCategory("dfdfd", mItemListArray));
       // pSubItemArrayList2.add(new Product.SubCategory("Cloths", mItemListArray2));
        /**
         *
         */

        pProductArrayList=new ArrayList<Product>();
        pProductArrayList.add(new Product("What is H3U?", pSubItemArrayList));
        pProductArrayList.add(new Product("Is H3U useful for me", pSubItemArrayList2));
        pProductArrayList.add(new Product("Are there any pre-conditions to become an H3U member?", pSubItemArrayList3));
        pProductArrayList.add(new Product("How do I save money using H3U app?", pSubItemArrayList4));
        pProductArrayList.add(new Product("Where can I avail H3U services?", pSubItemArrayList5));
        pProductArrayList.add(new Product("What is cashless OPD?", pSubItemArrayList6));
        pProductArrayList.add(new Product("What are tools trackers?", pSubItemArrayList7));
        pProductArrayList.add(new Product("Is there any advantage of booking my health check up through H3U?", pSubItemArrayList8));
        pProductArrayList.add(new Product("What is Medhealth Card?", pSubItemArrayList9));
        pProductArrayList.add(new Product("What is your cancellation policy?", pSubItemArrayList10));
        pProductArrayList.add(new Product("What are the supported stores?", pSubItemArrayList11));
        pProductArrayList.add(new Product("How much time does it take to fulfil a request?", pSubItemArrayList12));

        /***
         * adding item into listview
         */
        for (int i = 0; i < pProductArrayList.size(); i++) {

            LayoutInflater inflater = null;
            inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mLinearView = inflater.inflate(R.layout.row_first, null);

            final TextView mProductName = (TextView) mLinearView.findViewById(R.id.textViewName);
            final RelativeLayout mLinearFirstArrow=(RelativeLayout)mLinearView.findViewById(R.id.linearFirst);
            final ImageView mImageArrowFirst=(ImageView)mLinearView.findViewById(R.id.imageFirstArrow);
            final LinearLayout mLinearScrollSecond=(LinearLayout)mLinearView.findViewById(R.id.linear_scroll);

            if(isFirstViewClick==false){
                mLinearScrollSecond.setVisibility(View.GONE);
                mImageArrowFirst.setBackgroundResource(R.drawable.plussymbol_icon);
            }
            else{
                mLinearScrollSecond.setVisibility(View.VISIBLE);
                mImageArrowFirst.setBackgroundResource(R.drawable.minussymbol_icon);
            }

            mLinearFirstArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isFirstViewClick==false){
                        isFirstViewClick=true;
                        mImageArrowFirst.setBackgroundResource(R.drawable.minussymbol_icon);
                        mLinearScrollSecond.setVisibility(View.VISIBLE);

                    }else{
                        isFirstViewClick=false;
                        mImageArrowFirst.setBackgroundResource(R.drawable.plussymbol_icon);    //prem
                        mLinearScrollSecond.setVisibility(View.GONE);
                    }
                }
            });

//            mLinearFirstArrow.setOnTouchListener(new OnTouchListener() {
//
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//
//                    if(isFirstViewClick==false){
//                        isFirstViewClick=true;
//                        mImageArrowFirst.setBackgroundResource(R.drawable.plussymbol_icon);
//                        mLinearScrollSecond.setVisibility(View.VISIBLE);
//
//                    }else{
//                        isFirstViewClick=false;
//                        mImageArrowFirst.setBackgroundResource(R.drawable.plussymbol_icon);    //prem
//                        mLinearScrollSecond.setVisibility(View.GONE);
//                    }
//                    return false;
//                }
//            });


            final String name = pProductArrayList.get(i).getpName();
            mProductName.setText(name);

            /**
             *
             */
            for (int j = 0; j < pProductArrayList.get(i).getmSubCategoryList().size(); j++) {

                LayoutInflater inflater2 = null;
                inflater2 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View mLinearView2 = inflater2.inflate(R.layout.row_second, null);

                TextView mSubItemName = (TextView) mLinearView2.findViewById(R.id.textViewTitle);
                final RelativeLayout mLinearSecondArrow=(RelativeLayout)mLinearView2.findViewById(R.id.linearSecond);
                final ImageView mImageArrowSecond=(ImageView)mLinearView2.findViewById(R.id.imageSecondArrow);
                final LinearLayout mLinearScrollThird=(LinearLayout)mLinearView2.findViewById(R.id.linear_scroll_third);

/*
                if(isSecondViewClick==false){
                    mLinearScrollThird.setVisibility(View.GONE);
                    mImageArrowSecond.setBackgroundResource(R.drawable.plussymbol_icon);
                }
                else{
                    mLinearScrollThird.setVisibility(View.VISIBLE);
                    mImageArrowSecond.setBackgroundResource(R.drawable.arw_down);
                }
*/

                mLinearSecondArrow.setOnTouchListener(new OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                      /*  if(isSecondViewClick==false){
                            isSecondViewClick=true;
                            mImageArrowSecond.setBackgroundResource(R.drawable.arw_down);
                            mLinearScrollThird.setVisibility(View.VISIBLE);

                        }else{
                            isSecondViewClick=false;
                            mImageArrowSecond.setBackgroundResource(R.drawable.arw_lt);
                            mLinearScrollThird.setVisibility(View.GONE);
                        }
                     */   return false;
                    }
                });


                final String catName = pProductArrayList.get(i).getmSubCategoryList().get(j).getpSubCatName();
                mSubItemName.setText(catName);
                /**
                 *
                 */
                for (int k = 0; k < pProductArrayList.get(i).getmSubCategoryList().get(j).getmItemListArray().size(); k++) {

                    LayoutInflater inflater3 = null;
                    inflater3 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View mLinearView3 = inflater3.inflate(R.layout.row_third, null);

                    TextView mItemName = (TextView) mLinearView3.findViewById(R.id.textViewItemName);
                    TextView mItemPrice = (TextView) mLinearView3.findViewById(R.id.textViewItemPrice);
                    final String itemName = pProductArrayList.get(i).getmSubCategoryList().get(j).getmItemListArray().get(k).getItemName();
                    final String itemPrice = pProductArrayList.get(i).getmSubCategoryList().get(j).getmItemListArray().get(k).getItemPrice();
                    mItemName.setText(itemName);
                    mItemPrice.setText(itemPrice);

                    mLinearScrollThird.addView(mLinearView3);
                }

                mLinearScrollSecond.addView(mLinearView2);

            }

            mLinearListView.addView(mLinearView);
        }
    }

}
