package com.vipulmedcare.h3u.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;


/**
 * Created by vipul on 8/24/2017.
 */

public class HealthCorporate  extends Fragment {
    private LinearLayout ll_book_btn;
    private TextView tv_expire, tv_date , health_corp_card_name, health_corp_con_avail, health_corp_cons_num, health_corp_gen_cpn;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.health_corporate, container, false);
        ll_book_btn = (LinearLayout) rootView.findViewById(R.id.ll_book_btn);
        ll_book_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startCalender  = new Intent(getActivity(),CalenderActivity.class);
                startActivity(startCalender);
            }
        });
        tv_expire = (TextView) rootView.findViewById(R.id.tv_expire);
        tv_date = (TextView) rootView.findViewById(R.id.tv_date);

        health_corp_card_name= (TextView) rootView.findViewById(R.id.health_corp_card_name);
        health_corp_con_avail= (TextView) rootView.findViewById(R.id.health_corp_con_avail);
        health_corp_cons_num= (TextView) rootView.findViewById(R.id.health_corp_cons_num);
        health_corp_gen_cpn= (TextView) rootView.findViewById(R.id.health_corp_gen_cpn);

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ocraextended.ttf");



        tv_expire.setTypeface(font);
        tv_date.setTypeface(font);



        return rootView;
    }
}
