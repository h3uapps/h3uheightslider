package com.vipulmedcare.h3u.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.CardDeatilPagerAdapter;
import com.vipulmedcare.h3u.health_checkup.Health_Checkup_Opd;
import com.vipulmedcare.h3u.utils.Utils;


/**
 * Created by Andrew Pj on 8/11/2017.
 */

public class CardDetail  extends AppCompatActivity implements
        View.OnClickListener {

    private static ViewPager mPager;
    private static ViewPager viewPager;
    private CardDeatilPagerAdapter mAdapter;
    private ImageView chevron_rt, chevron_lft;
    private Context mContext;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    LinearLayout lin_hameburge,ll_cards,lin_contextul;
    private RelativeLayout rl_smoke_meter;
    private TextView tv_corporate,tv_card_type,tv_free_opd,tv_available,tv_used,tv_used_count,tv_basic_health,tv_basic_health_avail,tv_basic_health_avail_count,tv_basic_health_used,tv_basic_health_used_count,tv_med_opn,tv_med_opn1,tv_med_opn1_avail,tv_med_opn1_avail_count,tv_med_opn1_used,tv_med_opn1_used_count,tv_med_opn2,tv_med_opn2_avail,tv_med_opn2_avail_count,tv_med_opn2_avail_used,tv_med_opn2_avail_used_count,tv_toolstracker_heading,tv_salt,tv_smoke;


    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.card_detail);
        mContext = CardDetail.this;
        viewPager = (ViewPager) findViewById(R.id.card_detail_pager);
        mPager= (ViewPager) findViewById(R.id.card_detail_pager_img);

        mAdapter = new CardDeatilPagerAdapter(getSupportFragmentManager(), mContext);
        viewPager.setAdapter(mAdapter);
        initView();
    }

    public void initView() {

        Typeface fontLatoRegular = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontHeading = Typeface.createFromAsset(getAssets(), "fonts/Lato-Bold.ttf");

        tv_corporate = (TextView)findViewById(R.id.tv_corporate);
                tv_card_type = (TextView)findViewById(R.id.tv_card_type);
        tv_free_opd = (TextView)findViewById(R.id.tv_free_opd);
                tv_available = (TextView)findViewById(R.id.tv_available);
        tv_used = (TextView)findViewById(R.id.tv_used);
                tv_used_count = (TextView)findViewById(R.id.tv_used_count);
        tv_basic_health = (TextView)findViewById(R.id.tv_basic_health);
                tv_basic_health_avail = (TextView)findViewById(R.id.tv_basic_health_avail);
        tv_basic_health_avail_count = (TextView)findViewById(R.id.tv_basic_health_avail_count);
                tv_basic_health_used = (TextView)findViewById(R.id.tv_basic_health_used);
        tv_basic_health_used_count = (TextView)findViewById(R.id.tv_basic_health_used_count);
                tv_med_opn = (TextView)findViewById(R.id.tv_med_opn);
        tv_med_opn1 = (TextView)findViewById(R.id.tv_med_opn1);
                tv_med_opn1_avail = (TextView)findViewById(R.id.tv_med_opn1_avail);
        tv_med_opn1_avail_count = (TextView)findViewById(R.id.tv_med_opn1_avail_count);
                tv_med_opn1_used = (TextView)findViewById(R.id.tv_med_opn1_used);
        tv_med_opn1_used_count = (TextView)findViewById(R.id.tv_med_opn1_used_count);
                tv_med_opn2 = (TextView)findViewById(R.id.tv_med_opn2);
        tv_med_opn2_avail = (TextView)findViewById(R.id.tv_med_opn2_avail);
                tv_med_opn2_avail_count = (TextView)findViewById(R.id.tv_med_opn2_avail_count);
        tv_med_opn2_avail_used = (TextView)findViewById(R.id.tv_med_opn2_avail_used);
                tv_med_opn2_avail_used_count = (TextView)findViewById(R.id.tv_med_opn2_avail_used_count);
        tv_toolstracker_heading = (TextView)findViewById(R.id.tv_toolstracker_heading);
                tv_salt = (TextView)findViewById(R.id.tv_salt);
        tv_smoke = (TextView)findViewById(R.id.tv_smoke);


        lin_contextul= (LinearLayout) findViewById(R.id.lin_contextul);
        lin_contextul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomDialog();
            }
        });
//
//
//                tv_corporate.setTypeface(fontLatoRegular);
//        tv_card_type.setTypeface(fontLatoRegular);
//                tv_free_opd.setTypeface(fontHeading);
//        tv_available.setTypeface(fontLatoRegular);
//                tv_used.setTypeface(fontLatoRegular);
//        tv_used_count.setTypeface(fontLatoRegular);
//                tv_basic_health.setTypeface(fontHeading);
//        tv_basic_health_avail.setTypeface(fontLatoRegular);
//                tv_basic_health_avail_count.setTypeface(fontLatoRegular);
//        tv_basic_health_used.setTypeface(fontLatoRegular);
//                tv_basic_health_used_count.setTypeface(fontLatoRegular);
//        tv_med_opn.setTypeface(fontHeading);
//                tv_med_opn1.setTypeface(fontLatoRegular);
//        tv_med_opn1_avail.setTypeface(fontLatoRegular);
//                tv_med_opn1_avail_count.setTypeface(fontLatoRegular);
//        tv_med_opn1_used.setTypeface(fontLatoRegular);
//                tv_med_opn1_used_count.setTypeface(fontLatoRegular);
//        tv_med_opn2.setTypeface(fontLatoRegular);
//                tv_med_opn2_avail.setTypeface(fontLatoRegular);
//        tv_med_opn2_avail_count.setTypeface(fontLatoRegular);
//                tv_med_opn2_avail_used.setTypeface(fontLatoRegular);
//        tv_med_opn2_avail_used_count.setTypeface(fontLatoRegular);
//                tv_toolstracker_heading.setTypeface(fontHeading);
//        tv_salt.setTypeface(fontLatoRegular);
//                tv_smoke.setTypeface(fontLatoRegular);








        lin_hameburge= (LinearLayout) findViewById(R.id.lin_hameburge);
        lin_hameburge.setOnClickListener(this);
        rl_smoke_meter = (RelativeLayout) findViewById(R.id.rl_smoke_meter);
        rl_smoke_meter.setOnClickListener(this);

        mPager = (ViewPager) findViewById(R.id.pager);
        chevron_rt = (ImageView) findViewById(R.id.chevron_rt);
        chevron_lft = (ImageView) findViewById(R.id.chevron_lft);
        chevron_rt.setVisibility(View.INVISIBLE);
        chevron_lft.setVisibility(View.INVISIBLE);
        chevron_rt.setOnClickListener(this);
        chevron_lft.setOnClickListener(this);

        int tab = viewPager.getCurrentItem();
        if (tab == 0) {
            chevron_lft.setVisibility(View.VISIBLE);
            chevron_rt.setVisibility(View.INVISIBLE);

        } else {
            chevron_rt.setVisibility(View.VISIBLE);
            chevron_lft.setVisibility(View.INVISIBLE);

        }

    }

    private void CustomDialog() {

        final Dialog alertDialog = new Dialog(this);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        // AlertDialog Builder
        AlertDialog.Builder bmneu = new AlertDialog.Builder(this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        alertDialog.setContentView(R.layout.activity_conetxupal);

        // alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        LinearLayout ll_hopital= (LinearLayout) alertDialog.findViewById(R.id.ll_hopital);
        ll_hopital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.showAlert(mContext, "Near by Hospital", "Work in progress");
            }
        });
        LinearLayout ll_addcard= (LinearLayout) alertDialog.findViewById(R.id.ll_addcard);
        ll_addcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showAlert(mContext, "ADD New Card", "Work in progress");
            }
        });
        LinearLayout ll_schedulehealth= (LinearLayout) alertDialog.findViewById(R.id.ll_schedulehealth);
        ll_schedulehealth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(CardDetail.this,Health_Checkup_Opd.class);
                startActivity(i);
                alertDialog.dismiss();
            }
        });

        LinearLayout ll_needopd= (LinearLayout) alertDialog.findViewById(R.id.ll_needopd);
        ll_needopd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(CardDetail.this,Activity_OPD.class);
                startActivity(i);
                alertDialog.dismiss();
            }
        });

        LinearLayout ll_tooltracks= (LinearLayout)alertDialog.findViewById(R.id.ll_tooltracks);
        ll_tooltracks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(CardDetail.this,Tools_tracker_main_Activity.class);
                startActivity(i);
                alertDialog.dismiss();
            }
        });

        LinearLayout ll_corss= (LinearLayout) alertDialog.findViewById(R.id.ll_corss);
        ll_corss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();

            }
        });
        alertDialog.show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chevron_rt:
                viewPager.setCurrentItem(0);
                chevron_lft.setVisibility(View.VISIBLE);
                chevron_rt.setVisibility(View.INVISIBLE);

                break;

            case R.id.chevron_lft:
                viewPager.setCurrentItem(1);
                chevron_rt.setVisibility(View.VISIBLE);
                chevron_lft.setVisibility(View.INVISIBLE);
                break;
            case R.id.rl_smoke_meter:
              Intent tool_tracker_start_intent = new Intent(mContext,HealthMapActivity.class);
                startActivity(tool_tracker_start_intent);
                break;




            case R.id.lin_hameburge:

                Intent i =new Intent(CardDetail.this,Activity_drawer.class);
                startActivity(i);

        }
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (viewPager.getCurrentItem()== 0 ){
                    chevron_rt.setVisibility(View.GONE);
                    chevron_lft.setVisibility(View.VISIBLE);
                }
                else {
                    chevron_rt.setVisibility(View.VISIBLE);
                    chevron_lft.setVisibility(View.GONE);
                }

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


//    private void showDiscount() {
//        if (NetworkUtil.isNetworkAvailable(mContext)) {
//            final Call<TestimonialMResponse> getTestimonial = CustomApplication.getRestClient().getService().getTestimonials();
//            getTestimonial.enqueue(new Callback<TestimonialMResponse>() {
//                @Override
//                public void onResponse(Response<TestimonialMResponse> response, Retrofit retrofit) {
//
//                    if (response.body().getSuccess()) {
//
//                        ArrayList<TestimonialResponce> list = response.body().getData();
//
//
//                        ArrayList<String> text = new ArrayList<String>();
//                        for (int i = 0; i < list.size(); i++) {
//                            String text1 = response.body().getData().get(i).getTestim();
//                            text.add(text1);
//                        }
//                        mPager.setAdapter(new sliding_card_adapter(mContext, text));
//                        CirclePageIndicator indicator = (CirclePageIndicator)findViewById(R.id.indicator);
//                        indicator.setViewPager(mPager);
//                        try {
//                            final float density = getResources().getDisplayMetrics().density;
////Set circle indicator radius
//                            indicator.setRadius(5 * density);
//
//                            NUM_PAGES = text.size();
//
//                            // Auto start of viewpager
//                            final Handler handler = new Handler();
//                            final Runnable Update = new Runnable() {
//                                public void run() {
//                                    if (currentPage == NUM_PAGES) {
//                                        currentPage = 0;
//                                    }
//                                    mPager.setCurrentItem(currentPage++, true);
//                                }
//                            };
//                            Timer swipeTimer = new Timer();
//                            swipeTimer.schedule(new TimerTask() {
//                                @Override
//                                public void run() {
//                                    handler.post(Update);
//                                }
//                            }, 10000, 10000);
//
//                            // Pager listener over indicator
//                            indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//
//                                @Override
//                                public void onPageSelected(int position) {
//                                    currentPage = position;
//
//                                }
//
//                                @Override
//                                public void onPageScrolled(int pos, float arg1, int arg2) {
//
//                                }
//
//                                @Override
//                                public void onPageScrollStateChanged(int pos) {
//
//                                }
//                            });
//                        } catch (Exception e) {
//
//                        }
//
//                    }
//                }
//
//            });
//
//        } else {
////            Utils.showNetworkSettingAlert(mContext);
//        }
//    }
    }
}
