package com.vipulmedcare.h3u.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.activity.Select_Type_Opinion;
import com.vipulmedcare.h3u.pojo.listitem;
import com.vipulmedcare.h3u.utils.Utils;
import com.vipulmedcare.h3u.view.text.CustomTextView;
import com.vipulmedcare.h3u.view.text.CustomTextViewBold;

import java.util.List;

/**
 * Created by vipul on 8/31/2017.
 */

public class Opd_Consult_Adapter extends RecyclerView.Adapter<Opd_Consult_Adapter.MyViewHolder> {

    private List<listitem> moviesList;
    private Context mContext;

    public Opd_Consult_Adapter(Context mcontext,List<listitem> movieList) {
        this.mContext = mcontext;
        this.moviesList = movieList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_heading, tv_dob, tv_title,tv_share,tv_view;
        LinearLayout ll_getopinion;

        public MyViewHolder(final View view) {
            super(view);

            tv_heading = (CustomTextView) view.findViewById(R.id.tv_heading);
            tv_dob = (CustomTextView) view.findViewById(R.id.tv_dob);
            tv_share= (CustomTextView) view.findViewById(R.id.tv_share);
            tv_view= (CustomTextView) view.findViewById(R.id.tv_view);

            tv_title= (CustomTextViewBold) view.findViewById(R.id.tv_title);
            ll_getopinion= (LinearLayout) view.findViewById(R.id.ll_getopinion);

        }
    }

    @Override
    public Opd_Consult_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.opdconsult_adapter, parent, false);


        return new Opd_Consult_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(Opd_Consult_Adapter.MyViewHolder holder, int position) {
        final listitem movie = moviesList.get(position);
        holder.tv_heading.setText(movie.getGenre());
        holder.tv_dob.setText(movie.getYear());
        holder.tv_title.setText(movie.getTitle());


        holder.tv_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String shareBody = " H3u smart health care";
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                mContext.startActivity(Intent.createChooser(sharingIntent, "SHARE"));
            }
        });

        holder.tv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showAlert(mContext,"Work in progress","view");
            }
        });

        holder.ll_getopinion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i =new Intent(mContext, Select_Type_Opinion.class);
                mContext.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
