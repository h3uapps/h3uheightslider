package com.vipulmedcare.h3u.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;


import com.vipulmedcare.h3u.R;

import java.util.List;

/**
 * Created by vipul on 9/1/2017.
 */

public class OPDCardsAdapter extends RecyclerView.Adapter<OPDCardsAdapter.MyViewHolder> {

    private List<String> verticalList;
    private Context mContext;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_rating_no , tv_no,tv_adrs,tv_spcl,tv_dist;
        private RatingBar ver_rcyr_rat_bar;
        ImageButton ib_nav;
        LinearLayout ver_rv_hosp_inf;

        public MyViewHolder(View view) {
            super(view);
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            tv_rating_no = (TextView) view.findViewById(R.id.tv_rating_no);

            tv_no = (TextView) view.findViewById(R.id.tv_no);

            tv_spcl = (TextView) view.findViewById(R.id.tv_spcl);

            tv_dist = (TextView) view.findViewById(R.id.tv_dist);
            ver_rcyr_rat_bar = (RatingBar) view.findViewById(R.id.ver_rcyr_rat_bar);
            ib_nav= (ImageButton)view.findViewById(R.id.ib_nav);
            ver_rv_hosp_inf = (LinearLayout) view.findViewById(R.id.ver_rv_hosp_inf);

        }
    }


    public OPDCardsAdapter(List<String> verticalList, Context mContext) {
        this.verticalList = verticalList;
        this.mContext=mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.opd_card_rv, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

//        holder.ver_rv_hosp_inf.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                Intent i = new Intent(mContext,Provider_detail_Activity.class);
////                mContext.startActivity(i);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return verticalList.size();
    }
}

