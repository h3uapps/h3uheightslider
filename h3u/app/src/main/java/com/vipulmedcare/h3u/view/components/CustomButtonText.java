package com.vipulmedcare.h3u.view.components;/*
package com.vipulmedcare.h3u20.view.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rahhi.rahhi.R;

*/
/**
 * Created by vjangra on 19/05/16.
 *//*

public class CustomButtonText extends LinearLayout {

    private TextView textView;

    public CustomButtonText(Context context) {
        super(context, null);
    }

    public CustomButtonText(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_button_text, this, true);

        textView = (TextView) findViewById(R.id.btnText);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.CustomButtonIconText, 0, 0);
        String titleText = attributes.getString(R.styleable.CustomButtonIconText_cbtTitleText);

        int background = attributes.getColor(R.styleable.CustomButtonIconText_cbtBackColor, getResources().getColor(R.color.green));
        int txtColor = attributes.getColor(R.styleable.CustomButtonIconText_cbtTxtColor, getResources().getColor(R.color.white));

        attributes.recycle();

        setOrientation(LinearLayout.HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);

        textView.setText(titleText);
        textView.setTextColor(txtColor);

        setBackgroundColor(background);
    }

}
*/
