/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package com.vipulmedcare.h3u.profile;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.db.DBManager;
import com.vipulmedcare.h3u.db.DatabaseHelper;
import com.vipulmedcare.h3u.ui.ClickToSelectEditText;
import com.vipulmedcare.h3u.utils.Utils;
import com.vipulmedcare.h3u.utils.Validation;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import static com.vipulmedcare.h3u.db.DatabaseHelper.Userid;


public class Personal_Fragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private View rootView;
    EditText edit_fname,edit_lname,edit_dob,edit_email,edit_address;
    TextView text_save;
    private Context mContext;
    TextView img_avatar_profile;
    private ProgressDialog pd;
    Spinner spin;
    private int cStartMonth, cStartDay, cStartYear;
    private ClickToSelectEditText edit_spin_state,edit_spin_city;
    TextInputLayout til_email,til_address;
    public static Personal_Fragment newInstance() {
        Personal_Fragment fragment = new Personal_Fragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      //  return inflater.inflate(R.layout.fragment_item_personal, container, false);

        View rootView = inflater.inflate(R.layout.fragment_item_personal, container, false);

        mContext = getActivity();


         //Creating the ArrayAdapter instance having the country list





        til_address= (TextInputLayout) rootView.findViewById(R.id.til_address);
        til_email= (TextInputLayout) rootView.findViewById(R.id.til_email);


        edit_spin_state= (ClickToSelectEditText) rootView.findViewById(R.id.edit_spin_state);

        edit_spin_city= (ClickToSelectEditText) rootView.findViewById(R.id.edit_spin_city);

        //img_avatar_profile= (TextView) rootView.findViewById(R.id.img_avatar_profile);

        edit_dob= (EditText)rootView.findViewById(R.id.edit_dob);
        edit_email= (EditText) rootView.findViewById(R.id.edit_email);
        edit_address= (EditText) rootView.findViewById(R.id.edit_address);
        text_save= (TextView) rootView.findViewById(R.id.text_save);

        //

        // offline save data
         bindprofile();


        text_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkValidations()) {
                    String  select_profile  ="Select * from tbl_profile where Userid='"+Userid +"'";
                    DatabaseHelper db=new DatabaseHelper(mContext);

                    Cursor cur =  db.getAllRecord(select_profile);
                    int count=cur.getCount();
                    int totalcount=0;
                    if (count>0) {
                        String select_profile1 = "update tbl_profile set DOB ='" + edit_dob + "',Emailid='" + edit_email.getText() + "',address='" + edit_address.getText() + "',State='" + edit_spin_state.getText() + "',city='" + edit_spin_city.getText() + "'";
                        Cursor cur1= db.getAllRecord(select_profile1);
                        Utils.showToast(mContext,"successfully view ");
                    }
                    else {

                        String select_profile1 = "INSERT INTO tbl_profile(DOB,Emailid,address,State,city)values('"+edit_dob.getText()+"','"+edit_email.getText()+"','"+edit_address.getText()+"','"+edit_spin_state.getText()+"','"+edit_spin_city.getText()+"')";
                        db.InsertRecord(select_profile1);
                        Utils.showToast(mContext,"successfully insert data ");

                    }
                    //  Utils.hideSoftKeyboard(mContext, mView);
                  //  saveMember();

                }
            }
        });

        edit_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              final Calendar c = Calendar.getInstance();

                cStartYear = c.get(Calendar.YEAR);
                cStartMonth = c.get(Calendar.MONTH);
                cStartDay = c.get(Calendar.DAY_OF_MONTH);

                int mStartYear = cStartYear - 18;
                int mStartMonth = cStartMonth;
                int mStartDay = cStartDay;
                Calendar calendar = Calendar.getInstance();
                calendar.set(mStartYear, mStartMonth, mStartDay, 0, 0, 0);

                //For Maximum Date Picker Logic
                int maxStartYear = cStartYear - 18;
                int maxStartMonth = cStartMonth;
                int maxStartDay = cStartDay;
                Calendar calendar1 = Calendar.getInstance();
                calendar1.set(maxStartYear, maxStartMonth, maxStartDay, 0, 0, 0);
                DatePickerDialog _date;
//                _date = new DatePickerDialog(mContext, datePickerListener, cStartYear, cStartMonth, cStartDay);

                _date = new DatePickerDialog(mContext, datePickerListener, maxStartYear, maxStartMonth, maxStartDay);
                _date.getDatePicker().setMaxDate(calendar1.getTimeInMillis());
                _date.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            //et_to_date.setText("");
                            edit_dob.setText("");
                        }
                    }
                });
                _date.show();
            }
        });

        return rootView;
    }
    public  void bindprofile(){
       // String  select_profile  ="Select * from tbl_profile where Userid='"+Userid +"'";
        String  select_profile  ="Select * from tbl_profile ";
        DatabaseHelper db=new DatabaseHelper(mContext);

        Cursor cur =  db.getAllRecord(select_profile);
        int count=cur.getCount();
        int totalcount=0;
        if (count>0) {
            if (cur.moveToFirst()) {
                do {
                    cur.getString(0);

                    edit_dob.setText(cur.getString(cur.getColumnIndex("DOB")));
                    edit_email.setText(cur.getString(cur.getColumnIndex("Emailid")));
                    edit_address.setText(cur.getString(cur.getColumnIndex("address")));
                    edit_spin_state.setText(cur.getString(cur.getColumnIndex("State")));
                    edit_spin_city.setText(cur.getString(cur.getColumnIndex("city")));

                    /*String FileName = cur.getString(cur.getColumnIndex("profileimagepath"));
                    String Imgbase64_1 = "";
                    if (FileName==null && !FileName.isEmpty()) {


                        try {
                            Imgbase64_1 = Utils.Createbase64Image("/sdcard/H3u" + "/" + FileName);
                        } catch (Exception e) {
                            Imgbase64_1 = "";
                        }
                    }*/
                }

                while (cur.moveToNext());

            }

        }
    }

    private boolean checkValidations() {

        Boolean result = true;
        if (!Validation.isName(til_address, edit_address, true)) {
            result = false;
        }
        else if (!Validation.isEmailAddress(til_email, edit_email, true)) {
            result = false;
        }
       /* else if (!Validation.isName(til_corporate_empid, et_corporate_empid, true)) {
            result = false;
        }*/
       /* else if (spin_relation.getSelectedItem().toString().equalsIgnoreCase("Select Relation")) {
            Utils.showErrorAlert(mContext, "Select Relation Type");
            result = false;
        }*/
        return result;
    }

    private final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            edit_dob.setText(new StringBuilder().append(Utils.checkDigit(selectedDay)).append("-")
                    .append(new DateFormatSymbols().getShortMonths()[selectedMonth]).append("-").append(selectedYear));
        }
    };

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
