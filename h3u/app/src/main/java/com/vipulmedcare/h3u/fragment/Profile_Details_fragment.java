package com.vipulmedcare.h3u.fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.Profile_PagerAdapter;


/**
 * Created by Andrew pj on 5/5/2017.
 */

public class Profile_Details_fragment extends AppCompatActivity implements
        View.OnClickListener {
    TabLayout tabLayout;
    private ViewPager viewpager;
    private Profile_PagerAdapter mAdapter;
    // Tab titles
    private String[] tabs = {"CORPORTAE", "PERSONAL"};
    private Context mContext;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.fragment_details);
        mContext = Profile_Details_fragment.this;

        // Initilization
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        mAdapter = new Profile_PagerAdapter(getSupportFragmentManager(), mContext);
        viewpager.setAdapter(mAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewpager);
        TabLayout.Tab tab = tabLayout.getTabAt(0);
        tab.select();

        initViews();

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void initViews() {

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
          /*  case R.id.prov_dtl_arrow_back:
                Intent intent1 = new Intent(mContext, provider_map_activity.class);
                startActivity(intent1);
                break;

            case R.id.chs_spl_cross:
                finish();
                break;
            case R.id.provider_detail_chat:
                Intent intent2 = new Intent(mContext, CardActivity.class);
                startActivity(intent2);
                break;
*/

        }

    }


}
