package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.HeightSliderAdapter;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.utils.Utils;
import com.vipulmedcare.h3u.view.text.CustomTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vipul on 9/13/2017.
 */

public class HeightSlider extends BaseActivity {


    private RecyclerView rv_sliding_ht;
    private HeightSliderAdapter heightSliderAdapter;
    private List heightList;
    private LinearLayout selector_value;
    private float selectorPosition ;
    private Context mContext;
    int width;

    int last;





    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.height_slider);
        mContext=HeightSlider.this;
        initViews();

    }

    public void initViews(){
        rv_sliding_ht = (RecyclerView) findViewById(R.id.rv_sliding_ht);
        selector_value = (LinearLayout) findViewById(R.id.selector_value);
        selectorPosition =(selector_value.getX()+(selector_value.getWidth()/2)) ;



        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels/2;

        heightList = new ArrayList();
        heightList.add("5.5");
        heightList.add("5.6");
        heightList.add("5.7");
        heightList.add("5.8");
        heightList.add("5.9");
        heightList.add("5.10");
        heightList.add("5.11");
        heightList.add("6.0");
        heightList.add("6.1");
        heightList.add("6.2");
        heightList.add("6.3");
        heightList.add("6.4");
//        heightSliderAdapter = new HeightSliderAdapter(heightList,mContext);
        final LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(HeightSlider.this, LinearLayoutManager.HORIZONTAL, false);

        horizontalLayoutManagaer.scrollToPosition(Integer.MAX_VALUE / 2);
        rv_sliding_ht.setLayoutManager(horizontalLayoutManagaer);
//        rv_sliding_ht.setAdapter(heightSliderAdapter);

        rv_sliding_ht.setAdapter(new HeightSliderAdapter(heightList,mContext, new HeightSliderAdapter.OnItemClickListener() {
            @Override public void onItemClick(String item) {
                Toast.makeText(mContext, "Item Clicked"+item, Toast.LENGTH_LONG).show();
//                for (int i = 0; i<heightList.size();i++){
//                    if(heightList.contains(item))
//                    {
////                        int totalVisibleItems = horizontalLayoutManagaer.findLastVisibleItemPosition() - horizontalLayoutManagaer.findFirstVisibleItemPosition();
////                        int centeredItemPosition = totalVisibleItems / 2;
//////                        rv_sliding_ht.smoothScrollToPosition(i);
////                        rv_sliding_ht.setScrollY(centeredItemPosition );
//
//                    }
//                }
            }
        }));





    }





}
