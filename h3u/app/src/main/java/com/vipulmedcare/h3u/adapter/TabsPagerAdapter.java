package com.vipulmedcare.h3u.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.vipulmedcare.h3u.fragment.Corporate;
import com.vipulmedcare.h3u.fragment.Personal;


public class TabsPagerAdapter extends FragmentPagerAdapter {
	final int PAGE_COUNT = 2;
	private String tabTitles[] = new String[] { "CORPORATE", "PERSONAL"};
	private Context context;

	public TabsPagerAdapter(FragmentManager fm, Context context) {
		super(fm);
		this.context = context;
	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			// Top Rated fragment activity
			return new Corporate();
		case 1:
			// Games fragment activity
			return new Personal();

		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return PAGE_COUNT;
	}

	public CharSequence getPageTitle(int position) {
		// Generate title based on item position

		return tabTitles[position];
	}

}
