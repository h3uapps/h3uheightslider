package com.vipulmedcare.h3u.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;


/**
 * Created by vipul on 8/24/2017.
 */

public class HealthPersonal   extends Fragment {
    private TextView tv_personal_expire, tv_personal_date;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.health_personal, container, false);
        tv_personal_expire = (TextView) rootView.findViewById(R.id.tv_personal_expire);
        tv_personal_date = (TextView) rootView.findViewById(R.id.tv_personal_date);
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ocraextended.ttf");
        tv_personal_expire.setTypeface(font);
        tv_personal_date.setTypeface(font);

        return rootView;
    }
}
