package com.vipulmedcare.h3u.view.text;

/**
 * Created by  on 04/10/15.
 */

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.vipulmedcare.h3u.model.Fonts;


public class CustomTextViewColonna extends TextView {

    public CustomTextViewColonna(Context context) {
        super(context);
        setFont();
    }
    public CustomTextViewColonna(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }
    public CustomTextViewColonna(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    protected void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), Fonts.TYPO_LOGO);
        setTypeface(font, Typeface.NORMAL);
    }

}
