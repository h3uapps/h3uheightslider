package com.vipulmedcare.h3u.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vipulmedcare.h3u.R;


/**
 * Created by vipul on 9/4/2017.
 */

public class Corporate_profile extends  android.support.v4.app.Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.corporate_profile, container, false);

        return rootView;
    }
}