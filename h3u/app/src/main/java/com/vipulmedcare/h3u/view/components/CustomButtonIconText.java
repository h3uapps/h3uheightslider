package com.vipulmedcare.h3u.view.components;/*
package com.vipulmedcare.h3u20.view.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rahhi.rahhi.R;

*/
/**
 * Created by vjangra on 19/05/16.
 *//*

public class CustomButtonIconText extends LinearLayout {

    private ImageView iconView;
    private TextView textView;

    public CustomButtonIconText(Context context) {
        super(context, null);
    }

    public CustomButtonIconText(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_button_icon_text, this, true);

        iconView = (ImageView) findViewById(R.id.btnIcon);
        textView = (TextView) findViewById(R.id.btnText);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.CustomButtonIconText, 0, 0);
        String titleText = attributes.getString(R.styleable.CustomButtonIconText_cbtTitleText);

        int iconBackground = attributes.getColor(R.styleable.CustomButtonIconText_cbtIconBackColor, getResources().getColor(R.color.black));
        int background = attributes.getColor(R.styleable.CustomButtonIconText_cbtBackColor, getResources().getColor(R.color.green));
        int txtColor = attributes.getColor(R.styleable.CustomButtonIconText_cbtTxtColor, getResources().getColor(R.color.white));
        int iconTintColor = attributes.getColor(R.styleable.CustomButtonIconText_cbtIconDrawableTint, 0);

        Drawable icon = attributes.getDrawable(R.styleable.CustomButtonIconText_cbtIconDrawable);
        attributes.recycle();

        setOrientation(LinearLayout.HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);

        iconView.setBackgroundColor(iconBackground);
        if(icon != null) iconView.setImageDrawable(icon);
        iconView.setColorFilter(iconTintColor);

        textView.setText(titleText);
        textView.setTextColor(txtColor);

        setBackgroundColor(background);
    }

}
*/
