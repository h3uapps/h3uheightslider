package com.vipulmedcare.h3u.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vjangra on 21/11/15.
 */
public class PermissionRequest {

    public int requestCode;

    public String permission;

    private String[] permissions = null;

    public String title; // message to show user, if permission is not allowed
    public String message; // message to show user, if permission is not allowed
    public Drawable drawable;

    public PermissionRequest(int requestCode, String permission) {

        this.requestCode = requestCode;
        this.permission = permission;

        this.title = "Permission Required";
        this.message = permission.substring(0, 19) + " permission is important to give you a smooth experience. Please allow access to the requested permission.";
    }

    public PermissionRequest(int requestCode, String[] permissions) {

        this.requestCode = requestCode;
        this.permissions = permissions;

        this.title = "Permissions Required";
        this.message = "Requested permissions are important to give you a smooth experience. Please allow access to the requested permissions.";
    }

    public PermissionRequest(int requestCode, String permission, String title, String desc, Drawable drawable) {
        this.requestCode = requestCode;
        this.permission = permission;

        this.title = title;
        this.message = desc;
        this.drawable = drawable;
    }


    public PermissionRequest(int requestCode, String[] permissions, String title, String desc, Drawable drawable) {
        this.requestCode = requestCode;
        this.permissions = permissions;

        this.title = title;
        this.message = desc;
        this.drawable = drawable;
    }

    public int getSize() {
        return permissions != null ? permissions.length : 0;
    }

    public String[] getDeniedPermissionsList(Activity activity) {
        List<String> list = new ArrayList<String>();
        if (permissions != null) {
            for (String perm : permissions) {
                if (ContextCompat.checkSelfPermission(activity, perm) != PackageManager.PERMISSION_GRANTED) {
                    list.add(perm);
                }
            }
        } else {
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                list.add(permission);
            }
        }
        String[] permissions = list.toArray(new String[list.size()]);
        return permissions;
    }

    public boolean deniedTemporary(Activity activity) {
        return ActivityCompat.shouldShowRequestPermissionRationale(activity, permission);
    }


}
