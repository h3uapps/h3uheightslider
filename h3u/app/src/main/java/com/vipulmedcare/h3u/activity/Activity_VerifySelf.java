package com.vipulmedcare.h3u.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.utils.Validation;


/**
 * Created by vipul on 5/29/2017.
 */

public class Activity_VerifySelf extends BaseActivity implements View.OnClickListener {
    Button btn_process;
    EditText et_name,et_mobile;
    TextInputLayout til_fname,til_mobile;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_self);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("VERIFY NOW");
        toolbar.setTitleTextColor(Color.WHITE);
       /* setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);*/
        til_fname= (TextInputLayout) findViewById(R.id.til_fname);
        til_mobile= (TextInputLayout) findViewById(R.id.til_mobile);

        et_name= (EditText)findViewById(R.id.et_name);
        et_mobile=(EditText)findViewById(R.id.et_mobile);
        btn_process = (Button) findViewById(R.id.btn_process);
        btn_process.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_process:
                if (checkValidation()) {
                    Intent  mIntent = new Intent(Activity_VerifySelf.this, Activity_Otp.class);
                    startActivity(mIntent);
                    finish();
                }
                break;
        }
    }

    private boolean checkValidation() {
        boolean result = true;

        if (!Validation.isName(til_fname, et_name, true))
            result = false;

        if (!Validation.isPhoneNumber(til_mobile, et_mobile, true))
            result = false;

        return result;
    }
}
