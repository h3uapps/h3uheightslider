package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;


import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.MyRecyclerAdapter;
import com.vipulmedcare.h3u.adapter.OnlineRvAdapter;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.pojo.listitem;
import com.vipulmedcare.h3u.view.text.CustomTextView;
import com.vipulmedcare.h3u.view.text.CustomTextViewBold;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew pj on 8/21/2017.
 */

public class OnlineOpinionActivity extends BaseActivity implements View.OnClickListener{

    private ImageButton tool_tacker_main_cross_btn;
    private Context mContext;
    private OnlineRvAdapter onlineRvAdapter;
    private RecyclerView rv_online_opinion;
    TextView tv_headertext,tv_dcard,tvb_corpatet,tv_medicaltop,tv_first, tv_available,tv_opinion,tv_used,tv_availables,tv_usedt,tv_online_opinion;
    private MyRecyclerAdapter adapter;
    private List<listitem> movieList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.online_opinions);

        mContext = OnlineOpinionActivity.this;

        initViews();
    }

    public void initViews(){
        //    .......  heading .... ................        //
        tv_headertext =(TextView) findViewById(R.id.tv_headertext);
        tv_headertext.setText("ONLINE OPINION");

        tool_tacker_main_cross_btn= (ImageButton) findViewById(R.id.tool_tacker_main_cross_btn);
        tool_tacker_main_cross_btn.setOnClickListener(this);

        tvb_corpatet= (CustomTextView) findViewById(R.id.tvb_corpatet);
        tv_dcard= (CustomTextView) findViewById(R.id.tv_dcard);
        tv_first= (CustomTextView) findViewById(R.id.tv_first);
        tv_medicaltop= (CustomTextViewBold) findViewById(R.id.tv_medicaltop);

        tv_opinion= (CustomTextViewBold) findViewById(R.id.tv_opinion);
        //  ArrayList<String> verticalList=new ArrayList<>();
        tv_available= (CustomTextView) findViewById(R.id.tv_available);
        tv_used= (CustomTextView) findViewById(R.id.tv_used);
        tv_availables= (CustomTextView) findViewById(R.id.tv_availables);
        tv_usedt= (CustomTextView) findViewById(R.id.tv_usedt);
        tv_online_opinion= (CustomTextView) findViewById(R.id.tv_online_opinion);









//        onlineRvAdapter = new VerticalAdapter(verticalList,mContext);


      /*  LinearLayoutManager verticalLayoutmanager
                = new LinearLayoutManager(OnlineOpinionActivity.this, LinearLayoutManager.VERTICAL, false);
        rv_online_opinion.setLayoutManager(verticalLayoutmanager);

        rv_online_opinion.setAdapter(onlineRvAdapter);*/


        rv_online_opinion= (RecyclerView) findViewById(R.id.rv_online_opinion);
        adapter = new MyRecyclerAdapter(movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_online_opinion.setLayoutManager(mLayoutManager);
        rv_online_opinion.setItemAnimator(new DefaultItemAnimator());
        rv_online_opinion.setAdapter(adapter);

        prepareMovieData();


    }

    private void prepareMovieData() {
        listitem movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        adapter.notifyDataSetChanged();
    }
    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.tool_tacker_main_cross_btn:
                finish();
                break;

        }

    }
}
