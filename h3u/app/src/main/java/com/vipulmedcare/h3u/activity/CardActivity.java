package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.base.BaseActivity;


/**
 * Created by vipul on 8/11/2017.
 */

public class CardActivity extends BaseActivity {

    private static ViewPager mPager;
    private Context mContext;
    Button chs_spl_nxt;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private TextView card_activity_heading,tv_card_num_1,tv_card_num_2,tv_card_num_3,tv_card_num_4,tv_card_num_5,tv_card_num_6;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.card_activity_layout);
                mContext=CardActivity.this;


        initView();
    }

    public void initView(){
        mPager = (ViewPager)findViewById(R.id.pager);
        chs_spl_nxt = (Button) findViewById(R.id.chs_spl_nxt);
        chs_spl_nxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent CardDetail = new Intent(mContext, CardDetail.class);
                startActivity(CardDetail);
            }
        });


        card_activity_heading = (TextView) findViewById(R.id.card_activity_heading);
        tv_card_num_1= (TextView) findViewById(R.id.tv_card_num_1);
        tv_card_num_2= (TextView) findViewById(R.id.tv_card_num_2);
        tv_card_num_3= (TextView) findViewById(R.id.tv_card_num_3);
        tv_card_num_4= (TextView) findViewById(R.id.tv_card_num_4);
        tv_card_num_5= (TextView) findViewById(R.id.tv_card_num_5);
        tv_card_num_6= (TextView) findViewById(R.id.tv_card_num_6);
//        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
//        Typeface fontHeading = Typeface.createFromAsset(getAssets(), "fonts/Lato-Bold.ttf");
//        tv_card_num_1.setTypeface(font);
//        tv_card_num_2.setTypeface(font);
//        tv_card_num_3.setTypeface(font);
//        tv_card_num_4.setTypeface(font);
//        tv_card_num_5.setTypeface(font);
//        tv_card_num_6.setTypeface(font);
//        card_activity_heading.setTypeface(fontHeading);
    }


//    private void showDiscount() {
//        if (NetworkUtil.isNetworkAvailable(mContext)) {
//            final Call<TestimonialMResponse> getTestimonial = CustomApplication.getRestClient().getService().getTestimonials();
//            getTestimonial.enqueue(new Callback<TestimonialMResponse>() {
//                @Override
//                public void onResponse(Response<TestimonialMResponse> response, Retrofit retrofit) {
//
//                    if (response.body().getSuccess()) {
//
//                        ArrayList<TestimonialResponce> list = response.body().getData();
//
//
//                        ArrayList<String> text = new ArrayList<String>();
//                        for (int i = 0; i < list.size(); i++) {
//                            String text1 = response.body().getData().get(i).getTestim();
//                            text.add(text1);
//                        }
//                        mPager.setAdapter(new sliding_card_adapter(mContext, text));
//                        CirclePageIndicator indicator = (CirclePageIndicator)findViewById(R.id.indicator);
//                        indicator.setViewPager(mPager);
//                        try {
//                            final float density = getResources().getDisplayMetrics().density;
////Set circle indicator radius
//                            indicator.setRadius(5 * density);
//
//                            NUM_PAGES = text.size();
//
//                            // Auto start of viewpager
//                            final Handler handler = new Handler();
//                            final Runnable Update = new Runnable() {
//                                public void run() {
//                                    if (currentPage == NUM_PAGES) {
//                                        currentPage = 0;
//                                    }
//                                    mPager.setCurrentItem(currentPage++, true);
//                                }
//                            };
//                            Timer swipeTimer = new Timer();
//                            swipeTimer.schedule(new TimerTask() {
//                                @Override
//                                public void run() {
//                                    handler.post(Update);
//                                }
//                            }, 10000, 10000);
//
//                            // Pager listener over indicator
//                            indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//
//                                @Override
//                                public void onPageSelected(int position) {
//                                    currentPage = position;
//
//                                }
//
//                                @Override
//                                public void onPageScrolled(int pos, float arg1, int arg2) {
//
//                                }
//
//                                @Override
//                                public void onPageScrollStateChanged(int pos) {
//
//                                }
//                            });
//                        } catch (Exception e) {
//
//                        }
//
//                    }
//                }
//
//            });
//
//        } else {
////            Utils.showNetworkSettingAlert(mContext);
//        }
//    }
}
