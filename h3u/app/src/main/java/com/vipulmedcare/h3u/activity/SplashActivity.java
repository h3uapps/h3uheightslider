package com.vipulmedcare.h3u.activity;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.registration.Activity_Signup;
import com.vipulmedcare.h3u.view.animator.Animator;


public class SplashActivity extends BaseActivity {
    RelativeLayout logo;
    TextView txtLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_activity);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        this.buildLogo();
    }

    private void buildLogo(){

        this.logo = (RelativeLayout) findViewById(R.id.logo);

        ImageView base = (ImageView) logo.findViewById(R.id.logo_base_bg);
        base.setVisibility(View.INVISIBLE);
        ImageView outline = (ImageView) logo.findViewById(R.id.logo_outline);
        base.setVisibility(View.INVISIBLE);
        RelativeLayout wings = (RelativeLayout) logo.findViewById(R.id.logo_wings);
        wings.setVisibility(View.INVISIBLE);
        ImageView wingSpl = (ImageView) logo.findViewById(R.id.logo_wings_spl);
        wings.setVisibility(View.INVISIBLE);


       /* int[] anim = {R.anim.fadein, R.anim.scale_up_from_center,R.anim.rotation};
        int[] time = {500,750,1000};
        View[] views = {base,wings,outline,wingSpl};
        Animator.animate(anim,views,time,0);

        ImageView txt = (ImageView) logo.findViewById(R.id.logo_txt_color);
        txt.setVisibility(View.INVISIBLE);
        Animator.animate(R.anim.fadein,txt,400,1500);
         final ImageView icon = (ImageView) logo.findViewById(R.id.logo_icon);
        icon.setVisibility(View.INVISIBLE);
        Animator.animate(R.anim.fadein,icon,400,2500);

        TextView txtWelcome = (TextView) findViewById(R.id.txtWelcome);
        txtWelcome.setVisibility(View.INVISIBLE);
        Animator.animate(R.anim.push_up_in,txtWelcome,750,3500);*/

        final TextView txtWelcome = (TextView) findViewById(R.id.txthealthy);
        final TextView txthabits = (TextView) findViewById(R.id.txthabits);
        //txtWelcome.setVisibility(View.INVISIBLE);
        //TranslateAnimation t = (TranslateAnimation) AnimationUtils.loadAnimation(AppContext.getContext(),R.anim.move_up);
        //Animation a = AnimationUtils.loadAnimation(AppContext.getContext(),R.anim.move_up);
        //TranslateAnimation t = new TranslateAnimation()
        /*int[] anim = {R.anim.fadeout,R.anim.move_up};
        int[] time = {500,500};
        Animator.animate(anim,txtWelcome,time,500);*/



        Animator.animate(R.anim.push_up_in,txtWelcome,750,500)
                .setOnCompleteListener(new Animator.OnCompleteListener() {
            @Override
            public void onComplete() {
                Animator.animate(R.anim.push_up_out,txtWelcome,750,1500);
            }
        });

        Animator.animate(R.anim.push_up_in,txthabits,850,600)
                .setOnCompleteListener(new Animator.OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        Animator.animate(R.anim.push_up_out,txthabits,850,1500);
                    }
                });

        final TextView thealthy = (TextView) findViewById(R.id.thealthy);
        Animator.animate(R.anim.push_up_in,txthabits,850,600)
                .setOnCompleteListener(new Animator.OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        Animator.animate(R.anim.push_up_out,thealthy,850,1500);
                    }
                });
        final TextView textyou = (TextView) findViewById(R.id.textyou);
        Animator.animate(R.anim.push_up_in,txthabits,850,600)
                .setOnCompleteListener(new Animator.OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        Animator.animate(R.anim.push_up_out,textyou,850,1500);
                    }
                });
        final TextView txtLogo = (TextView) findViewById(R.id.txtLogo);
        Animator.animate(R.anim.push_up_in,txthabits,850,600)
                .setOnCompleteListener(new Animator.OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        Animator.animate(R.anim.move_down,txtLogo,850,1500);
                    }
                });

     /*   Techniques technique = (Techniques) view.getTag();
        rope = YoYo.with(technique)
                .duration(1200)
                .repeat(YoYo.INFINITE)
                .pivot(YoYo.CENTER_PIVOT, YoYo.CENTER_PIVOT)
                .interpolate(new AccelerateDecelerateInterpolator())*/

      /*  TextView txthealthy= (TextView) findViewById(R.id.txthealthy);
       // txthealthy.setVisibility(View.INVISIBLE);
        //Animator.animate(R.anim.rotation_loop,txthealthy,3000,4000);
        int[] anim = {R.anim.rotation_loop};
        int[] time = {500};
        Animator.animate(anim,txthealthy,time,500);*/



 /*
        TextView txthabits= (TextView) findViewById(R.id.txthabits);
        txthabits.setVisibility(View.INVISIBLE);
        Animator.animate(R.anim.push_right_in,txthabits,3000,4000);
      //  Animator.animate(R.anim.fadeout,txthabits,2000,4000);


        TextView thealthy= (TextView) findViewById(R.id.thealthy);
        thealthy.setVisibility(View.INVISIBLE);
        Animator.animate(R.anim.rotation_loop,thealthy,3000,4000);
      //  Animator.animate(R.anim.fadeout,thealthy,2000,4000); // out

        txtLogo = (TextView) findViewById(R.id.txtLogo);
        txtLogo.setVisibility(View.INVISIBLE);
        Animator.animate(R.anim.push_up_in,txtLogo,7000,4000);

        TextView textyou= (TextView) findViewById(R.id.textyou);
        textyou.setVisibility(View.INVISIBLE);
        Animator.animate(R.anim.push_down_in,textyou,3000,4000);
      //  Animator.animate(R.anim.fadeout,textyou,2000,4000);*/

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

              //  Utils.showToast(getApplicationContext(),"login");
               // showLoginActivity();
                Intent in = new Intent(SplashActivity.this, Activity_Signup.class);
                startActivity(in);
                finish();
            }
        }, 1000);
    }

    private void showLoginActivity(){
        //startActivity(new Intent(this, ActivityWebView.class));

       // this.controller.retrieveUserFromPrefs();
      //  User user = User.getInstance();
        ActivityOptionsCompat options = null;

        Intent i =new Intent(SplashActivity.this,Activity_Signup.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,p1,p2, getString(R.string.activity_logo_trans));
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,logo,getString(R.string.activity_logo_trans));
        }

        Pair<View, String> p1 = Pair.create((View) logo, "logo");
        Pair<View, String> p2 = Pair.create((View) txtLogo, "txtLogo");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
             //options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,p1,p2, getString(R.string.activity_logo_trans));
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,p1,p2);
        }
        //startActivity(intent, options != null ? options.toBundle() : null);
        startActivity(i);

    }


}
