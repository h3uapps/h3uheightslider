package com.vipulmedcare.h3u.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.Opd_Consult_Adapter;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.pojo.listitem;
import com.vipulmedcare.h3u.view.text.CustomTextView;
import com.vipulmedcare.h3u.view.text.CustomTextViewBold;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew pj on 8/21/2017.
 */

public class Activity_Opd_Consult extends BaseActivity {

    RecyclerView rv_mytest;
    ProgressDialog pd;
    Context mContext;
    private Opd_Consult_Adapter adapter;
    TextView tv_perious,tv_headertext,tv_share,tv_cross,tv_getconsulation,tv_opdname,tv_available_count,tv_used,tv_cardname,tv_price,tv_expire_dob,tv_holder_name;

    private List<listitem> movieList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_opd_consult);

        mContext = Activity_Opd_Consult.this;
        initViews();
    }

    private void initViews() {

        //    .......  heading .... ................        //
        tv_headertext =(TextView) findViewById(R.id.tv_headertext);
        tv_headertext.setText("OPD CONSULTATION");
        tv_cross= (TextView) findViewById(R.id.tv_cross);

        tv_getconsulation= (CustomTextView) findViewById(R.id.tv_getconsulation);
        tv_available_count= (CustomTextView) findViewById(R.id.tv_available_count);
        tv_used= (CustomTextView) findViewById(R.id.tv_used);

        tv_cardname= (CustomTextViewBold) findViewById(R.id.tv_cardname);
        tv_perious= (CustomTextViewBold) findViewById(R.id.tv_perious);
        tv_opdname= (CustomTextViewBold) findViewById(R.id.tv_opdname);

        tv_price= (TextView) findViewById(R.id.tv_price);
        tv_expire_dob= (TextView) findViewById(R.id.tv_expire_dob);
        tv_holder_name= (TextView) findViewById(R.id.tv_holder_name);


        tv_share= (TextView) findViewById(R.id.tv_share);
        rv_mytest= (RecyclerView) findViewById(R.id.rv_mytest);



        adapter = new Opd_Consult_Adapter(mContext,movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_mytest.setLayoutManager(mLayoutManager);
        rv_mytest.setItemAnimator(new DefaultItemAnimator());
        rv_mytest.setAdapter(adapter);


        tv_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        prepareMovieData();

      /*  pd = new ProgressDialog(Activity_Testresult.this,
                AlertDialog.THEME_HOLO_LIGHT);
        pd.setMessage("Loading...");
        pd.setIndeterminate(false);
        pd.setCancelable(false);
        pd.show();

        if (NetworkUtil.isNetworkAvailable(mContext)) {
                pd.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.APP_TESTRESULT,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                System.out.println(response);
                                //   Log.d("Response volley", response);
                                pd.dismiss();
                                try {
                                    JSONObject json = new JSONObject(response);
                                    System.out.println(response);
                                    String check = json.getString("success");

                                    if (check.toLowerCase().equals("true")) {
                                        JSONArray pm = json.getJSONArray("data");
                                        JSONObject c = pm.getJSONObject(0);
                                        String id = c.getString("Id");

                                       *//* List<TestData> list = response.body().getData();
                                        adapter = new myTestAdapter(mContext, getActivity(), list);
                                        adapter.notifyDataSetChanged();*//*

                                    } else {
                                        Utils.showToast(mContext, json.getString("message"));

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //  mProgressDialog.dismiss();
                                pd.dismiss();
                                Toast.makeText(Activity_Testresult.this, "Error, Please try again" + error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        })

                {
                    //This is for Headers If You Needed
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        String creds = String.format("%s:%s", "VIL1XuqUVnkq31L2PdCtjw==", "zxdwT819BSOhT9pmnadjeg==");
                        String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                        params.put("Authorization", auth);
                        return params;
                    }

                    //Pass Your Parameters here
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("UserName", "andrew@gmail.com");
                        System.out.println("Login SendData" + params);
                        return params;

                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(stringRequest);
            }
*/

    }

    private void prepareMovieData() {
        listitem movie = new listitem("Name of the Specialization", "Dr. Andrew pj", "17 AUG 2017");
        movieList.add(movie);

        movie = new listitem("Name of the Specialization", "Dr. Andrew pj", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Name of the Specialization", "Dr. Andrew pj", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Name of the Specialization", "Dr. Andrew pj", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Name of the Specialization", "Dr. Andrew pj", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Name of the Specialization", "Dr. Andrew pj", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Dr. Andrew pj", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        adapter.notifyDataSetChanged();
    }
}