package com.vipulmedcare.h3u.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.base.BaseActivity;


/**
 * Created by vipul on 8/31/2017.
 */

public class Select_Type_Opinion extends BaseActivity {

    RelativeLayout rl_first_opinion,rl_second_opinion;
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.select_type_opinion);

        rl_first_opinion= (RelativeLayout) findViewById(R.id.rl_first_opinion);
        rl_second_opinion= (RelativeLayout) findViewById(R.id.rl_second_opinion);
        rl_first_opinion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(Select_Type_Opinion.this,First_Opinion.class);
                startActivity(i);
            }
        });
        rl_second_opinion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  Intent i =new Intent(Select_Type_Opinion.this,First_Opinion.class);
                startActivity(i);*/
            }
        });

    }
}
