package com.vipulmedcare.h3u.utils;

import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.widget.EditText;

import java.util.regex.Pattern;

/**
 * Created by Prem mali on 5/30/2017.
 */

public class Validation {


    private static final String DATE_PATTERN= "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)";
    private static final String NAME_REGEX = "^[a-zA-Z ]{1,}$";
    private static final String NAME_REGEX1 = "^[a-zA-Z0-9]{1,}$";
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PHONE_REGEX = "^[6-9][0-9]{9}$";
    private static final String PHONE_LENGTH_REGEX = "^[0-9]{10}$";
    private static final String PINCODE_REGEX = "^[0-9]{6}$";
    private static final String PANNUMBER_REGEX = "[A-Za-z]{5}\\d{4}[A-Za-z]{1}";
    private static final String PASSWORD_REGEX = "^[a-zA-Z0-9]{6,}$";//alphanumeric password min 6 charcter long
    private static final String SAPCODE_REGEX = "^[0-9]{7}$";
    private static final String EMP_CODE_REGEX = "^[a-zA-Z0-9@.]{1,}$";//at least 3 characters needed or alphanumeric
    private static final String GROUP_CODE_REGEX = "^[a-zA-Z0-9]{1,}$";//at least 1 digit needed
    private static final String Addhar = "\\d{12}";
    private static final String VOTER_ID = "[A-Z]{3}\\d{7}";
    private static final String passport ="(([a-zA-Z])\\d{8})";
    //private static final String Passport=;

    private static final String PANNUMBER = "\"[A-Z]{5}[0-9]{4}[A-Z]{1}\")";
    private static final String REQUIRED_MSG = "Required";
    private static final String NOT_SAME = "Password Not Match";
    private static final String INVALID_NAME_MSG = "Invalid name";
    private static final String INVALID_NAME_MSG1 = "Invalid Kms";
    private static final String INVALID_EMAIL_MSG = "Invalid email address";
    private static final String INVALID_PASSOWRD_MSG = "Invalid password";
    private static final String INVALID_PINCODE_MSG = "Invalid pin code";
    private static final String INVALID_SAP_MSG = "Invalid sap code";
    private static final String INVALID_EMPCODE_MSG = "Invalid Employee code";
    private static final String INVALID_GROUPCODE_MSG = "Invalid Group code";
    private static final String INVALID_PHONE_MSG = "Invalid phone number";
    private static final String INVALID_PHONE_LENGTH_MSG= "Invalid Mobile Number";
    private static final String INVALID_PAN_MSG = "Invalid pan number";
    private static final String WEIGHT="^[0-9]{2}$";
    private static final String MSG = "Invalid weight";
    private static final String dateformate = "Invalid formate";

    public static boolean dob(TextInputLayout textInputLayout, EditText editText, boolean required) {
        return isValid(textInputLayout, editText, DATE_PATTERN, dateformate, required);
    }
    public static boolean isnumberweight(TextInputLayout textInputLayout, EditText editText, boolean required) {
        return isValid(textInputLayout, editText, WEIGHT, MSG, required);
    }
    public static boolean isName(TextInputLayout textInputLayout, EditText editText, boolean required) {
        return isValid(textInputLayout, editText, NAME_REGEX, INVALID_NAME_MSG, required);
    }

    public static boolean isName1(TextInputLayout textInputLayout, EditText editText, boolean required) {
        return isValid(textInputLayout, editText, NAME_REGEX1, INVALID_NAME_MSG, required);
    }

    public static boolean isEmailAddress(TextInputLayout textInputLayout, EditText editText, boolean required) {
        return isValid(textInputLayout, editText, EMAIL_REGEX, INVALID_EMAIL_MSG, required);
    }

    public static boolean isAddress(TextInputLayout textInputLayout, EditText editText,boolean required) {
        return hasText(textInputLayout, editText);
    }

    public static boolean isPhoneNumber(TextInputLayout textInputLayout, EditText editText, boolean required) {
        return isValid(textInputLayout, editText, PHONE_REGEX, INVALID_PHONE_MSG, required);
    }
    public static boolean isPhoneLength(TextInputLayout textInputLayout, EditText editText, boolean required) {
        return isValid(textInputLayout, editText, PHONE_LENGTH_REGEX, INVALID_PHONE_LENGTH_MSG, required);
    }
    public static boolean isPanNumber(TextInputLayout textInputLayout, EditText editText, boolean required) {
        return isValid(textInputLayout, editText, PANNUMBER_REGEX, INVALID_PAN_MSG, required);
    }

    //
    public static boolean isVoterNumber(TextInputLayout textInputLayout, EditText editText, boolean required) {
        return isValid(textInputLayout, editText, VOTER_ID, dateformate, required);
    }
    //\\d{12}
    public static boolean isPassportNumber(TextInputLayout textInputLayout, EditText editText, boolean required) {
        return isValid(textInputLayout, editText, passport, dateformate, required);
    }
    public static boolean isAadharNumber(TextInputLayout textInputLayout, EditText editText, boolean required) {
        return isValid(textInputLayout, editText, Addhar, dateformate, required);
    }
    //
    public static boolean isPassword(TextInputLayout textInputLayout, EditText editText, boolean required) {
        return isValid(textInputLayout, editText, PASSWORD_REGEX, INVALID_PASSOWRD_MSG, required);
    }
    public static boolean isConfirmPassword(TextInputLayout textInputLayout, EditText editText1, EditText editText2) {
        return isSame(textInputLayout, editText1, editText2);
    }
    public static boolean isPincode(TextInputLayout textInputLayout, EditText editText, boolean required) {
        return isValid(textInputLayout, editText, PINCODE_REGEX, INVALID_PINCODE_MSG, required);
    }

    public static boolean isSapCode(TextInputLayout textInputLayout, EditText editText, boolean required) {
        return isValid(textInputLayout, editText, SAPCODE_REGEX, INVALID_SAP_MSG, required);
    }
    public static boolean isEmpCode(TextInputLayout textInputLayout, EditText editText, boolean required) {
        return isValid(textInputLayout, editText, EMP_CODE_REGEX, INVALID_EMPCODE_MSG, required);
    }

    public static boolean isGroupCode(TextInputLayout textInputLayout, EditText editText, boolean required) {
        return isValid(textInputLayout, editText, GROUP_CODE_REGEX, INVALID_GROUPCODE_MSG, required);
    }
    public static boolean isDateOfBirth(TextInputLayout textInputLayout, EditText editText) {
        return hasText(textInputLayout, editText);
    }

    public static boolean isNull(TextInputLayout textInputLayout, EditText editText, boolean b)
    {if (TextUtils.isEmpty(editText.getText())) {
        textInputLayout.setError(REQUIRED_MSG);
        return false;
    } else {
        textInputLayout.setErrorEnabled(false);
    }
        return true;
    }

    private static boolean isValid(TextInputLayout textInputLayout, EditText editText, String regex, String errMsg, boolean required) {

        String text = editText.getText().toString().trim();
        textInputLayout.setError(null);

        if (required && !hasText(textInputLayout, editText)) return false;

        if (required && !Pattern.matches(regex, text)) {
            textInputLayout.setError(errMsg);
            return false;
        } else {
            textInputLayout.setErrorEnabled(false);
        }

        return true;
    }

    public static boolean hasText(TextInputLayout textInputLayout, EditText editText) {
        if (TextUtils.isEmpty(editText.getText())) {
            textInputLayout.setError(REQUIRED_MSG);
            return false;
        } else {
            textInputLayout.setErrorEnabled(false);
        }
        return true;
    }

    public static boolean isSame(TextInputLayout textInputLayout, EditText editText1, EditText editText2) {
        if (editText1.getText().toString().equals(editText2.getText().toString())) {

            textInputLayout.setErrorEnabled(false);

        } else {
            textInputLayout.setError(NOT_SAME);
            return false;
        }
        return true;
    }
}
