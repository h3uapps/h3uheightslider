package com.vipulmedcare.h3u.pojo;

/**
 * Created by vipul on 5/11/2017.
 */

public class CorporateDataNew {


    public CorporateDataNew(String CORPORATENAME, String CORPORATEEMAIL, String EMPID) {

        this.CORPORATENAME = CORPORATENAME;
        this.CORPORATEEMAIL = CORPORATEEMAIL;
        this.EMPID = EMPID;
    }

    public String getCORPORATENAME() {
        return CORPORATENAME;
    }

    public void setCORPORATENAME(String CORPORATENAME) {
        this.CORPORATENAME = CORPORATENAME;
    }

    public String getCORPORATEEMAIL() {
        return CORPORATEEMAIL;
    }

    public void setCORPORATEEMAIL(String CORPORATEEMAIL) {
        this.CORPORATEEMAIL = CORPORATEEMAIL;
    }

    public String getEMPID() {
        return EMPID;
    }

    public void setEMPID(String EMPID) {
        this.EMPID = EMPID;
    }

    private String CORPORATENAME;
    private String CORPORATEEMAIL;
    private String EMPID;




}
