package com.vipulmedcare.h3u.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.activity.ChatInterface;
import com.vipulmedcare.h3u.activity.Provider_detail_Activity;

import java.util.List;

/**
 * Created by vipul on 9/12/2017.
 */

public class ChatAdapter   extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {

    private List<String> verticalList;
    private Context mContext;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private  LinearLayout chat_option1;

        public MyViewHolder(View view) {
            super(view);
            chat_option1 = (LinearLayout) view.findViewById(R.id.chat_option1);

        }
    }


    public ChatAdapter(List<String> verticalList, Context mContext) {
        this.verticalList = verticalList;
        this.mContext=mContext;
    }

    @Override
    public ChatAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_options, parent, false);

        return new ChatAdapter.MyViewHolder(itemView);
    }




    @Override
    public void onBindViewHolder(final ChatAdapter.MyViewHolder holder, final int position) {

        holder.chat_option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext,ChatInterface.class);
                mContext.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return verticalList.size();
    }
}
