package com.vipulmedcare.h3u.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.utils.ApiConstants;
import com.vipulmedcare.h3u.utils.NetworkUtil;
import com.vipulmedcare.h3u.utils.Validation;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andrew on 7/14/2017.
 */

public class SignUp_Activity extends BaseActivity implements View.OnClickListener {

    TextView toolbarText;
    private Context mContext;
    ImageView back;
    private View mView;
    private Button btn_signup;
    private String username;
    private Boolean eye_open = false;
    private Boolean eye_open1 = false;
    private TextInputLayout til_name, til_email, til_password, til_conf_password, til_lname,til_usermname;
    private EditText et_name, et_lname, et_email, et_password, et_conf_password,et_usermname;



    ProgressDialog mProgressDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_signup);

        //    .......  heading .... ................        //
        toolbarText =(TextView) findViewById(R.id.tv_toolbarText);
        toolbarText.setText("SignUp");

        mContext = SignUp_Activity.this;

        initviews();

    }

    private void initviews() {

        til_name = (TextInputLayout)findViewById(R.id.til_username);
        et_name = (EditText)findViewById(R.id.et_username);
        et_name.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                Validation.isName(til_name, et_name, false);
            }
        });

        til_usermname= (TextInputLayout) findViewById(R.id.til_usermname);
        et_usermname= (EditText) findViewById(R.id.et_usermname);

        til_lname = (TextInputLayout)findViewById(R.id.til_userlname);
        et_lname = (EditText)findViewById(R.id.et_userlname);
        et_lname.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                Validation.isName(til_lname, et_lname, false);
            }
        });

        til_email = (TextInputLayout)findViewById(R.id.til_email);
        et_email = (EditText)findViewById(R.id.et_email);
        et_email.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                Validation.isEmailAddress(til_email, et_email, false);
            }
        });

        final TextView tv_showHide = (TextView)findViewById(R.id.tv_showHide);
        tv_showHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eye_open) {
                    tv_showHide.setText("SHOW");
                    et_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    eye_open = false;
                } else {
                    tv_showHide.setText("HIDE");
                    et_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    eye_open = true;
                }
                et_password.setSelection(et_password.getText().length());
            }
        });
        tv_showHide.setVisibility(View.GONE);
        //tv_showHide.setVisibility(View.VISIBLE);

        til_password = (TextInputLayout)findViewById(R.id.til_password);
        et_password = (EditText)findViewById(R.id.et_password);
        et_password.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (et_password.getText().toString().trim().equals("")) {
                    //  tv_showHide.setVisibility(View.VISIBLE);
                    tv_showHide.setVisibility(View.GONE);
                } else {
                    tv_showHide.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                Validation.isPassword(til_password, et_password, false);
            }
        });

        final TextView tv_showHide1 = (TextView)findViewById(R.id.tv_showHide1);
        tv_showHide1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eye_open1) {
                    tv_showHide1.setText("SHOW");
                    et_conf_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    eye_open1 = false;
                } else {
                    tv_showHide1.setText("HIDE");
                    et_conf_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    eye_open1 = true;
                }
                et_conf_password.setSelection(et_conf_password.getText().length());
            }
        });
        // tv_showHide1.setVisibility(View.VISIBLE);
        tv_showHide1.setVisibility(View.GONE);
        til_conf_password = (TextInputLayout)findViewById(R.id.til_confirmpassword);
        et_conf_password = (EditText)findViewById(R.id.et_confirmpassword);
        et_conf_password.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (et_conf_password.getText().toString().trim().equals("")) {
                    // tv_showHide1.setVisibility(View.VISIBLE);
                    tv_showHide1.setVisibility(View.GONE);
                } else {
                    tv_showHide1.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                Validation.isConfirmPassword(til_conf_password, et_password, et_conf_password );
            }
        });
        btn_signup = (Button)findViewById(R.id.btn_signup);
        btn_signup.setOnClickListener(this);

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signup:
               // if (checkValidation()) {
                    signup();
              //  }
                break;
        }
    }
    private void signup() {
        mProgressDialog = new ProgressDialog(SignUp_Activity.this,
                AlertDialog.THEME_HOLO_LIGHT);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        showProgressDialog("Please wait...");
        String Confirm_password = et_conf_password.getText().toString().trim();

        final String LoginType="1";
        final String SocialId="";
        final String SocialImg="";
        final String CorporateId="";

        if (NetworkUtil.isNetworkAvailable(mContext)) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.APP_usewisecard,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                System.out.println(response);
                             //   Log.d("Response volley", response);
                                Toast.makeText(SignUp_Activity.this,response,Toast.LENGTH_LONG).show();
                                mProgressDialog.dismiss();
                                try {

                                    JSONObject json = new JSONObject(response);
                                    System.out.println(response);

                                    if (json.getInt("success") == 1) {
                                        JSONArray pm = json.getJSONArray("data");
                                        for (int i = 0; i < pm.length(); i++) {
                                        }
                                       // String FirstName = json.getJSONObject("data").getString("FirstName");
                                        Toast.makeText(SignUp_Activity.this, "Signup Successful!", Toast.LENGTH_LONG).show();

                                    } else {
                                        Toast.makeText(SignUp_Activity.this, "Invalid  Signup", Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                              //  mProgressDialog.dismiss();
                                Toast.makeText(SignUp_Activity.this, "Error, Please try again"+error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        })
                {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        String creds = String.format("%s:%s","VIL1XuqUVnkq31L2PdCtjw==","zxdwT819BSOhT9pmnadjeg==");
                        String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                        params.put("Authorization",auth);

                        return params;
                    }
                    //Pass Your Parameters here
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("FirstName",et_name.getText().toString().trim());
                        params.put("MiddleName",et_usermname.getText().toString().trim());
                        params.put("LastName",et_lname.getText().toString().trim());
                        params.put("UserName",et_email.getText().toString().trim());
                        params.put("Password",et_password.getText().toString().trim());
                        params.put("LoginType",LoginType);
                        params.put("SocialId",SocialId);
                        params.put("SocialImg",SocialImg);
                        System.out.println("Login SendData" + params);
                        return params;


                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(stringRequest);
            }}


    private boolean checkValidation() {
        boolean result = true;

        if (!Validation.isName(til_name, et_name, true)) result = false;
        if (!Validation.isAddress(til_lname, et_lname, true)) result = false;
        if (!Validation.isEmailAddress(til_email, et_email, true)) result = false;
        if (!Validation.isPassword(til_password, et_password, true)) result = false;
        if (!Validation.isConfirmPassword(til_conf_password, et_password, et_conf_password)) result = false;

        return result;
    }
}
