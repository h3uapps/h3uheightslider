package com.vipulmedcare.h3u.registration;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.utils.ApiConstants;
import com.vipulmedcare.h3u.utils.NetworkUtil;
import com.vipulmedcare.h3u.utils.Utils;
import com.vipulmedcare.h3u.utils.Validation;
import com.vipulmedcare.h3u.view.text.CustomEditText;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andrew pj on 8/2/2017.
 */

public class Activity_Signup extends BaseActivity implements View.OnClickListener {
    Button btn_login;
    TextView toolbarText;
    EditText edit_firstname,edit_lstname;
    TextInputLayout til_first,til_last;
    private Context mContext;
    String firstname, lastname;
    public static final String EXTRA_MESSAGE = "username";
    ProgressDialog pd;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mContext = Activity_Signup.this;
        initviews();
    }

    private void initviews() {

        til_first = (TextInputLayout)findViewById(R.id.til_first);

        edit_firstname= (CustomEditText) findViewById(R.id.edit_firstname);
       /* Typeface m= Typeface.createFromAsset(getAssets(),"fonts/HelveticaLTStd-Light.otf");
        edit_firstname.setTypeface(m);*/

        edit_firstname.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                Validation.isName(til_first, edit_firstname, false);
            }
        });

        til_last= (TextInputLayout) findViewById(R.id.til_last);
        edit_lstname= (CustomEditText) findViewById(R.id.edit_lstname);
       /* Typeface m1= Typeface.createFromAsset(getAssets(),"fonts/HelveticaLTStd-Light.otf");
        edit_lstname.setTypeface(m1);*/
        edit_lstname.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                Validation.isName(til_last, edit_lstname, false);
            }
        });

        btn_login= (Button) findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                 if (checkValidation()) {
                // signup();
                     Intent intent = new Intent(this, Activity_Selectgender.class);
                     String message1 = edit_firstname.getText().toString();
                     String message2 = edit_lstname.getText().toString();
                     String message = message1 + " " + message2;
                     intent.putExtra(EXTRA_MESSAGE, message);
                     startActivity(intent);

                     //
                  }
                break;
        }
    }

    private void signup() {

        pd = new ProgressDialog(Activity_Signup.this,
                AlertDialog.THEME_HOLO_LIGHT);
        pd.setMessage("Loading...");
        pd.setIndeterminate(false);
        pd.setCancelable(false);
        pd.show();

        firstname = edit_firstname.getText().toString().trim();
        lastname = edit_lstname.getText().toString().trim();
        final String LoginType = "1", CorporateId = "";

        if (NetworkUtil.isNetworkAvailable(mContext)) {
            if (checkValidation()) {
                pd.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.APP_Login,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                System.out.println(response);
                                //   Log.d("Response volley", response);
                                //  Toast.makeText(volley_test_login.this,response,Toast.LENGTH_LONG).show();
                                pd.dismiss();
                                try {
                                    JSONObject json = new JSONObject(response);
                                    System.out.println(response);
                                    String check = json.getString("success");

                                    if (check.toLowerCase().equals("true")) {
                                        JSONArray pm = json.getJSONArray("data");
                                        JSONObject c = pm.getJSONObject(0);
                                        String id = c.getString("Id");
                                        String UserName = c.getString("UserName");
                                        String Name = c.getString("Name");
                                        String UserType = c.getString("UserType");
                                        String IsProfileVerfied = c.getString("IsProfileVerfied");
                                        String ProfileImage = c.getString("ProfileImage");
                                        String DocOnCall = c.getString("DocOnCall");
                                        String IsTempPwd = c.getString("IsTempPwd");
                                        String Mobile = c.getString("Mobile");
                                        String Debug = c.getString("Debug");


                                   /* Utils.setPreference(mContext , PreferenceKeys.Mobile,Mobile);
                                    Utils.ClearPreferences(mContext);
                                    String  mob=Utils.getPreferences(mContext , PreferenceKeys.Mobile);*/

                                        Utils.showToast(mContext, "Hi " + Name + " Welcome to H3U");

                                      /*  Toast.makeText(Activity_Signup.this, "Login Successful! Location would be updated!", Toast.LENGTH_LONG).show();
                                        SharedPreferences.Editor editor = getSharedPreferences("MyPrefsFile", MODE_PRIVATE).edit();

                                        editor.putString("name", json.getJSONObject("data").getString("name"));
                                        editor.putString("number", json.getJSONObject("data").getString("phone"));
                                        editor.commit();

                                        Intent i = new Intent(Activity_Signup.this,Activity_Selectgender.class);
                                        i.putExtra("otp",json.getJSONObject("data").getString("otp"));
                                        startActivity(i);
                                        finish();
                                        */

                                    } else {
                                        Utils.showToast(mContext, json.getString("message"));
                                        // Toast.makeText(volley_test_login.this, "Wrong user name  ", Toast.LENGTH_LONG).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //  mProgressDialog.dismiss();
                                pd.dismiss();
                                Toast.makeText(Activity_Signup.this, "Error, Please try again" + error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        })

                {
                    //This is for Headers If You Needed
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        String creds = String.format("%s:%s", "VIL1XuqUVnkq31L2PdCtjw==", "zxdwT819BSOhT9pmnadjeg==");
                        String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                        params.put("Authorization", auth);
                        return params;
                    }

                    //Pass Your Parameters here
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        //  params.put("username","andrew@gmail.com");
                        params.put("UserName", firstname);
                        params.put("Password", lastname);
                        params.put("CorporateId", CorporateId);
                        params.put("LoginType", LoginType);
                        System.out.println("Login SendData" + params);
                        return params;

                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(stringRequest);
            }
        }
    }

    private boolean checkValidation() {
        boolean result = true;

        if (!Validation.isName(til_first, edit_firstname, true)) result = false;
        if (!Validation.isName(til_last, edit_lstname, true)) result = false;

        return result;
    }
}
