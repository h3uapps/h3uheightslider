package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.utils.Utils;
import com.vipulmedcare.h3u.view.text.CustomTextView;
import com.vipulmedcare.h3u.view.text.CustomTextViewBold;


/**
 * Created by Andrew Pj on 8/21/2017.
 */

public class Tools_tracker_main_Activity extends BaseActivity implements View.OnClickListener{

    private ImageButton tool_tacker_main_cross_btn;
    private Context mContext;
    private RelativeLayout tools_smoke_meter,rl_saltmeter,rl_mindmaster,rl_bodymass;
    TextView tv_headertext,tv_texttracker,tv_salt,tv_smoke,tv_mind_matter,tv_bmi,tv_close;
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.tools_tacker_main);

        mContext = Tools_tracker_main_Activity.this;
        initViews();
    }

 public void initViews(){
     //    .......  heading .... ................        //
     tv_headertext =(TextView) findViewById(R.id.tv_headertext);
     tv_headertext.setText("TOOL AND TRACKER");


     tv_close= (CustomTextViewBold) findViewById(R.id.tv_close);
     tv_bmi= (CustomTextView) findViewById(R.id.tv_bmi);
     tv_mind_matter= (CustomTextView) findViewById(R.id.tv_mind_matter);
     tv_smoke= (CustomTextView) findViewById(R.id.tv_smoke);
     tv_salt= (CustomTextView) findViewById(R.id.tv_salt);
     tv_texttracker= (CustomTextView) findViewById(R.id.tv_texttracker);

    /* Typeface p3 = Typeface.createFromAsset(mContext.getAssets(),"fonts/Lato-Regular.ttf");
     tv_texttracker.setTypeface(p3);

     Typeface p = Typeface.createFromAsset(mContext.getAssets(),"fonts/Lato-Regular.ttf");
     tv_bmi.setTypeface(p);

     Typeface p1= Typeface.createFromAsset(mContext.getAssets(),"fonts/Lato-Regular.ttf");
     tv_salt.setTypeface(p1);
     Typeface p2= Typeface.createFromAsset(mContext.getAssets(),"fonts/Lato-Regular.ttf");
     tv_mind_matter.setTypeface(p2);

     Typeface a= Typeface.createFromAsset(mContext.getAssets(),"fonts/Lato-Bold.ttf");
     tv_close.setTypeface(a);*/


     tool_tacker_main_cross_btn= (ImageButton) findViewById(R.id.tool_tacker_main_cross_btn);
     tool_tacker_main_cross_btn.setOnClickListener(this);

     rl_saltmeter= (RelativeLayout) findViewById(R.id.rl_saltmeter);
     tools_smoke_meter= (RelativeLayout) findViewById(R.id.tools_smoke_meter);
     rl_mindmaster= (RelativeLayout) findViewById(R.id.rl_mindmaster);
     rl_bodymass= (RelativeLayout) findViewById(R.id.rl_bodymass);

     tools_smoke_meter.setOnClickListener(this);

 }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.tool_tacker_main_cross_btn:
                finish();
                break;
            case R.id.tools_smoke_meter:
                Utils.showAlert(mContext,"Tools And tracker","Work in Progress");
                break;
        }

    }
}
