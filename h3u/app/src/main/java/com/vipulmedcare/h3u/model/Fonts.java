package com.vipulmedcare.h3u.model;

/**
 * Created by vjangra on 23/11/15.
 */
public class Fonts {
    static public String TYPO_LIGHT = "fonts/Roboto-Light.ttf";
    static public String TYPO_REGULAR = "fonts/Roboto-Regular.ttf";
    static public String TYPO_BOLD = "fonts/Roboto-Bold.ttf";
    static public String TYPO_ITALIC = "fonts/Roboto-Italic.ttf";
    static public String TYPO_CONDENSED = "fonts/RobotoCondensed-Regular.ttf";

    static public String TYPO_LOGO = "fonts/ColonnaMT.ttf";
    static public String TYPO_OSWALD = "fonts/Oswald-Regular.ttf";

    static public String TYPO_LATTO_REGULAR = "fonts/Lato-Regular.ttf";
    static public String TYPO_LATTO_BOLD = "fonts/Lato-Bold.ttf";
    static public String TYPO_LATTO_LIGHT = "fonts/Lato-Light.ttf";
    static public String TYPO_MEDIUM = "fonts/Lato-Medium.ttf";

}
