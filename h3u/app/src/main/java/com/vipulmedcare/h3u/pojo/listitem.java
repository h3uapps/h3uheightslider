package com.vipulmedcare.h3u.pojo;

/**
 * Created by vipul on 8/21/2017.
 */

public class listitem {
    private String title, genre, year;

    public listitem() {
    }

    public listitem(String title, String genre, String year) {
        this.title = title;
        this.genre = genre;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}