package com.vipulmedcare.h3u.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.activity.AddFamilyMember;

import java.util.List;

/**
 * Created by vipul on 9/4/2017.
 */

public class AddFamilyAdapter   extends RecyclerView.Adapter<AddFamilyAdapter.MyViewHolder> {

    private List<String> verticalList;
    private Context mContext;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private de.hdodenhof.circleimageview.CircleImageView iv_family_rv;


        public MyViewHolder(View view) {
            super(view);

            iv_family_rv= (de.hdodenhof.circleimageview.CircleImageView) view.findViewById(R.id.iv_family_rv);

        }
    }


    public AddFamilyAdapter(List<String> verticalList, Context mContext) {
        this.verticalList = verticalList;
        this.mContext=mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.add_family_rv, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

   holder.iv_family_rv.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View v) {
           Intent addMember  = new Intent(mContext, AddFamilyMember.class);
           mContext.startActivity(addMember);
       }
   });
    }

    @Override
    public int getItemCount() {
        return verticalList.size();
    }
}

