package com.vipulmedcare.h3u.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.myTestAdapter;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.pojo.listitem;
import com.vipulmedcare.h3u.view.text.CustomTextView;
import com.vipulmedcare.h3u.view.text.CustomTextViewBold;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew pj on 8/22/2017.
 */

public class Activity_Healthcheckup extends BaseActivity {

    TextView tv_healthcross;
    RecyclerView rv_mytest;
    ProgressDialog pd;
    Context mContext;
    private myTestAdapter adapter;
    TextView tv_share;
    private List<listitem> movieList = new ArrayList<>();
    TextView tv_card,tv_bpackage,tv_count,tv_used,tv_schedul_checkup,tv_headertext,tv_corporate,tv_perious;


    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_healthcheckup);

        mContext = Activity_Healthcheckup.this;
        initViews();

    }

    private void initViews() {

        mContext = Activity_Healthcheckup.this;
        //    .......  heading .... ................        //
        tv_headertext =(TextView) findViewById(R.id.tv_headertext);
        tv_headertext.setText("HEALTH CHECKUP");

        tv_perious= (CustomTextViewBold) findViewById(R.id.tv_perious);
        tv_corporate= (CustomTextView) findViewById(R.id.tv_corporate);
        tv_card= (CustomTextViewBold) findViewById(R.id.tv_card);
        tv_bpackage= (CustomTextViewBold) findViewById(R.id.tv_bpackage);


        tv_count= (CustomTextView) findViewById(R.id.tv_count);
        tv_used= (CustomTextView) findViewById(R.id.tv_used);
        tv_schedul_checkup= (CustomTextView) findViewById(R.id.tv_schedul_checkup);

        tv_healthcross= (TextView) findViewById(R.id.tv_healthcross);

        tv_share= (TextView) findViewById(R.id.tv_share);
        rv_mytest= (RecyclerView) findViewById(R.id.rv_mytest);

        Typeface p1 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Lato-Bold.ttf");
        tv_bpackage.setTypeface(p1);

        Typeface p3 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Lato.ttf");
        tv_used.setTypeface(p3);
        Typeface p4 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Lato-Bold.ttf");
        tv_card.setTypeface(p4);
        Typeface p6 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Lato.ttf");
        tv_schedul_checkup.setTypeface(p6);

        Typeface p5 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Lato.ttf");
        tv_corporate.setTypeface(p5);

        Typeface p7 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Lato-Bold.ttf");
        tv_perious.setTypeface(p7);


        adapter = new myTestAdapter(mContext,movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_mytest.setLayoutManager(mLayoutManager);
        rv_mytest.setItemAnimator(new DefaultItemAnimator());
        rv_mytest.setAdapter(adapter);

        tv_healthcross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });

        LinearLayout ll_name= (LinearLayout) findViewById(R.id.ll_name);
        ll_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(Activity_Healthcheckup.this,Activity_Healthcheckupsecond.class);
                startActivity(i);
            }
        });
        prepareMovieData();

      /*  pd = new ProgressDialog(Activity_Testresult.this,
                AlertDialog.THEME_HOLO_LIGHT);
        pd.setMessage("Loading...");
        pd.setIndeterminate(false);
        pd.setCancelable(false);
        pd.show();

        if (NetworkUtil.isNetworkAvailable(mContext)) {
                pd.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.APP_TESTRESULT,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                System.out.println(response);
                                //   Log.d("Response volley", response);
                                pd.dismiss();
                                try {
                                    JSONObject json = new JSONObject(response);
                                    System.out.println(response);
                                    String check = json.getString("success");

                                    if (check.toLowerCase().equals("true")) {
                                        JSONArray pm = json.getJSONArray("data");
                                        JSONObject c = pm.getJSONObject(0);
                                        String id = c.getString("Id");

                                       *//* List<TestData> list = response.body().getData();
                                        adapter = new myTestAdapter(mContext, getActivity(), list);
                                        adapter.notifyDataSetChanged();*//*

                                    } else {
                                        Utils.showToast(mContext, json.getString("message"));

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //  mProgressDialog.dismiss();
                                pd.dismiss();
                                Toast.makeText(Activity_Testresult.this, "Error, Please try again" + error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        })

                {
                    //This is for Headers If You Needed
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        String creds = String.format("%s:%s", "VIL1XuqUVnkq31L2PdCtjw==", "zxdwT819BSOhT9pmnadjeg==");
                        String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                        params.put("Authorization", auth);
                        return params;
                    }

                    //Pass Your Parameters here
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("UserName", "andrew@gmail.com");
                        System.out.println("Login SendData" + params);
                        return params;

                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(stringRequest);
            }
*/

    }

    private void prepareMovieData() {
        listitem movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        adapter.notifyDataSetChanged();
    }
}

