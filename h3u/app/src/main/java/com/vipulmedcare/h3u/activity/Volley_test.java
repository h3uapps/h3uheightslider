package com.vipulmedcare.h3u.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.utils.ApiConstants;
import com.vipulmedcare.h3u.utils.NetworkUtil;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vipul on 7/19/2017.
 */

public class Volley_test extends Activity {
    Button button3;
    private Context mContext;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vollet_test);

        mContext = Volley_test.this;
        button3 = (Button) findViewById(R.id.button3);

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                volleytest();
            }
        });
    }

    private void volleytest() {
      /*  mProgressDialog = new ProgressDialog(Volley_test.this,
                AlertDialog.THEME_HOLO_LIGHT);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();*/
        // showProgressDialog("Please wait...");

        if (NetworkUtil.isNetworkAvailable(mContext)) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.APP_idprof,
                    //  StringRequest stringRequest = new StringRequest(Request.Method.GET, ApiConstants.APP_Test,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            System.out.println(response);
                            //   Log.d("Response volley", response);
                            Toast.makeText(Volley_test.this,response,Toast.LENGTH_LONG).show();
                           // mProgressDialog.dismiss();
                            try {
                                JSONObject json = new JSONObject(response);
                                System.out.println(response);

                                if (json.getInt("success") == 1) {
                                    JSONArray pm = json.getJSONArray("data");
                                    for (int i = 0; i < pm.length(); i++) {
                                    }
                                    // String FirstName = json.getJSONObject("data").getString("FirstName");
                                    Toast.makeText(Volley_test.this, " Successful!", Toast.LENGTH_LONG).show();

                                } else {
                                    Toast.makeText(Volley_test.this, "Invalid  ", Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //  mProgressDialog.dismiss();
                            Toast.makeText(Volley_test.this, "Error, Please try again"+error.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })

            {
               //This is for Headers If You Needed
               @Override
               public Map<String, String> getHeaders() throws AuthFailureError {
                   Map<String, String> params = new HashMap<String, String>();
                   String creds = String.format("%s:%s","VIL1XuqUVnkq31L2PdCtjw==","zxdwT819BSOhT9pmnadjeg==");
                   String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                   params.put("Authorization",auth);
                   return params;
               }

                //Pass Your Parameters here
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("username","andrew@gmail.com");
                    System.out.println("Login SendData" + params);
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }}
    }

