package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import com.viewpagerindicator.CirclePageIndicator;
import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.OPDCardsAdapter;
import com.vipulmedcare.h3u.adapter.OPDCardsPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 9/1/2017.
 */

public class OPDCardsAcivity extends AppCompatActivity {
    private ViewPager opd_cards_pager;
    private OPDCardsPagerAdapter mAdapter;
    private OPDCardsAdapter opdCardsAdapter;
    private List<String> horizontalList;
    private Context mContext;
    private RecyclerView opd_card_rv;
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.opd_cards);
        mContext = OPDCardsAcivity.this;

        initViews();

    }

    public void initViews(){
        opd_card_rv= (RecyclerView) findViewById(R.id.opd_card_rv);

        opd_cards_pager = (ViewPager) findViewById(R.id.opd_cards_pager);
        mAdapter = new OPDCardsPagerAdapter(getSupportFragmentManager(),mContext);
        opd_cards_pager.setAdapter(mAdapter);
        opd_cards_pager.setClipToPadding(false);
        opd_cards_pager.setPadding(40, 0, 40, 0);
        // sets a margin b/w individual pages to ensure that there is a gap b/w them
        opd_cards_pager.setPageMargin(10);

        CirclePageIndicator indicator = (CirclePageIndicator)findViewById(R.id.indicator);
        indicator.setViewPager(opd_cards_pager);

        horizontalList=new ArrayList<>();
        horizontalList.add("horizontal 1");
        horizontalList.add("horizontal 2");
        horizontalList.add("horizontal 3");
        horizontalList.add("horizontal 4");
        horizontalList.add("horizontal 5");
        horizontalList.add("horizontal 6");

        opdCardsAdapter=new OPDCardsAdapter(horizontalList,mContext);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(OPDCardsAcivity.this, LinearLayoutManager.VERTICAL, false);
        opd_card_rv.setLayoutManager(horizontalLayoutManagaer);
        opd_card_rv.setAdapter(opdCardsAdapter);
    }
}
