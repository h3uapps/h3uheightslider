package com.vipulmedcare.h3u.view.animator;

import android.animation.ValueAnimator;
import android.graphics.Color;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;

import com.vipulmedcare.h3u.base.AppContext;


/**
 * Created by vjangra on 22/11/15.
 */
public class Animator implements Animation.AnimationListener {

    public View view;
    public View[] views;
    public Animation animation;
    public AnimationSet animationSet;
    private OnCompleteListener listener;


    private Animator(Animation animation, View view){
        this.view = view;
        this.animation = animation;
        this.startAnimationAndListener();
    }

    private void startAnimationAndListener(){

        if(this.animation != null) this.animation.setAnimationListener(this);

        if(this.animationSet != null) this.animationSet.setAnimationListener(this);

        if(this.view != null) this.view.setAnimation(animation);

        if(this.views != null) {
            for (View view : views) {
                view.setAnimation(animationSet != null ? animationSet : animation);
            }
        }
    }

    private Animator(Animation animation, View[] views){
        this.views = views;
        this.animation = animation;
        this.startAnimationAndListener();
    }

    private Animator(int animId, View view, int time, int delay){
        this.view = view;
        animation = AnimationUtils.loadAnimation(AppContext.getContext(),animId);
        animation.setDuration(time);
        animation.setStartOffset(delay);
        animation.setFillAfter(true);

        this.startAnimationAndListener();
    }

    private Animator(int animId, View view, int time, int delay, int repeat, int mode){
        this.view = view;

        animation = AnimationUtils.loadAnimation(AppContext.getContext(),animId);
        animation.setDuration(time);
        animation.setStartOffset(delay);
        animation.setFillAfter(true);
        animation.setRepeatCount(repeat);
        animation.setRepeatMode(mode);

        this.startAnimationAndListener();
    }

    private Animator(int[] anims, View view, int[] time, int delay){
        this.view = view;
        this.animationSet = new AnimationSet(false); //false mean don't share interpolators
        this.animationSet.setInterpolator(new AccelerateInterpolator());
        for(int i=0; i< anims.length;i++){
            Animation a = AnimationUtils.loadAnimation(AppContext.getContext(), anims[i]);
            a.setDuration(time[i]);
            animationSet.addAnimation(a);
        }
        animationSet.setStartOffset(delay);
        animationSet.setFillAfter(true);

        this.startAnimationAndListener();
    }

    private Animator(int[] anims, View[] views, int[] time, int delay){
        this.views = views;
        this.animationSet = new AnimationSet(false); //false mean don't share interpolators
        for(int i=0; i< anims.length;i++){
            Animation a = AnimationUtils.loadAnimation(AppContext.getContext(), anims[i]);
            a.setDuration(time[i]);
            animationSet.addAnimation(a);
        }
        animationSet.setStartOffset(delay);
        animationSet.setFillAfter(true);

        this.startAnimationAndListener();
    }

    private Animator(int[] anims, View view, int[] time, int delay, int repeat, int mode){
        this.view = view;
        this.animationSet = new AnimationSet(false); //false mean dont share interpolators
        for(int i=0; i< anims.length;i++){
            Animation a = AnimationUtils.loadAnimation(AppContext.getContext(), anims[i]);
            a.setDuration(time[i]);
            animationSet.addAnimation(a);
        }
        animationSet.setStartOffset(delay);
        animationSet.setFillAfter(true);
        animationSet.setRepeatCount(repeat);
        animationSet.setRepeatMode(mode);

        this.startAnimationAndListener();
    }



    public Animator setOnCompleteListener(OnCompleteListener listener){
        this.listener = listener;
        return this;
    }

    public void onAnimationStart(Animation animation){
        if(views != null){
            for (View view: views) {
                showViewIfHidden(view);
            }
        }
        showViewIfHidden(view);
    }

    private void showViewIfHidden(View view){
        if(view != null){
            if(view.getVisibility() != View.VISIBLE) view.setVisibility(View.VISIBLE);
        }
    }

    public void onAnimationRepeat(Animation animation){

    }

    public void onAnimationEnd(Animation animation){
        if(this.listener != null){
            this.listener.onComplete();
        }
    }

    public interface OnCompleteListener {
        void onComplete();
    }




    public static Animator animate(Animation animation, View view){
        return new Animator(animation,view);
    }

    public static Animator animate(int animId, View view, int time, int delay){
        return new Animator(animId,view,time,delay);
    }

    public static Animator animate(int animId, View view, int time, int delay, int repeat, int repeatMode){
        return new Animator(animId,view,time,delay,repeat,repeatMode);
    }

    public static Animator animate(int[] anims, View view, int[] time, int delay){
        /*
        int[] anim = {R.anim.fadein, R.anim.scale_up_from_center};
        int[] time = {500,1000};
        Animator.animate(anim,view,time,500);
         */
        return new Animator(anims,view,time,delay);
    }

    public static Animator animate(int[] anims, View[] views, int[] time, int delay){
        /*
        int[] anim = {R.anim.fadein, R.anim.scale_up_from_center};
        int[] time = {500,1000};
        Animator.animate(anim,view,time,500);
         */
        return new Animator(anims,views,time,delay);
    }

    public static Animator animate(int[] anims, View view, int[] time, int delay, int repeat, int repeatMode){
        return new Animator(anims,view,time,delay,repeat,repeatMode);
    }


    public static ScaleAnimation getScaleAnimation(){
        ScaleAnimation scale = new ScaleAnimation(1.0f, 0.9f, 1.0f, 0.9f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scale.setRepeatCount(Animation.INFINITE);
        scale.setRepeatMode(Animation.REVERSE);
        scale.setDuration(1000);
        return scale;
    }


    public static Animator transformColor(final View view, final float[] from, final float[] to, int time, int delay, android.animation.Animator.AnimatorListener listener){
        return new Animator(view,from,to,time,delay,listener);
    }

    private Animator(final View view, final float[] from, final float[] to, int time, int delay, android.animation.Animator.AnimatorListener listener){
        this.view = view;

        ValueAnimator animator = ValueAnimator.ofFloat(0, 1);   // animate from 0 to 1
        animator.setDuration(time);                              // for 300 ms
        animator.setStartDelay(delay);

        final float[] hsv  = new float[3];                  // transition color
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(){
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // Transition along each axis of HSV (hue, saturation, value)
                hsv[0] = from[0] + (to[0] - from[0]) * animation.getAnimatedFraction();
                hsv[1] = from[1] + (to[1] - from[1]) * animation.getAnimatedFraction();
                hsv[2] = from[2] + (to[2] - from[2]) * animation.getAnimatedFraction();

                view.setBackgroundColor(Color.HSVToColor(hsv));
            }
        });
        animator.addListener(listener);
        animator.start();
    }

}
