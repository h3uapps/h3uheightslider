package com.vipulmedcare.h3u.network;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.vipulmedcare.h3u.BuildConfig;


import org.json.JSONObject;

/**
 * Created by vipul on 7/13/2017.
 */

public class UpdateListener implements Response.Listener<JSONObject>, Response.ErrorListener {

    private int	reqType;
    private onUpdateViewListener onUpdateViewListener;

    public interface onUpdateViewListener {
        public void updateView(String responseString, boolean isSuccess, int reqType);
    }

    public UpdateListener(onUpdateViewListener onUpdateView, int reqType) {
        this.reqType = reqType;
        this.onUpdateViewListener = onUpdateView;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        onUpdateViewListener.updateView(VolleyExceptionUtil.getErrorMessage(error), false, reqType);
    }


    @Override
    public void onResponse(JSONObject response) {
        // TODO Auto-generated method stub
        if (BuildConfig.DEBUG) {
            Log.i("Respone-------------->", response.toString());
        }
        onUpdateViewListener.updateView(response.toString(), true, reqType);
    }

}
