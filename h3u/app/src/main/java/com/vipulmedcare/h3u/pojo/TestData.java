package com.vipulmedcare.h3u.pojo;

public class TestData {

    private String TransactionID;
    private String TransactionDate;
    private String TransactionType;
    private String PatientName;
    private String ProviderName;
    private String ProviderLogo;
    private String ProviderCode;
    private String Status;
    private String CardHolderName;
    private String CardNo;
    private String CouponCode;
    private int TotalOPDAmount;

    public String getTransactionID() {
        return TransactionID;
    }

    public void setTransactionID(String transactionID) {
        TransactionID = transactionID;
    }

    public String getTransactionDate() {
        return TransactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        TransactionDate = transactionDate;
    }

    public String getTransactionType() {
        return TransactionType;
    }

    public void setTransactionType(String transactionType) {
        TransactionType = transactionType;
    }

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }

    public String getProviderName() {
        return ProviderName;
    }

    public void setProviderName(String providerName) {
        ProviderName = providerName;
    }

    public String getProviderLogo() {
        return ProviderLogo;
    }

    public void setProviderLogo(String providerLogo) {
        ProviderLogo = providerLogo;
    }

    public String getProviderCode() {
        return ProviderCode;
    }

    public void setProviderCode(String providerCode) {
        ProviderCode = providerCode;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getCardHolderName() {
        return CardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        CardHolderName = cardHolderName;
    }

    public String getCardNo() {
        return CardNo;
    }

    public void setCardNo(String cardNo) {
        CardNo = cardNo;
    }

    public String getCouponCode() {
        return CouponCode;
    }

    public void setCouponCode(String couponCode) {
        CouponCode = couponCode;
    }

    public int getTotalOPDAmount() {
        return TotalOPDAmount;
    }

    public void setTotalOPDAmount(int totalOPDAmount) {
        TotalOPDAmount = totalOPDAmount;
    }
}

