package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.view.text.CustomTextView;
import com.vipulmedcare.h3u.view.text.CustomTextViewBold;


/**
 * Created by Andrew PJ on 8/12/2017.
 */

public class Activity_OPD extends BaseActivity implements View.OnClickListener {

    ImageView opd_conslt_cross;
    ToggleButton tb_no,tb_yes;
    private Context mContext;
    TextView tv_headertext,tv_hnameopd,tv_namehospital,tv_tellabout;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.opd_consult_activity);

        mContext = Activity_OPD.this;
        initviews();

    }

    private void initviews() {

        //    .......  heading .... ................        //
        tv_headertext =(TextView) findViewById(R.id.tv_headertext);
        tv_headertext.setText("OPD CONSULTATION");
        tb_no= (ToggleButton) findViewById(R.id.tb_no);
        tb_yes= (ToggleButton) findViewById(R.id.tb_yes);
        opd_conslt_cross= (ImageView) findViewById(R.id.opd_conslt_cross);


        tv_tellabout= (CustomTextViewBold) findViewById(R.id.tv_tellabout);
        tv_hnameopd= (CustomTextView) findViewById(R.id.tv_hnameopd);
        tv_namehospital= (CustomTextView) findViewById(R.id.tv_namehospital);


        Typeface o= Typeface.createFromAsset(mContext.getAssets(),"fonts/Lato-Bold.ttf");
        tb_yes.setTypeface(o);
        Typeface po= Typeface.createFromAsset(mContext.getAssets(),"fonts/Lato-Bold.ttf");
        tb_no.setTypeface(po);


        tb_no.setOnClickListener(this);
        tb_yes.setOnClickListener(this);
        opd_conslt_cross.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tb_no:
                tb_yes.setChecked(false);
                if(tb_no.isChecked())
                {
                    Intent in =new Intent(Activity_OPD.this,choose_special_activity.class);
                    startActivity(in);
                   // Toast.makeText(Activity_OPD.this, "yes", Toast.LENGTH_LONG).show();
                }
                else
                {
                    tb_yes.setChecked(false);
                   // Toast.makeText(Activity_OPD.this, "Un-Checked", Toast.LENGTH_LONG).show();
                }

                break;

            case R.id.tb_yes:
                tb_no.setChecked(false);
                if(tb_yes.isChecked())
                {
                    Intent in =new Intent(Activity_OPD.this,Enter_special_activity.class);
                    startActivity(in);
                    //Toast.makeText(Activity_OPD.this, "no suggest one", Toast.LENGTH_LONG).show();
                }
                else
                {
                  //  Toast.makeText(Activity_OPD.this, "Un-Checked", Toast.LENGTH_LONG).show();
                }


               /* Intent in =new Intent(Activity_OPD.this,choose_special_activity.class);
                startActivity(in);*/
                break;
               /* Intent i =new Intent(Activity_OPD.this,Enter_special_activity.class);
                startActivity(i);
                break;*/
            case R.id.opd_conslt_cross:
                finish();
                break;
        }
    }
}
