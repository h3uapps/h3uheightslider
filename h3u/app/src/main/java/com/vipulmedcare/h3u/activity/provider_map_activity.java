package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.HorizontalAdapter;
import com.vipulmedcare.h3u.adapter.Spec_recy_adapter;
import com.vipulmedcare.h3u.adapter.VerticalAdapter;
import com.vipulmedcare.h3u.base.BaseActivity;


import java.util.ArrayList;

/**
 * Created by Andrew pj on 8/3/2017.
 */

public class provider_map_activity extends BaseActivity implements View.OnClickListener , OnMapReadyCallback {

    RecyclerView horizontal_recycler_view, vertical_recycler_view,special_recycler_view;
    private HorizontalAdapter horizontalAdapter;
    private ArrayList<String> horizontalList,verticalList ;
    private VerticalAdapter verticalAdapter;
    private Spec_recy_adapter spec_recyc_adapter;

    private Context mContext;
    private GoogleMap googleMap;
    private ImageView iv_specialisaion_down;
    private  View provid_map_view;
    private LinearLayout ll_ver_rec_view,ll_bottom_bttns,ll_spec_rec_view , ll_nxtbtn;
    private boolean map_spec_recy_view_show = false;
    ImageButton map_arrow_back, map_cross_btn;
    private FrameLayout prov_map_nxt_btn;

    private TextView tv_near_txt,tv_change,tv_specialisation;


    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.provider_map_activity);

        mContext = provider_map_activity.this;

        horizontal_recycler_view = (RecyclerView) findViewById(R.id.horizontal_recycler_view);
        vertical_recycler_view= (RecyclerView) findViewById(R.id.vertical_recycler_view);
        horizontalList=new ArrayList<>();
        horizontalList.add("horizontal 1");
        horizontalList.add("horizontal 2");
        horizontalList.add("horizontal 3");
        horizontalList.add("horizontal 4");
        horizontalList.add("horizontal 5");
        horizontalList.add("horizontal 6");
        horizontalList.add("horizontal 7");
        horizontalList.add("horizontal 8");
        horizontalList.add("horizontal 9");
        horizontalList.add("horizontal 10");
        horizontalAdapter=new HorizontalAdapter(horizontalList);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(provider_map_activity.this, LinearLayoutManager.HORIZONTAL, false);
        horizontal_recycler_view.setLayoutManager(horizontalLayoutManagaer);
        horizontal_recycler_view.setAdapter(horizontalAdapter);

        verticalList=new ArrayList<>();
        verticalList.add("verticallist 1");
        verticalList.add("verticallist 2");
        verticalList.add("verticallist 3");
        verticalList.add("verticallist 4");
        verticalList.add("verticallist 5");
        verticalList.add("verticallist 6");
        verticalList.add("verticallist 7");
        verticalList.add("verticallist 8");
        verticalList.add("verticallist 9");
        verticalList.add("verticallist 10");


        verticalAdapter=new VerticalAdapter(verticalList,mContext);
        LinearLayoutManager verticalLayoutmanager
                = new LinearLayoutManager(provider_map_activity.this, LinearLayoutManager.VERTICAL, false);
        vertical_recycler_view.setLayoutManager(verticalLayoutmanager);

        vertical_recycler_view.setAdapter(spec_recyc_adapter);




        initViews();
        initMap();
    }

    public void initViews(){
        map_cross_btn = (ImageButton) findViewById(R.id.map_cross_btn);
        map_arrow_back = (ImageButton) findViewById(R.id.map_arrow_back);
        prov_map_nxt_btn = (FrameLayout) findViewById(R.id.prov_map_nxt_btn);

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontHeading = Typeface.createFromAsset(getAssets(), "fonts/Lato-Bold.ttf");

        tv_near_txt= (TextView) findViewById(R.id.tv_near_txt);
        tv_change= (TextView) findViewById(R.id.tv_near_txt);
        tv_specialisation= (TextView) findViewById(R.id.tv_near_txt);

        tv_near_txt.setTypeface(font);
                tv_change.setTypeface(font);
        tv_specialisation.setTypeface(fontHeading);


        map_cross_btn.setOnClickListener(this);
        map_arrow_back.setOnClickListener(this);

        iv_specialisaion_down = (ImageView) findViewById(R.id.iv_specialisaion_down);
        iv_specialisaion_down.setOnClickListener(this);
        provid_map_view = findViewById(R.id.provid_map_view);
        ll_ver_rec_view = (LinearLayout) findViewById(R.id.ll_ver_rec_view);
        ll_bottom_bttns= (LinearLayout) findViewById(R.id.ll_bottom_bttns);
        ll_spec_rec_view= (LinearLayout) findViewById(R.id.ll_spec_rec_view);
        horizontal_recycler_view.setVisibility(View.VISIBLE);
        provid_map_view.setVisibility(View.VISIBLE);
        ll_ver_rec_view.setVisibility(View.VISIBLE);
        ll_bottom_bttns.setVisibility(View.VISIBLE);
        map_arrow_back.setVisibility(View.VISIBLE);
        map_cross_btn.setVisibility(View.VISIBLE);
        prov_map_nxt_btn.setVisibility(View.INVISIBLE);

        ll_spec_rec_view.setVisibility(View.GONE);

        if(map_spec_recy_view_show==false){

            iv_specialisaion_down.setBackgroundResource(R.drawable.ic_down);

        }
        else{
            iv_specialisaion_down.setBackgroundResource(R.drawable.ic_cheverolet_up);


        }
        LinearLayoutManager verticalLayoutmanager1
                = new LinearLayoutManager(provider_map_activity.this, LinearLayoutManager.VERTICAL, false);
        special_recycler_view= (RecyclerView) findViewById(R.id.vertical_recycler_view);
        special_recycler_view.setLayoutManager(verticalLayoutmanager1);
        special_recycler_view.setAdapter(verticalAdapter);

    }

    private void initMap() {

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }



    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.map_arrow_back:
                Intent intent1 = new Intent(mContext,choose_special_activity.class );
                startActivity(intent1);
                break;

            case R.id.chs_spl_cross:
                finish();
                break;
            case R.id.iv_specialisaion_down:

                if(map_spec_recy_view_show==false) {
                    horizontal_recycler_view.setVisibility(View.GONE);
                    provid_map_view.setVisibility(View.GONE);
                    ll_ver_rec_view.setVisibility(View.GONE);
                    map_arrow_back.setVisibility(View.INVISIBLE);
                    map_cross_btn.setVisibility(View.INVISIBLE);
                    prov_map_nxt_btn.setVisibility(View.VISIBLE);
                    ll_spec_rec_view.setVisibility(View.VISIBLE);



                    iv_specialisaion_down.setBackgroundResource(R.drawable.ic_cheverolet_up);
                    map_spec_recy_view_show=true;
                }
                else {
                    horizontal_recycler_view.setVisibility(View.VISIBLE);
                    provid_map_view.setVisibility(View.VISIBLE);
                    ll_ver_rec_view.setVisibility(View.VISIBLE);
                    map_arrow_back.setVisibility(View.VISIBLE);
                    map_cross_btn.setVisibility(View.VISIBLE);
                    prov_map_nxt_btn.setVisibility(View.INVISIBLE);

                    ll_spec_rec_view.setVisibility(View.GONE);


                    iv_specialisaion_down.setBackgroundResource(R.drawable.ic_down);
                    map_spec_recy_view_show=false;
                }

                break;
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap=googleMap;
        LatLng latLng = new LatLng(13.05241, 80.25082);
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.addMarker(new MarkerOptions().position(latLng).title("Raj Amal"));
        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
    }


}
