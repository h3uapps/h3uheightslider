package com.vipulmedcare.h3u.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.vipulmedcare.h3u.R;

import java.util.ArrayList;


public class SlidingImage_Adapter extends PagerAdapter {


    private ArrayList<String> Testimonials;
    private LayoutInflater inflater;
    private Context context;


    public SlidingImage_Adapter(Context context, ArrayList<String> Testimonial) {
        this.context = context;
        this.Testimonials = Testimonial;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return Testimonials.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View TextLayout = inflater.inflate(R.layout.slidingimages_layout, view, false);

        assert TextLayout  != null;
        final TextView tv_testimonials = (TextView) TextLayout.findViewById(R.id.tv_testimonials);

        tv_testimonials.setText(Testimonials.get(position));

        view.addView(TextLayout, 0);

        return TextLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}