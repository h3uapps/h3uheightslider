package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.HealthTabsPagerAdapter;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vipul on 8/24/2017.
 */

public class HealthProviderDetail extends AppCompatActivity implements
        View.OnClickListener , OnMapReadyCallback {

    TabLayout tabLayout;
    private GoogleMap googleMap;

    private ViewPager viewPager;
    private HealthTabsPagerAdapter mAdapter;
    private android.support.v7.app.ActionBar actionBar;

    private GridView GridLayout1;
    // Tab titles
    private String[] tabs = { "CORPORTAE", "PERSONAL" };
    ImageButton prov_dtl_arrow_back, prov_dtl_cross_btn,provider_detail_chat;
    private Context mContext;

    private TextView health_prov_name,tv_rating_no,tv_no,tv_abt_hosp,tv_more,tv_abt_hosp_desc,tv_diagnostic,tv_sat_sun,tv_hrs,tv_mon_fri,tv_mon_fri_timings,tv_sat_sun_timings,tv_loc,tv_open_map,tv_adrs,tv_dist;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.health_provider_detail);
        mContext = HealthProviderDetail.this;

        GridView gv = (GridView) findViewById(R.id.gv);

        String[] Specialities = new String[]{
                "Catalinaery ",
                "Cabinetewr",
                "Pale Pale ",
                "Pinkinaery",
                "Landinaery",
                "Coastinaery",

        };

        // Populate a List from Array elements
        final List<String> plantsList = new ArrayList<String>(Arrays.asList(Specialities));

        // Create a new ArrayAdapter
        final ArrayAdapter<String> gridViewArrayAdapter = new ArrayAdapter<String>
                (this, R.layout.provider_detail_grid_speciality, plantsList);

        // Data bind GridView with ArrayAdapter (String Array elements)
        gv.setAdapter(gridViewArrayAdapter);
        int girdViewRowsCount = plantsList.size()/3;
        ViewGroup.LayoutParams layoutParams = gv.getLayoutParams();
        layoutParams.height = girdViewRowsCount*100; //this is in pixels
        gv.setLayoutParams(layoutParams);




        // Initilization
        viewPager = (ViewPager) findViewById(R.id.pager);
        mAdapter = new HealthTabsPagerAdapter(getSupportFragmentManager(),mContext);


        viewPager.setAdapter(mAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs1);
        tabLayout.setupWithViewPager(viewPager);
        TabLayout.Tab tab= tabLayout.getTabAt(0);
        tab.select();





        initViews();
        initMap();

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void initViews(){

        prov_dtl_arrow_back = (ImageButton) findViewById(R.id.prov_dtl_arrow_back);
        prov_dtl_cross_btn = (ImageButton) findViewById(R.id.prov_dtl_cross_btn);
        provider_detail_chat = (ImageButton) findViewById(R.id.provider_detail_chat);
        prov_dtl_arrow_back.setOnClickListener(this);
        prov_dtl_cross_btn.setOnClickListener(this);
        provider_detail_chat.setOnClickListener(this);



        health_prov_name = (TextView) findViewById(R.id.health_prov_name);
        tv_rating_no= (TextView) findViewById(R.id.tv_no);
        tv_no= (TextView) findViewById(R.id.tv_abt_hosp);
        tv_abt_hosp= (TextView) findViewById(R.id.tv_abt_hosp);
        tv_more= (TextView) findViewById(R.id.tv_more);
        tv_abt_hosp_desc= (TextView) findViewById(R.id.tv_abt_hosp_desc);
        tv_diagnostic= (TextView) findViewById(R.id.tv_diagnostic);
        tv_hrs= (TextView) findViewById(R.id.tv_hrs);
        tv_mon_fri= (TextView) findViewById(R.id.tv_mon_fri);
        tv_mon_fri_timings= (TextView) findViewById(R.id.tv_mon_fri_timings);
        tv_sat_sun= (TextView) findViewById(R.id.tv_sat_sun);
        tv_sat_sun_timings= (TextView) findViewById(R.id.tv_sat_sun_timings);
        tv_loc= (TextView) findViewById(R.id.tv_loc);
        tv_open_map= (TextView) findViewById(R.id.tv_open_map);
        tv_adrs= (TextView) findViewById(R.id.tv_adrs);
        tv_dist= (TextView) findViewById(R.id.tv_dist);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        Typeface fontHeading = Typeface.createFromAsset(getAssets(), "fonts/Lato-Bold.ttf");
//        health_prov_name.setTypeface(fontHeading);
//        tv_rating_no.setTypeface(fontHeading);
//        tv_no.setTypeface(font);
//        tv_abt_hosp_desc.setTypeface(font);
//        tv_mon_fri_timings.setTypeface(font);
//        tv_sat_sun_timings.setTypeface(font);
//        tv_adrs.setTypeface(font);
//        tv_dist.setTypeface(font);
//        tv_abt_hosp.setTypeface(fontHeading);
//        tv_more.setTypeface(fontHeading);
//        tv_diagnostic.setTypeface(fontHeading);
//        tv_hrs.setTypeface(fontHeading);
//        tv_mon_fri.setTypeface(fontHeading);
//        tv_sat_sun.setTypeface(fontHeading);
//        tv_loc.setTypeface(fontHeading);
//        tv_open_map.setTypeface(fontHeading);


    }
    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.prov_dtl_arrow_back:
                Intent intent1 = new Intent(mContext,provider_map_activity.class );
                startActivity(intent1);
                break;

            case R.id.chs_spl_cross:
                finish();
                break;
            case R.id.provider_detail_chat:
                Intent intent2 = new Intent(mContext,CardActivity.class );
                startActivity(intent2);
                break;




        }

    }
    private void initMap() {

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap=googleMap;
        LatLng latLng = new LatLng(13.05241, 80.25082);
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.addMarker(new MarkerOptions().position(latLng).title("Raj Amal"));
        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

    }
}
