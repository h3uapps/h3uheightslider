/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package com.vipulmedcare.h3u.profile;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.vipulmedcare.h3u.R;

import java.io.ByteArrayOutputStream;


public class Identy_Fragment extends Fragment {
    private ImageView img_user_profile,img_user_pancard,img_user_passport,img_user_voterid;
    Context mContext;
    private String user_proimage;
    private String userChoosenTask;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1234;
    private static final int CROP_PIC_REQUEST_CODE = 1235;
    private Boolean isCamDenied=false, isGalDenied=false;
    LinearLayout ll_imguser_upload,lin_pancard,lin_voterid,lin_passport;

    public static Identy_Fragment newInstance() {
        Identy_Fragment fragment = new Identy_Fragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_item_identy, container, false);


        mContext = getContext();
        ll_imguser_upload= (LinearLayout) rootView.findViewById(R.id.ll_imguser_upload);
        lin_pancard= (LinearLayout) rootView.findViewById(R.id.lin_pancard);
        lin_voterid= (LinearLayout) rootView.findViewById(R.id.lin_voterid);
        lin_passport= (LinearLayout) rootView.findViewById(R.id.lin_passport);

        img_user_voterid= (ImageView) rootView.findViewById(R.id.img_user_voterid);
        img_user_voterid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
        img_user_passport= (ImageView) rootView.findViewById(R.id.img_user_passport);
        img_user_passport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
        img_user_pancard= (ImageView) rootView.findViewById(R.id.img_user_pancard);
        img_user_pancard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

        img_user_profile = (ImageView)rootView.findViewById(R.id.img_user_profile);
        img_user_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

         return rootView;

    }



    public int Type=0;
    private void showDialog() {
        final Dialog cameraDialog = new Dialog(mContext); // change this
        cameraDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cameraDialog.setContentView(R.layout.camera_select);
        ImageView ib_btnclose = (ImageView) cameraDialog.findViewById(R.id.ib_btnclose);
        ib_btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Type=1;
                cameraDialog.dismiss();

            }
        });

        ImageButton ib_camera = (ImageButton) cameraDialog.findViewById(R.id.ib_camera);
        ib_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Utils.showToast(mContext, "camera selected");
                userChoosenTask = "Take Photo";
                if (checkPermissionCamera(mContext)) {
                    cameraIntent();
                    cameraDialog.dismiss();
                }
                else {
//                    Utils.showToast(mContext, "Permissions are not granted!!!");
                    cameraDialog.dismiss();
                }

            }
        });

        ImageButton ib_gallery = (ImageButton) cameraDialog.findViewById(R.id.ib_gallery);
        ib_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Type=1;
//                Utils.showToast(mContext,"gallery selected");
                userChoosenTask = "Choose from Library";

                if (checkPermissionGallery(mContext)) {
                    galleryIntent();
                    cameraDialog.dismiss();
                } else {
//                    Utils.showToast(mContext, "Permissions are not granted!!!");
                    cameraDialog.dismiss();
                }

            }
        });
        cameraDialog.show();
    }


    public boolean checkPermissionGallery(final Context context) {
        Boolean result = false;
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    ) {
                // if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE))
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.READ_EXTERNAL_STORAGE))
                {

                    requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    //  ActivityCompat.requestPermissions(Editprofile_Fragment.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    isGalDenied=true;


                } else {
                    if(isGalDenied){
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                        alertBuilder.setCancelable(true);
                        alertBuilder.setTitle("Permission Required");
                        alertBuilder.setMessage("External storage permission is necessary for image reading form storage");
                        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + mContext.getPackageName()));
                                myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
                                myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                mContext.startActivity(myAppSettings);
                            }
                        });
                        AlertDialog alert = alertBuilder.create();
                        alert.show();

                    }else {
                        requestPermissions( new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                }
//                checkPermission(context);
                result = false;
            } else {
                result = true;
            }
        } else {
            result = true;
        }
        return result;
    }

    public boolean checkPermissionCamera(final Context context) {
        Boolean result = false;
// Utility.mContext= context;
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,  Manifest.permission.READ_EXTERNAL_STORAGE)
                    + ContextCompat.checkSelfPermission(mContext,Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                // if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this, android.Manifest.permission.CAMERA))
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA)){
                    requestPermissions(new String[]{Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_CAMERA);
                    //ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_CAMERA);
                    isCamDenied=true;
                } else {
                    if(isCamDenied){
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                        alertBuilder.setCancelable(true);
                        alertBuilder.setTitle("Permissions Required");
                        alertBuilder.setMessage("We need camera permissions to get the picture.");
                        alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + mContext.getPackageName()));
                                myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
                                myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                mContext.startActivity(myAppSettings);
                            }
                        });
                        AlertDialog alert = alertBuilder.create();
                        alert.show();
                    }else {
                        requestPermissions(new String[]{Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_CAMERA);
                        //  ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_CAMERA);
                    }
                }
                result = false;
            } else {
                result = true;
            }
        } else {
            result = true;
        }
        return result;
    }


    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);

    }

    private void onCaptureImageResult(Intent data) {

        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            Uri picUri = getImageUri(mContext, thumbnail);
            cropIntent.setDataAndType(picUri, "image");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 512);
            cropIntent.putExtra("outputY", 512);
            cropIntent.putExtra("return-data", true);
            startActivityForResult(cropIntent, CROP_PIC_REQUEST_CODE);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
//            String errorMessage = "Whoops - your device doesn't support the crop action!";
//            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
//            toast.show();
            onCropImageResult(data);
//            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//            user_proimage = BitMapToString(thumbnail);
//            img_user.setImageBitmap(thumbnail);
        }
    }

    private void onCropImageResult(Intent data) {

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        user_proimage = BitMapToString(thumbnail);  ///..........................

if (Type==1){
    ll_imguser_upload.setBackground(new BitmapDrawable(getResources(),thumbnail));
}else if (Type==2){
    lin_pancard.setBackground(new BitmapDrawable(getResources(),thumbnail));
}else if (Type==3){
    lin_voterid.setBackground(new BitmapDrawable(getResources(),thumbnail));
}else if (Type==4){
    lin_passport.setBackground(new BitmapDrawable(getResources(),thumbnail));
}
     // img_user_profile.setImageBitmap(thumbnail);
       // ll_imguser_upload.setImageBitmap(thumbnail);

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "ProfilePic", null);
        return Uri.parse(path);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
            else if (requestCode == CROP_PIC_REQUEST_CODE)
                onCropImageResult(data);
        }
    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;

    }

    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
//        user_proimage = BitMapToString(bm);
        if (data != null)

        {

            try {
                Intent cropIntent = new Intent("com.android.camera.action.CROP");
//            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                bm = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), data.getData());
//            user_proimage = BitMapToString(bm);
//            img_user.setImageBitmap(thumbnail);
                Uri picUri = getImageUri(mContext, bm);
                cropIntent.setDataAndType(picUri, "image/*");
                cropIntent.putExtra("crop", "true");
                cropIntent.putExtra("aspectX", 1);
                cropIntent.putExtra("aspectY", 1);
                cropIntent.putExtra("outputX", 128);
                cropIntent.putExtra("outputY", 128);
                cropIntent.putExtra("return-data", true);
                startActivityForResult(cropIntent, CROP_PIC_REQUEST_CODE);
            }
            // respond to users whose devices do not support the crop action
            catch (Exception anfe) {
                // display an error message
                String errorMessage = "Whoops - your device doesn't support the crop action!";
                Toast toast = Toast.makeText(mContext, errorMessage, Toast.LENGTH_SHORT);
                toast.show();
                onCropImageResult(data);

                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                user_proimage = BitMapToString(thumbnail);
             //   img_user_profile.setImageBitmap(thumbnail);
                //ll_imguser_upload.setBackgroundDrawable(thumbnail);
              // ll_imguser_upload.setBackgroundResource(R.drawable.camera);

                if (Type==1){
                    ll_imguser_upload.setBackground(new BitmapDrawable(getResources(),thumbnail));
                }else if (Type==2){
                    lin_pancard.setBackground(new BitmapDrawable(getResources(),thumbnail));
                }else if (Type==3){
                    lin_voterid.setBackground(new BitmapDrawable(getResources(),thumbnail));
                }else if (Type==4){
                    lin_passport.setBackground(new BitmapDrawable(getResources(),thumbnail));
                }


            }
        }
        }}
