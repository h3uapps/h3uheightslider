package com.vipulmedcare.h3u.activity;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.BmiResultAdapter;
import com.vipulmedcare.h3u.adapter.VerticalAdapter;
import com.vipulmedcare.h3u.db.DBManager;
import com.vipulmedcare.h3u.db.DatabaseHelper;
import com.vipulmedcare.h3u.pojo.BmiResult;
import com.vipulmedcare.h3u.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.vipulmedcare.h3u.R.id.vertical_recycler_view;


/**
 * Created by vipul on 9/8/2017.
 */

public class BmiActivity extends Activity {
    private EditText bmi_height, bmi_weight,bmi;
    private Button calculate_bmi;
    private Context mContext;
    private RecyclerView rv_bmi ;

    private List<BmiResult> BmiHistory  = new ArrayList<BmiResult>();
    private BmiResultAdapter bmiResultadapter;



    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.bmi);
        mContext=BmiActivity.this;

        initViews();



    }
    public void initViews(){
        bmi_height = (EditText) findViewById(R.id. bmi_height);
        bmi_weight = (EditText) findViewById(R.id. bmi_weight);
        bmi        = (EditText) findViewById(R.id. bmi);
        calculate_bmi= (Button) findViewById(R.id.calculate_bmi);
        getBmiData();
        calculate_bmi.setOnClickListener(new View.OnClickListener() {



            @Override
            public void onClick(View v) {
                if(checkValidation()) {
                    float height = Float.valueOf(bmi_height.getText().toString());
                    float weight = Float.valueOf(bmi_weight.getText().toString());
                    float result = CalculateBmi(height, weight);
                    bmi.setText(String.valueOf(result));
                    rv_bmi.setAdapter(bmiResultadapter);
                }
            }
        });


        rv_bmi= (RecyclerView) findViewById(R.id.rv_bmi);




        LinearLayoutManager verticalLayoutmanager
                = new LinearLayoutManager(BmiActivity.this, LinearLayoutManager.VERTICAL, false);
        rv_bmi.setLayoutManager(verticalLayoutmanager);
        rv_bmi.setAdapter(bmiResultadapter);
        bmiResultadapter =new BmiResultAdapter(BmiHistory,mContext);



    }

    public  float CalculateBmi(float height ,float weight){
       float bmi = (float) (weight/((height*0.3048)*(height*0.3048)));
        DatabaseHelper db=new DatabaseHelper(mContext);
        String CREATE_BMI="create table IF not EXISTS tb1_bmi(id INTEGER PRIMARY KEY AUTOINCREMENT,Height text,Weight text,Bmi text)";
        db.InsertRecord(CREATE_BMI);
        String select_profile1 = "INSERT INTO tb1_bmi(Height,Weight,Bmi)values('"+height+"','"+ weight+"','"+bmi+"')";

        db.InsertRecord(select_profile1);
        Utils.showToast(mContext,"successfully insert data ");
        getBmiData();


        return  bmi;

    }

    public  void getBmiData(){
        String  select_profile  ="Select  * from tb1_bmi ORDER BY ID DESC";
        DatabaseHelper db=new DatabaseHelper(mContext);

        Cursor cur =  db.getAllRecord(select_profile);
        int count=cur.getCount();
        int totalcount=0;
        if (count>0) {
            if (cur.moveToFirst()) {
                do {
                    cur.getString(0);

                    BmiResult bmi = new BmiResult(cur.getString(cur.getColumnIndex("Height")),cur.getString(cur.getColumnIndex("Weight")),cur.getString(cur.getColumnIndex("Bmi")));
                    BmiHistory.add(bmi);


                }

                while (cur.moveToNext());

            }

        }
        bmiResultadapter =new BmiResultAdapter(BmiHistory,mContext);

        bmiResultadapter.notifyDataSetChanged();


    }


    public boolean checkValidation(){
        if(bmi_height.getText().toString().equals(""))
        {
            Utils.showToast(mContext,"Please fill your height");
            return false;
        }
        else if(bmi_weight.getText().toString().equals(""))
        {
            Utils.showToast(mContext,"Please fill your weight");
            return false;
        }
        return true;
    }
}
