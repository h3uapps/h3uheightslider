package com.vipulmedcare.h3u.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;

import com.vipulmedcare.h3u.R;


/**
 * Created by Andrew on 8/11/2017.
 */

public class Activity_contextual extends Activity implements View.OnClickListener {

  LinearLayout ll_corss,ll_tooltracks,ll_hopital,ll_schedulehealth,ll_needopd,ll_addfamily;
    private Context mContext;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conetxupal);

        mContext = Activity_contextual.this;


        ll_hopital= (LinearLayout) findViewById(R.id.ll_hopital);
        ll_schedulehealth= (LinearLayout) findViewById(R.id.ll_schedulehealth);
        ll_needopd= (LinearLayout) findViewById(R.id.ll_needopd);
        ll_addfamily= (LinearLayout) findViewById(R.id.ll_addfamily);
        ll_tooltracks= (LinearLayout) findViewById(R.id.ll_tooltracks);


        ll_hopital.setOnClickListener(this);
        ll_schedulehealth.setOnClickListener(this);
        ll_needopd.setOnClickListener(this);
        ll_addfamily.setOnClickListener(this);
        ll_tooltracks.setOnClickListener(this);

        ll_corss= (LinearLayout) findViewById(R.id.ll_corss);
        ll_corss.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_corss:
                finish();
                break;

            case R.id.ll_hopital:

                Intent i =new Intent(mContext,Activity_OPD.class);
                startActivity(i);
                break;
            case R.id.ll_schedulehealth:

                break;
            case R.id.ll_needopd:


                break;
            case R.id.ll_addfamily:

                break;
            case R.id.ll_tooltracks:


                break;
        }
    }

}
