package com.vipulmedcare.h3u.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.utils.ApiConstants;
import com.vipulmedcare.h3u.utils.NetworkUtil;
import com.vipulmedcare.h3u.utils.Utils;
import com.vipulmedcare.h3u.utils.Validation;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vipul on 7/19/2017.
 */

public class volley_test_login extends Activity implements View.OnClickListener {
    Button btn_Login;
    private EditText et_emailid, et_password;
    String username, password;
    private Context mContext;
    private TextInputLayout til_password, til_emailid;
    ProgressDialog mProgressDialog;
    Boolean eye_open = false;ProgressDialog pd;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_volley);

        mContext = volley_test_login.this;


        pd = new ProgressDialog(mContext);
        pd.setIndeterminate(true);
        pd.setMessage("Please wait...");
        pd.setCancelable(false);


        initiviews();

    }


    private void initiviews() {

        til_emailid = (TextInputLayout) findViewById(R.id.til_emailid);
        et_emailid = (EditText) findViewById(R.id.et_emailid);
        et_emailid.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                Validation.isName(til_emailid, et_emailid, false);
            }
        });

        final TextView tv_showHide = (TextView) findViewById(R.id.tv_showHide);
        tv_showHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eye_open) {
                    tv_showHide.setText("SHOW");
                    et_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    eye_open = false;
                } else {
                    tv_showHide.setText("HIDE");
                    et_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    eye_open = true;
                }
                et_password.setSelection(et_password.getText().length());
            }
        });
        // tv_showHide.setVisibility(View.GONE);
        tv_showHide.setVisibility(View.VISIBLE);
        til_password = (TextInputLayout) findViewById(R.id.tit_password);
        et_password = (EditText) findViewById(R.id.et_password);
        et_password.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (et_password.getText().toString().trim().equals("")) {
                    tv_showHide.setVisibility(View.VISIBLE);
                    // tv_showHide.setVisibility(View.GONE);
                } else {
                    tv_showHide.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                Validation.isPassword(til_password, et_password, false);
            }
        });


        btn_Login = (Button)findViewById(R.id.btn_login);
        btn_Login.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                Utils.hideSoftKeyboard(mContext, btn_Login);
                Login();

                break;
        }
    }

    private void Login() {

        username = et_emailid.getText().toString().trim();
        password = et_password.getText().toString().trim();
        final String LoginType = "1", CorporateId = "";

        if (NetworkUtil.isNetworkAvailable(mContext)) {
            if (checkValidation()) {
                pd.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.APP_Login,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                System.out.println(response);
                                //   Log.d("Response volley", response);
                                //  Toast.makeText(volley_test_login.this,response,Toast.LENGTH_LONG).show();
                                pd.dismiss();
                                try {
                                    JSONObject json = new JSONObject(response);
                                    System.out.println(response);
                                    String check = json.getString("success");

                                    if (check.toLowerCase().equals("true")) {
                                        JSONArray pm = json.getJSONArray("data");
                                        JSONObject c = pm.getJSONObject(0);
                                        String id = c.getString("Id");
                                        String UserName = c.getString("UserName");
                                        String Name = c.getString("Name");
                                        String UserType = c.getString("UserType");
                                        String IsProfileVerfied = c.getString("IsProfileVerfied");
                                        String ProfileImage = c.getString("ProfileImage");
                                        String DocOnCall = c.getString("DocOnCall");
                                        String IsTempPwd = c.getString("IsTempPwd");
                                        String Mobile = c.getString("Mobile");
                                        String Debug = c.getString("Debug");

                                   /* Utils.setPreference(mContext , PreferenceKeys.Mobile,Mobile);
                                    Utils.ClearPreferences(mContext);
                                    String  mob=Utils.getPreferences(mContext , PreferenceKeys.Mobile);*/

                                        Utils.showToast(mContext, "Hi " + Name + " Welcome to H3U");
                                        Intent i = new Intent(mContext, MainActivity.class);
                                        startActivity(i);
                                        finish();

                                    } else {
                                        Utils.showToast(mContext, json.getString("message"));
                                       // Toast.makeText(volley_test_login.this, "Wrong user name  ", Toast.LENGTH_LONG).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //  mProgressDialog.dismiss();
                                pd.dismiss();
                                Toast.makeText(volley_test_login.this, "Error, Please try again" + error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        })

                {
                    //This is for Headers If You Needed
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        String creds = String.format("%s:%s", "VIL1XuqUVnkq31L2PdCtjw==", "zxdwT819BSOhT9pmnadjeg==");
                        String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                        params.put("Authorization", auth);
                        return params;
                    }

                    //Pass Your Parameters here
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        //  params.put("username","andrew@gmail.com");
                        params.put("UserName", username);
                        params.put("Password", password);
                        params.put("CorporateId", CorporateId);
                        params.put("LoginType", LoginType);
                        System.out.println("Login SendData" + params);
                        return params;

                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(stringRequest);
            }
        }
    }

    private boolean checkValidation() {
        boolean result = true;

        if (!Validation.isEmailAddress(til_emailid, et_emailid, true)) result = false;
        if (!Validation.isPassword(til_password, et_password, true)) result = false;


        return result;
    }

}
