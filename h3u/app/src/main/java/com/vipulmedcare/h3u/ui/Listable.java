package com.vipulmedcare.h3u.ui;

public interface Listable {
    String getLabel();
}