package com.vipulmedcare.h3u.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.activity.AddFamilyMember;

import java.util.List;

/**
 * Created by vipul on 9/18/2017.
 */

public class AddMemberAdapter  extends RecyclerView.Adapter<AddMemberAdapter.MyViewHolder> {

    private List<String> verticalList;
    private Context mContext;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private de.hdodenhof.circleimageview.CircleImageView iv_family_rv;


        public MyViewHolder(View view) {
            super(view);

            iv_family_rv= (de.hdodenhof.circleimageview.CircleImageView) view.findViewById(R.id.iv_family_rv);

        }
    }


    public AddMemberAdapter(List<String> verticalList, Context mContext) {
        this.verticalList = verticalList;
        this.mContext=mContext;
    }

    @Override
    public AddMemberAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.add_member_adapter, parent, false);

        return new AddMemberAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AddMemberAdapter.MyViewHolder holder, final int position) {


    }

    @Override
    public int getItemCount() {
        return verticalList.size();
    }
}

