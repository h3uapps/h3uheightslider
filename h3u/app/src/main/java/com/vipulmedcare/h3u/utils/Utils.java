package com.vipulmedcare.h3u.utils;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.db.DBManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;


public class Utils {

    private static SharedPreferences mAppPreferences;
    private static Editor mEditor;



    //......... show toast .........//

    public static void showToast(Context context, String message) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(R.layout.toast_layout,null);
        TextView tv_toast = (TextView) layout.findViewById(R.id.tv_toast);
        tv_toast.setText(message);
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.BOTTOM, 0,60);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }


    public static String Createbase64Image(String FileName) {
        try {
            // hide video preview

            // bimatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 3;

            final Bitmap bitmap = BitmapFactory.decodeFile(FileName,
                    options);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,bytes);
            String img_str = Base64.encodeToString(bytes.toByteArray(), 0);
            return img_str;

        } catch (NullPointerException e) {
            e.printStackTrace();
            return  "";
        }
    }

    //
    public static void createDirectoryAndSaveFile(Bitmap imageToSave, String fileName) {

        File direct = new File(Environment.getExternalStorageDirectory() + "/H3u");

        if (!direct.exists()) {
            File wallpaperDirectory = new File("/sdcard/H3u");
            wallpaperDirectory.mkdirs();
        }

        File file = new File(new File("/sdcard/H3u"), fileName);

        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //
    //......... show SoftKeyboard .........//

    public static void showSoftKeyboard(final Context cntx, final EditText et) {
        et.requestFocus();
        new Handler().postDelayed(new Runnable() {
            public void run() {
                final InputMethodManager imm = (InputMethodManager) cntx.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
            }
        }, 0);
    }

    //......... hide SoftKeyboard .........//

    public static void hideSoftKeyboard(Context cntx, View v) {
        final InputMethodManager imm = (InputMethodManager) cntx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static boolean addPreferenceBoolean(Context context, String pref_field, Boolean pref_value) {
        mAppPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mEditor = mAppPreferences.edit();
        mEditor.putBoolean(pref_field, pref_value);
        mEditor.commit();
        return mAppPreferences.getBoolean(pref_field, false);
    }
    public static String addPreferenceString(Context context, String pref_field, String pref_value) {
        mAppPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mEditor = mAppPreferences.edit();
        mEditor.putString(pref_field, pref_value);
        mEditor.apply();
        return mAppPreferences.getString(pref_field, "null");
    }

    public static String getPreferenceString(Context context, String pref_field, String def_value) {
        mAppPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return mAppPreferences.getString(pref_field, def_value);
    }
    public static String getPreference(Context context, String pref_field, String def_value) {
        mAppPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return mAppPreferences.getString(pref_field, def_value);
    }

    public static boolean getPreferenceBoolean(Context context, String pref_field, Boolean def_value) {
        mAppPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return mAppPreferences.getBoolean(pref_field, def_value);
    }

    /*public static void showLocationSettingAlert(final Context context) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(context.getString(R.string.location_error_title))
                .setMessage(context.getString(R.string.location_error_msg))
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(intent);
                    }
                });
        AlertDialog alert = alertDialog.create();
        alert.show();
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(Color.WHITE);
        pbutton.setBackgroundColor(Color.GRAY);
    }*/


    //......... show NetworkSettingAlert .........//

    public static void showNetworkSettingAlert(final Context context) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(context.getString(R.string.network_error_title));
        alertDialog.setMessage(context.getString(R.string.network_error_msg));
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                context.startActivity(intent);
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setBackgroundColor(Color.GRAY);

        pbutton.setTextColor(Color.CYAN);
    }

    public static void saveStringFieldSpinner(Context mContext, String fieldName, String fieldValue) {
        if (fieldValue == null) {
            Utils.addPreferenceString(mContext, fieldName, "");
        } else {
            Utils.addPreferenceString(mContext, fieldName, fieldValue);
        }
    }
    public static void showErrorAlert(final Context context, String Message) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Mandatory Fields");
        alertDialog.setMessage(Message);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setBackgroundColor(Color.GRAY);

        pbutton.setTextColor(Color.CYAN);
    }

    public static void alertbypasspaymentGateway(final Context context, String Message, String Title) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(Title);
        alertDialog.setMessage(Message);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setBackgroundColor(Color.GRAY);

        pbutton.setTextColor(Color.CYAN);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();

        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT));
            }

            listItem.measure(0, 0);
            float x =  listItem.getMeasuredHeight();
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount()));
        listView.setLayoutParams(params);
    }
    public static void showAlert(final Context mContext, String Message, String Title) {
        final Dialog myDialog = new Dialog(mContext);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.dialog_myalert);
        final TextView tv_title = (TextView) myDialog.findViewById(R.id.tv_title);
        tv_title.setText(Title);
        final TextView tv_message = (TextView) myDialog.findViewById(R.id.tv_message);
        tv_message.setText(Message);
        final Button btn_security_submit = (Button) myDialog.findViewById(R.id.btn_ok);
        btn_security_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            myDialog.dismiss();
        }});
        myDialog.show();
    }


   /* public static void ShowToast(Context context, String msg){
        LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(R.layout.toast_layout,null);
        TextView tv_toast = (TextView) layout.findViewById(R.id.tv_toast);
        tv_toast.setText(msg);
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.BOTTOM, 0,60);

        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }*/

    public static void CustomDialog(final Context mContext, String Message, String Title) {
        final Dialog myDialog = new Dialog(mContext);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.dialog_myalert);
        final TextView tv_title = (TextView) myDialog.findViewById(R.id.tv_title);
        tv_title.setText(Title);
        final TextView tv_message = (TextView) myDialog.findViewById(R.id.tv_message);
        tv_message.setText(Message);
        final Button btn_security_submit = (Button) myDialog.findViewById(R.id.btn_ok);
        btn_security_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }});
        myDialog.show();
    }

   /* private void switchFragment(Fragment mTarget) {
        String backStateName = mTarget.getClass().getName();

        FragmentManager manager = getFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left, R.anim.slide_in_from_left, R.anim.slide_out_to_right);
            ft.replace(R.id.frame_container, mTarget);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }*/
    //

   /* public static void setImage(final ImageView img_view, String img_url, Context mContext) {
//        Picasso.with(mContext).invalidate(img_url);
        Picasso.with(mContext)
                .load(img_url)
                .placeholder(R.anim.progress_animation)
                .error(R.mipmap.ic_launcher)
                .fit()
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(img_view, new com.squareup.picasso.Callback() {

                    @Override
                    public void onSuccess() {
                        img_view.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {
                        ;
                    }
                });

    }

    public static void setImage1(final ImageView img_view, String img_url, Context mContext) {
//        Picasso.with(mContext).invalidate(img_url);
        Picasso.with(mContext)
                .load(img_url)
                .placeholder(R.anim.progress_animation)
                .error(R.mipmap.ic_launcher)
                .fit()
                .into(img_view, new com.squareup.picasso.Callback() {

                    @Override
                    public void onSuccess() {
                        img_view.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {
                        ;
                    }
                });

    }
    public static void setImageUser(final ImageView img_view, String img_url, Context mContext) {
//        Picasso.with(mContext).invalidate(img_url);
        Picasso.with(mContext)
                .load(img_url)
                .placeholder(R.anim.progress_animation)
                .error(R.drawable.img_upload)
                .fit()
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(img_view, new com.squareup.picasso.Callback() {

                    @Override
                    public void onSuccess() {
                        img_view.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {
                        ;
                    }
                });

    }

    public static void loadImage(final ImageView img_view, String img_url, Context mContext, int errorResId) {
        Picasso.with(mContext)
                .load(img_url)
                .placeholder(R.anim.progress_animation)
                .error(errorResId)
                .fit()
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(img_view, new com.squareup.picasso.Callback() {

                    @Override
                    public void onSuccess() {
                        img_view.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {
                        ;
                    }
                });

    }*/

    public static void addImageToGallery(final String filePath, final String title, final Context context) {

        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.MediaColumns.DATA, filePath);
        values.put(MediaStore.Images.Media.TITLE, title);
        values.put(MediaStore.Images.Media.DISPLAY_NAME, title);
//        values.put(Images.Media.DESCRIPTION, description);
        // Add the date meta data to ensure the image is added at the front of the gallery
        values.put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());

        context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }

    public static String checkDigit(int number)
    {
        return number<=9?"0"+number: String.valueOf(number);
    }

    public static void setFont(Context mContext, EditText editText)
    {
        Typeface type = Typeface.createFromAsset(mContext.getAssets(),"fonts/Frutiger LT 55 Roman.ttf");
        editText.setTypeface(type);
    }


    // ..................................sqlite data base  .................................\\

    public static void setPreference(Context mcontext , String Key ,String Value){
        DBManager  dbManager = new DBManager(mcontext);
        dbManager.open();
        if(dbManager.Check(Key)==-1)

        { dbManager.insert(Key, Value);}
        else
            dbManager.update(dbManager.Check(Key),Key,Value);
        dbManager.close();


    }
    public static String getPreferences(Context mcontext, String Key){
        DBManager  dbManager = new DBManager(mcontext);
        dbManager.open();
        if(dbManager.Check(Key)!=-1)

        {   String value =  dbManager.Search(Key);
            dbManager.close();
            return  value;}
        else {
            dbManager.close();
            return "Not Found...!";
        }
    }

    public static void ClearPreferences(Context mcontext){
        DBManager  dbManager = new DBManager(mcontext);
        dbManager.open();
        dbManager.clearData();
        dbManager.close();
    }


}
