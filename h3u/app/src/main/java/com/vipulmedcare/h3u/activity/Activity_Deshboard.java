package com.vipulmedcare.h3u.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.health_checkup.Health_Checkup_Opd;
import com.vipulmedcare.h3u.utils.Utils;
import com.vipulmedcare.h3u.view.text.CustomTextView;
import com.vipulmedcare.h3u.view.text.CustomTextViewBold;


/**
 * Created by Andrew Pj on 8/10/2017.
 */

public class Activity_Deshboard extends AppCompatActivity implements View.OnClickListener {

    private static ViewPager mPager;
    private Context mContext;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    public   Dialog dialog;
    LinearLayout lin_hameburge,ll_cards,lin_contextul,lin_chat,lin_famely;
    private ViewPager viewPager;
    TextView tv_calculate;
    ImageView img_Logo;
    TextView tv_card,tv_famaily,tv_chat;

    TextView tv_reco,tv_calculate_height,tv_calculate_bmi,tv_bmi,tv_calculate_weigh,tv_incm,tv_inkg,tv_days;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cdeshboard);
        mContext = Activity_Deshboard.this;
        initviews();

        // showDiscount();
    }

    private void initviews() {

      /*  tv_calculate= (TextView) findViewById(R.id.tv_calculate);

        Typeface m1 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Bold.ttf");
        tv_calculate.setTypeface(m1);*/

        tv_famaily= (CustomTextView) findViewById(R.id.tv_famaily);
        tv_card= (CustomTextView) findViewById(R.id.tv_card);
        tv_chat= (CustomTextView) findViewById(R.id.tv_chat);

        tv_inkg=(CustomTextView) findViewById(R.id.tv_inkg);
        tv_days= (CustomTextView) findViewById(R.id.tv_days);
        tv_incm= (CustomTextView) findViewById(R.id.tv_incm);

        tv_reco= (CustomTextView) findViewById(R.id.tv_reco);
        tv_calculate_height= (CustomTextViewBold) findViewById(R.id.tv_calculate_height);
        tv_calculate_bmi= (CustomTextViewBold) findViewById(R.id.tv_calculate_bmi);
        tv_bmi= (CustomTextViewBold) findViewById(R.id.tv_bmi);
        tv_calculate_weigh= (CustomTextViewBold)findViewById(R.id.tv_calculate_weigh);


       /* Typeface m1 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        tv_famaily.setTypeface(m1);
        Typeface m2 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        tv_card.setTypeface(m2);
        Typeface m3 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Light.ttf");
        tv_chat.setTypeface(m3);*/


        lin_famely= (LinearLayout) findViewById(R.id.lin_famely);
        lin_famely.setOnClickListener(this);
        lin_chat= (LinearLayout) findViewById(R.id.lin_chat);
        lin_chat.setOnClickListener(this);
        img_Logo= (ImageView) findViewById(R.id.img_Logo);
        img_Logo.setOnClickListener(this);

        ll_cards= (LinearLayout) findViewById(R.id.ll_cards);
        ll_cards.setOnClickListener(this);
        lin_hameburge= (LinearLayout) findViewById(R.id.lin_hameburge);
        lin_hameburge.setOnClickListener(this);
        lin_contextul= (LinearLayout) findViewById(R.id.lin_contextul);
        lin_contextul.setOnClickListener(this);
        mPager = (ViewPager)findViewById(R.id.pager2);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_cards:
              //  Utils.showAlert(mContext,"Work in progress","work in progress");
               // if (checkValidation()) {
                    // signup();
                    // Intent I = new Intent(mContext,Activity_number.class);

              //  }

                Intent n=new Intent(Activity_Deshboard.this,OPDCardsAcivity.class);
                startActivity(n);
                break;

            case R.id.lin_hameburge:
                Intent p=new Intent(Activity_Deshboard.this,Activity_drawer.class);
                startActivity(p);
                break;

            case R.id.lin_contextul:
                /*Intent p=new Intent(Activity_Deshboard.this,Activity_contextual.class);
                startActivity(p);*/
                CustomDialog();
                break;

            case R.id.img_Logo:
                Intent i=new Intent(Activity_Deshboard.this,Activity_Switchcard.class);
                startActivity(i);
                break;
            case R.id.lin_famely:
                Intent t=new Intent(Activity_Deshboard.this,AddFamily.class);
                startActivity(t);
                //Utils.showAlert(mContext,"Work in progress","work in progress");
                break;
            case R.id.lin_chat:
               // Utils.showAlert(mContext,"Work in progress","work in progress");
                showDialog();
                break;
        }

    }

    public void showDialog(){
        final Dialog dialog = new Dialog(Activity_Deshboard.this, android.R.style.DeviceDefault_Light_ButtonBar);
        dialog.setContentView(R.layout.full_screen_dialog_layout);
        dialog.getWindow().setBackgroundDrawableResource(R.color.dialog_transparent);
        CustomTextView change_city_dialog_close = (CustomTextView) dialog.findViewById(R.id.change_city_dialog_close);
        change_city_dialog_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void CustomDialog() {

        final Dialog dialog = new Dialog(Activity_Deshboard.this, android.R.style.DeviceDefault_Light_ButtonBar);
        dialog.setContentView(R.layout.activity_conetxupal);
        dialog.getWindow().setBackgroundDrawableResource(R.color.dialog_transparent);
        LinearLayout ll_corss = (LinearLayout) dialog.findViewById(R.id.ll_corss);
        ll_corss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        LinearLayout ll_hopital= (LinearLayout) dialog.findViewById(R.id.ll_hopital);
        ll_hopital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.showAlert(mContext, "Near by Hospital", "Work in progress");
            }
        });
        LinearLayout ll_addcard= (LinearLayout) dialog.findViewById(R.id.ll_addcard);
        ll_addcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showAlert(mContext, "ADD New Card", "Work in progress");
            }
        });
        LinearLayout ll_schedulehealth= (LinearLayout) dialog.findViewById(R.id.ll_schedulehealth);
        ll_schedulehealth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Activity_Deshboard.this,Health_Checkup_Opd.class);
                startActivity(i);
                dialog.dismiss();
            }
        });

        LinearLayout ll_needopd= (LinearLayout) dialog.findViewById(R.id.ll_needopd);
        ll_needopd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Activity_Deshboard.this,Activity_OPD.class);
                startActivity(i);
                dialog.dismiss();
            }
        });

        LinearLayout ll_tooltracks= (LinearLayout)dialog.findViewById(R.id.ll_tooltracks);
        ll_tooltracks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Activity_Deshboard.this,Tools_tracker_main_Activity.class);
                startActivity(i);
                dialog.dismiss();
            }
        });

        dialog.show();

    }

   /* private void showDiscount() {
        if (NetworkUtil.isNetworkAvailable(mContext)) {
            final Call<TestimonialMResponse> getTestimonial = CustomApplication.getRestClient().getService().getTestimonials();
            getTestimonial.enqueue(new Callback<TestimonialMResponse>() {
                @Override
                public void onResponse(Response<TestimonialMResponse> response, Retrofit retrofit) {

                    if (response.body().getSuccess()) {

                        ArrayList<TestimonialResponce> list = response.body().getData();


                        ArrayList<String> text = new ArrayList<String>();
                        for (int i = 0; i < list.size(); i++) {
                            String text1 = response.body().getData().get(i).getTestim();
                            text.add(text1);
                        }
                        mPager.setAdapter(new SlidingImage_Adapter(mContext, text));
                        CirclePageIndicator indicator = (CirclePageIndicator)findViewById(R.id.indicator);
                        indicator.setViewPager(mPager);
                        try {
                            final float density = getResources().getDisplayMetrics().density;
//Set circle indicator radius
                            indicator.setRadius(5 * density);

                            NUM_PAGES = text.size();

                            // Auto start of viewpager
                            final Handler handler = new Handler();
                            final Runnable Update = new Runnable() {
                                public void run() {
                                    if (currentPage == NUM_PAGES) {
                                        currentPage = 0;
                                    }
                                    mPager.setCurrentItem(currentPage++, true);
                                }
                            };
                            Timer swipeTimer = new Timer();
                            swipeTimer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    handler.post(Update);
                                }
                            }, 10000, 10000);

                            // Pager listener over indicator
                            indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                                @Override
                                public void onPageSelected(int position) {
                                    currentPage = position;

                                }

                                @Override
                                public void onPageScrolled(int pos, float arg1, int arg2) {

                                }

                                @Override
                                public void onPageScrollStateChanged(int pos) {

                                }
                            });
                        } catch (Exception e) {

                        }

                    }
                }

            });

        } else {
//            Utils.showNetworkSettingAlert(mContext);
        }
    }*/
}
