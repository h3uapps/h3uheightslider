package com.vipulmedcare.h3u.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.vipulmedcare.h3u.fragment.Corporate_profile;
import com.vipulmedcare.h3u.fragment.Personal_Profile;


/**
 * Created by Andrew pj
 */

public class Profile_PagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 2;
    private String tabTitles[] = new String[] {"PERSONAL","CORPORATE"};
    private Context context;

    public Profile_PagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                return new Personal_Profile();
            case 1:
                return new Corporate_profile();

            case 2:
                return new Personal_Profile();

        }

        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return PAGE_COUNT;
    }

    public CharSequence getPageTitle(int position) {
        // Generate title based on item position

        return tabTitles[position];
    }

}
