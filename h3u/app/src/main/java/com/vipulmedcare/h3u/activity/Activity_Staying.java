package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.base.BaseActivity;


/**
 * Created by vipul on 8/3/2017.
 */

public class Activity_Staying extends BaseActivity {
    Button btn_login;
    TextView toolbarText;
    private Context mContext;
    TextView tv_verify;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staying);

        //    .......  heading .... ................        //
       /* toolbarText =(TextView) findViewById(R.id.tv_toolbarText);
        toolbarText.setText("SignUp");
*/
        mContext = Activity_Staying.this;
        initviews();
    }

    private void initviews() {
        tv_verify= (TextView) findViewById(R.id.tv_verify);
        tv_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i =new Intent(Activity_Staying.this,Activity_styingtwo.class);
                startActivity(i);

            }
        });

    }
}
