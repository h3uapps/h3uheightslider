package com.vipulmedcare.h3u.registration;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.utils.Utils;


/**
 * Created by vipul on 9/4/2017.
 */

public class Activity_AllreadyExits extends Activity {
    Button btn_next;
    private Context mContext;
    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.allready_exits);

        mContext = Activity_AllreadyExits.this;
        initviews();
    }

    private void initviews() {

        btn_next= (Button) findViewById(R.id.btn_next);

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.showAlert(mContext,"Already Exist","");
            }
        });
    }
}
