package com.vipulmedcare.h3u.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.base.BaseActivity;


/**
 * Created by Andrew pj on 8/31/2017.
 */

public class FirstOpinion_Form extends BaseActivity {

    Button btn_submit;
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.first_opinion_form);

        btn_submit= (Button) findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I=new Intent(FirstOpinion_Form.this,Submit_Form.class);
                startActivity(I);
            }
        });


    }
}
