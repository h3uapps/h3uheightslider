package com.vipulmedcare.h3u.view.components;/*
package com.vipulmedcare.h3u20.view.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.vipulmedcare.h3u20.R;


*/
/**
 * Created by vjangra on 19/05/16.
 *//*

public class CustomButtonIcon extends RelativeLayout {

    private ImageView iconView;
    private ImageView iconOutline;
    private ImageView iconBase;

    public CustomButtonIcon(Context context) {
        super(context, null);
    }

    public CustomButtonIcon(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_button_icon, this, true);

        iconView = (ImageView) findViewById(R.id.btnIcon);
        iconOutline = (ImageView) findViewById(R.id.btnOutline);
        iconBase = (ImageView) findViewById(R.id.btnBase);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.CustomButtonIcon, 0, 0);

        int baseColor = attributes.getColor(R.styleable.CustomButtonIcon_cbiBackColor, 0);
        int outlineColor = attributes.getColor(R.styleable.CustomButtonIcon_cbiIconOutlineColor, 0);
        int iconTintColor = attributes.getColor(R.styleable.CustomButtonIcon_cbiIconDrawableTint, 0);

        int padding = attributes.getInteger(R.styleable.CustomButtonIcon_cbiIconPadding, 10);
        float density = context.getResources().getDisplayMetrics().density;
              padding = Math.round(padding * density);

        Drawable icon = attributes.getDrawable(R.styleable.CustomButtonIcon_cbiIconDrawable);

        attributes.recycle();

        if(baseColor != 0) iconBase.setColorFilter(baseColor);
        else iconBase.setVisibility(GONE);

        if(outlineColor != 0) iconOutline.setColorFilter(outlineColor);
        else iconOutline.setVisibility(GONE);

        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(iconView.getLayoutParams());
        lp.setMargins(padding,padding,padding,padding);
        iconView.setLayoutParams(lp);

        if(icon != null) iconView.setImageDrawable(icon);
        iconView.setColorFilter(iconTintColor);
    }

}
*/
