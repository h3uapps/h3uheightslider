package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.utils.Utils;
import com.vipulmedcare.h3u.view.text.CustomTextView;

/**
 * Created by Andrew Pj on 8/24/2017.
 */

public class Activity_Enter_TestName extends BaseActivity implements View.OnClickListener {

    ImageView ent_spcl_cross,ent_health_back;
    private Context mContext;
    TextView tv_headertext;
    EditText edit_showlist;
    TextView tv_testname,tv_hname;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_testname);

        mContext = Activity_Enter_TestName.this;
        initViews();

    }

    public void initViews(){

        //    .......  heading .... ................        //
        tv_headertext =(TextView) findViewById(R.id.tv_headertext);
        tv_headertext.setText("HEALTH CHECKUP");


        tv_hname= (CustomTextView) findViewById(R.id.tv_hname);
        tv_testname= (CustomTextView) findViewById(R.id.tv_testname);

        edit_showlist= (EditText) findViewById(R.id.edit_showlist);
        ent_spcl_cross = (ImageView) findViewById(R.id.ent_spcl_cross);
        ent_health_back= (ImageView) findViewById(R.id.ent_health_back);

        ent_spcl_cross.setOnClickListener( this);
        ent_health_back.setOnClickListener( this);
        edit_showlist.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.ent_health_back:
                finish();
                break;

            case R.id.ent_spcl_cross:
                finish();
                break;

            case R.id.edit_showlist:
                Utils.showAlert(mContext, "Work in progress", "Work in progress");
                break;
        }

    }


}

