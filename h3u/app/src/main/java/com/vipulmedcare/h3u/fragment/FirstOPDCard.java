package com.vipulmedcare.h3u.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vipulmedcare.h3u.R;


/**
 * Created by vipul on 9/1/2017.
 */

public class FirstOPDCard  extends Fragment {




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.first_opd_card, container, false);

        return rootView;
    }
}
