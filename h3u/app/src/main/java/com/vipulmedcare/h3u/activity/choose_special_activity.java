package com.vipulmedcare.h3u.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.MyChooseAdapter;
import com.vipulmedcare.h3u.pojo.listitem;
import com.vipulmedcare.h3u.view.text.CustomTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew Pj on 8/3/2017.
 */

public class choose_special_activity extends Activity implements View.OnClickListener {

    Button chs_spl_nxt;
    ImageView ent_spcl_cross,ent_spcl_back;
    private Context mContext;
    RecyclerView consult_select_rv;
   TextView tv_headertext,tv_cname,tv_sname;
    private MyChooseAdapter adapter;

    private List<listitem> movieList = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_special_activity);
        mContext = choose_special_activity.this;

        initViews();
    }

    public void initViews(){

        //    .......  heading .... ................        //
        tv_headertext =(TextView) findViewById(R.id.tv_headertext);
        tv_headertext.setText("OPD CONSULTATION");
        consult_select_rv= (RecyclerView) findViewById(R.id.consult_select_rv);
        consult_select_rv.setOnClickListener(this);
        chs_spl_nxt= (Button) findViewById(R.id.chs_spl_nxt);
        chs_spl_nxt.setOnClickListener(this);
        ent_spcl_cross= (ImageView) findViewById(R.id.ent_spcl_cross);
        ent_spcl_cross.setOnClickListener(this);
        ent_spcl_back= (ImageView) findViewById(R.id.ent_spcl_back);
        ent_spcl_back.setOnClickListener(this);


        tv_cname= (CustomTextView) findViewById(R.id.tv_cname);
        tv_sname= (CustomTextView) findViewById(R.id.tv_sname);



        adapter = new MyChooseAdapter(mContext,movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        consult_select_rv.setLayoutManager(mLayoutManager);
        consult_select_rv.setItemAnimator(new DefaultItemAnimator());
        consult_select_rv.setAdapter(adapter);

        prepareMovieData();
    }

    private void prepareMovieData() {
        listitem movie = new listitem("Complete Blood Count", "", "");
        movieList.add(movie);
        movie = new listitem("Orthopedics", "", "");
        movieList.add(movie);

        movie = new listitem("Name of the Specialization", "", "");
        movieList.add(movie);
        movie = new listitem("Orthopedics", "", "");
        movieList.add(movie);

        movie = new listitem("Name of the Specialization", "", "");
        movieList.add(movie);

        movie = new listitem("Orthopedics", "", "");
        movieList.add(movie);

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.chs_spl_nxt:

                Intent intent1 = new Intent(mContext,provider_map_activity.class );
                startActivity(intent1);
                break;

            case R.id.ent_spcl_cross:
                finish();
                break;

            case R.id.ent_spcl_back:
                finish();
                /*Intent intent3 = new Intent(mContext,OpdConsultActivity.class );
                startActivity(intent3);*/
                break;
        }

    }
}
