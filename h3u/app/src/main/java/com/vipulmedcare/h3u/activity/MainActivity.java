package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.UserQuestionAdapter;
import com.vipulmedcare.h3u.utils.Utils;

import jp.wasabeef.recyclerview.animators.FadeInAnimator;
import jp.wasabeef.recyclerview.animators.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.adapters.ScaleInAnimationAdapter;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private UserQuestionAdapter adapter;
    private RecyclerView recyclerView;
    Context mContext;
    ImageView img_user;
    TextView text_name,text_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        img_user= (ImageView) findViewById(R.id.img_user);
        text_name= (TextView) findViewById(R.id.text_name);
        text_email= (TextView) findViewById(R.id.text_email);

        recyclerView = (RecyclerView)findViewById(R.id.rv_list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new FadeInAnimator());

        adapter = new UserQuestionAdapter();
        adapter.notifyDataSetChanged();
        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
        ScaleInAnimationAdapter scaleAdapter = new ScaleInAnimationAdapter(alphaAdapter);
        recyclerView.setAdapter(scaleAdapter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        //adapter = new UserQuestionAdapter(mContext, getActivity(), list);
      /*  adapter = new UserQuestionAdapter();
        adapter.notifyDataSetChanged();
        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
        ScaleInAnimationAdapter scaleAdapter = new ScaleInAnimationAdapter(alphaAdapter);
        recyclerView.setAdapter(scaleAdapter);*/
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.m_call) {
             Utils.showToast(getApplicationContext(),"Work in progress");

            return true;
        }
        if (id == R.id.m_notification) {
            Utils.showToast(getApplicationContext(),"Work in progress");
            return true;
        }
        if (id == R.id.m_login) {
            Utils.showToast(getApplicationContext(),"Work in progress");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            Utils.showToast(getApplicationContext(),"Work in progress");
            Intent i=new Intent(MainActivity.this, FAQ_Activity.class);
            startActivity(i);
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {
            Utils.showToast(getApplicationContext(),"Work in progress");
            Intent i=new Intent(MainActivity.this, SignUp_Activity.class);
            startActivity(i);
        } else if (id == R.id.nav_slideshow) {
            Utils.showToast(getApplicationContext(),"Volley");
            Intent i=new Intent(MainActivity.this, Volley_test.class);
            startActivity(i);
        } else if (id == R.id.nav_manage) {
            Intent i=new Intent(MainActivity.this, volley_test_login.class);
            startActivity(i);


        } else if (id == R.id.nav_share) {
            Utils.showToast(getApplicationContext(),"Work in progress");
        }
        else if (id == R.id.item_faq) {
            Intent i=new Intent(MainActivity.this, FAQ_Activity.class);
            startActivity(i);
        }else if (id==R.id.nav_about){
            Intent i=new Intent(MainActivity.this,About_Activity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }




}
