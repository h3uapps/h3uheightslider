

package com.vipulmedcare.h3u.profile;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.activity.BmiActivity;
import com.vipulmedcare.h3u.adapter.BmiResultAdapter;
import com.vipulmedcare.h3u.db.DatabaseHelper;
import com.vipulmedcare.h3u.pojo.BmiResult;
import com.vipulmedcare.h3u.utils.Utils;

import java.util.ArrayList;
import java.util.List;


public class Bio_Fragment extends Fragment {
    private EditText bmi_height, bmi_weight,bmi;
    private Button calculate_bmi,calculate_bmi_dialog;
    private Context mContext;
    private RecyclerView rv_bmi ;

    private List<BmiResult> BmiHistory  = new ArrayList<BmiResult>();
    private BmiResultAdapter bmiResultadapter;

    public static Bio_Fragment newInstance() {
        Bio_Fragment fragment = new Bio_Fragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mContext=getContext();

        View rootView = inflater.inflate(R.layout.bmi, container, false);
        getBmiData();

        calculate_bmi_dialog = (Button) rootView.findViewById(R.id.calculate_bmi_dialog);
        calculate_bmi_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BmiDialog();
            }
        });

        rv_bmi= (RecyclerView) rootView.findViewById(R.id.rv_bmi);

        LinearLayoutManager verticalLayoutmanager
                = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rv_bmi.setLayoutManager(verticalLayoutmanager);
        rv_bmi.setAdapter(bmiResultadapter);
        bmiResultadapter =new BmiResultAdapter(BmiHistory,mContext);

        return rootView;
    }


    public  float CalculateBmi(float height ,float weight){
        float bmi = (float) (weight/((height*0.3048)*(height*0.3048)));
        DatabaseHelper db=new DatabaseHelper(mContext);
        String CREATE_BMI="create table IF not EXISTS tb1_bmi(id INTEGER PRIMARY KEY AUTOINCREMENT,Height text,Weight text,Bmi text)";
        db.InsertRecord(CREATE_BMI);
        String select_profile1 = "INSERT INTO tb1_bmi(Height,Weight,Bmi)values('"+height+"','"+ weight+"','"+bmi+"')";

        db.InsertRecord(select_profile1);
        Utils.showToast(mContext,"successfully insert data ");
        getBmiData();
        return  bmi;

    }

    public  void getBmiData(){
        String  select_profile  ="Select  * from tb1_bmi ORDER BY ID DESC";
        DatabaseHelper db=new DatabaseHelper(mContext);

        Cursor cur =  db.getAllRecord(select_profile);
        int count=cur.getCount();
        int totalcount=0;
        if (count>0) {
            if (cur.moveToFirst()) {
                do {
                    cur.getString(0);
                    BmiResult bmi = new BmiResult(cur.getString(cur.getColumnIndex("Height")),cur.getString(cur.getColumnIndex("Weight")),cur.getString(cur.getColumnIndex("Bmi")));
                    BmiHistory.add(bmi);
                }
                while (cur.moveToNext());
            }
        }
        bmiResultadapter =new BmiResultAdapter(BmiHistory,mContext);
        bmiResultadapter.notifyDataSetChanged();



    }


    public boolean checkValidation(){
        if(bmi_height.getText().toString().equals(""))
        {
            Utils.showToast(mContext,"Please fill your height");
            return false;
        }
        else if(bmi_weight.getText().toString().equals(""))
        {
            Utils.showToast(mContext,"Please fill your weight");
            return false;
        }
        return true;
    }

    public void BmiDialog(){
        final Dialog myDialog = new Dialog(mContext);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.calculate_bmi);
//        myDialog.setContentView(R.layout.ecard_share_dialog);
        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = myDialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        bmi_height = (EditText) myDialog.findViewById(R.id. bmi_height);
        bmi_weight = (EditText) myDialog.findViewById(R.id. bmi_weight);
        bmi = (EditText) myDialog.findViewById(R.id. bmi);
        calculate_bmi= (Button) myDialog.findViewById(R.id.calculate_bmi);
        getBmiData();
        calculate_bmi.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(checkValidation()) {
                    float height = Float.valueOf(bmi_height.getText().toString());
                    float weight = Float.valueOf(bmi_weight.getText().toString());
                    float result = CalculateBmi(height, weight);
                    bmi.setText(String.valueOf(result));
                    rv_bmi.setAdapter(bmiResultadapter);
                    myDialog.dismiss();

                }
            }
        });

        myDialog.show();
    }
}
