package com.vipulmedcare.h3u.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by vipul on 7/17/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    // Table Name
    public static final String TABLE_NAME = "Preferences";
    private SQLiteDatabase database;
    // Table columns
    public static final String _ID = "_id";
    public static final String KEY = "KEY";
    public static final String Userid = "Userid";
    public static final String VALUE = "VALUEription";

    // Database Information
    static final String DB_NAME = "Preferences.DB";

    // database version
    static final int DB_VERSION = 1;
    // Creating table query
    private static final String CREATE_TABLE = "create table " + TABLE_NAME + "(" + _ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY + " TEXT NOT NULL, " + VALUE + " TEXT);";
    // create table basic profile
    private static final String CREATE_PROFILE = " create table  IF not  EXISTS tbl_profile(id INTEGER PRIMARY KEY AUTOINCREMENT,firstName text,LastName text,Mobile text,Gender text,DOB text,Emailid text,address text, State text, city text,isuploaded int ,profileimagepath text,Userid text)";
    // create table corporate
    private  static final String CREATE_CORPORATE="create table IF not EXISTS tb1_corporate(id INTEGER PRIMARY KEY AUTOINCREMENT,CorporateName text,CorporateEmail text,EmpID text)";

    // Create Table KYC
    private  static final String CREATE_KYC="create table IF not EXISTS tb1_corporate(id INTEGER PRIMARY KEY AUTOINCREMENT,FullNamen text,AadhaarNumber text)";

    // Create Table BMI
    private  static final String CREATE_BMI="create table IF not EXISTS tb1_bmi(id INTEGER PRIMARY KEY AUTOINCREMENT,Height text,Weight text,Bmi text)";
    /*private static String  select_profile  ="Select * from tbl_profile where Userid='"+Userid +"'";*/

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
        db.execSQL(CREATE_PROFILE);
        db.execSQL(CREATE_CORPORATE);
        db.execSQL(CREATE_KYC);
        db.execSQL(CREATE_BMI);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
    //
    public Cursor getAllRecord(String strQuery)
    {
        database = this.getWritableDatabase();
        Cursor cur= database.rawQuery(strQuery,null);

        return cur;
    }
    public void InsertRecord(String strQuery)
    {
        try
        {
            database = this.getWritableDatabase();

            database.execSQL(strQuery);
            database.close();
        }
        catch (Exception e)
        {

            e.toString();
            database.close();
        }

    }
    //
}