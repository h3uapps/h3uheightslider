package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.AddMemberAdapter;
import com.vipulmedcare.h3u.adapter.VerticalAdapter;
import com.vipulmedcare.h3u.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vipul on 9/18/2017.
 */

public class AddMember extends BaseActivity {
private RecyclerView rv_added_member;
private AddMemberAdapter addMemberAdapter;
    private Context mContext;
    private List members;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.add_member);
        mContext=AddMember.this;

        initViews();

    }

    public void initViews(){
        members = new ArrayList();
        members.add("1");
        members.add("1");
        members.add("1");
        rv_added_member = (RecyclerView) findViewById(R.id.rv_added_member);
        addMemberAdapter=new AddMemberAdapter(members,mContext);
        LinearLayoutManager verticalLayoutmanager
                = new LinearLayoutManager(AddMember.this, LinearLayoutManager.VERTICAL, false);
        rv_added_member.setLayoutManager(verticalLayoutmanager);

        rv_added_member.setAdapter(addMemberAdapter);



    }
}
