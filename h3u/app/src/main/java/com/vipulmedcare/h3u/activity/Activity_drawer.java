package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.profile.Activity_Main_Profile;
import com.vipulmedcare.h3u.utils.Utils;
import com.vipulmedcare.h3u.view.text.CustomTextView;

/**
 * Created by Andrew Pj on 8/10/2017.
 */

public class Activity_drawer extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    private Context mContext;
    ImageView img_navprofile,img_profile;
    TextView tv_name,tv_dob,btn_logout,ll_corss;
    LinearLayout ll_nav_healt,ll_nav_test,ll_nav_checkups,ll_nav_opd,ll_nav_online,ll_nav_tools,ll_nav_setting;
    TextView tv_healthstatics,tv_testresult,tv_healthch,tv_opdcon,tv_onlineop,tv_toolstracker,tv_setting;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_drawer);

       /* toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Deshboard ");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);*/

        mContext = Activity_drawer.this;
        initviews();
    }

    private void initviews() {

        img_navprofile= (ImageView) findViewById(R.id.img_navprofile);
        img_profile= (ImageView) findViewById(R.id.img_profile);
        tv_dob= (TextView) findViewById(R.id.tv_dob);
        btn_logout= (CustomTextView) findViewById(R.id.btn_logout);

        tv_name= (TextView) findViewById(R.id.tv_name);
        Typeface m1= Typeface.createFromAsset(getAssets(),"fonts/Lato-Bold.ttf");
        tv_name.setTypeface(m1);

        Typeface m2= Typeface.createFromAsset(getAssets(),"fonts/Lato.ttf");
        tv_dob.setTypeface(m2);

        ll_nav_healt= (LinearLayout) findViewById(R.id.ll_nav_healt);
        ll_nav_test= (LinearLayout) findViewById(R.id.ll_nav_test);
        ll_nav_checkups=(LinearLayout) findViewById(R.id.ll_nav_checkups);
        ll_nav_opd=(LinearLayout) findViewById(R.id.ll_nav_opd);
        ll_nav_online=(LinearLayout) findViewById(R.id.ll_nav_online);
        ll_nav_tools=(LinearLayout) findViewById(R.id.ll_nav_tools);
        ll_nav_setting= (LinearLayout) findViewById(R.id.ll_nav_setting);
        ll_corss= (TextView) findViewById(R.id.ll_corss);

        //

        tv_healthstatics= (CustomTextView) findViewById(R.id.tv_healthstatics);

        tv_testresult= (CustomTextView) findViewById(R.id.tv_testresult);
        tv_healthch= (CustomTextView) findViewById(R.id.tv_healthch);
        tv_opdcon= (CustomTextView) findViewById(R.id.tv_opdcon);
        tv_onlineop= (CustomTextView) findViewById(R.id.tv_onlineop);
        tv_toolstracker= (CustomTextView) findViewById(R.id.tv_toolstracker);
        tv_setting= (CustomTextView) findViewById(R.id.tv_setting);

       /* Typeface p1 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Lato.ttf");
        tv_healthstatics.setTypeface(p1);
        Typeface p2 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Lato.ttf");
        tv_testresult.setTypeface(p2);
        Typeface p3 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Lato.ttf");
        tv_healthch.setTypeface(p3);
        Typeface p4 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Lato.ttf");
        tv_opdcon.setTypeface(p4);
        Typeface p5 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Lato.ttf");
        tv_onlineop.setTypeface(p5);
        Typeface p6 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Lato.ttf");
        tv_toolstracker.setTypeface(p6);
        Typeface p7 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Lato.ttf");
        tv_setting.setTypeface(p7);
        Typeface p8 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Lato.ttf");
        ll_corss.setTypeface(p8);
        Typeface p9 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Lato.ttf");
        btn_logout.setTypeface(p9);*/

        //
       // img_navprofile.setOnClickListener(this);
        ll_nav_healt.setOnClickListener(this);
        ll_nav_checkups.setOnClickListener(this);
        ll_nav_test.setOnClickListener(this);
        ll_nav_opd.setOnClickListener(this);
        ll_nav_online.setOnClickListener(this);
        ll_nav_tools.setOnClickListener(this);
        ll_corss.setOnClickListener(this);
        btn_logout.setOnClickListener(this);
        ll_nav_setting.setOnClickListener(this);
        img_profile.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_nav_healt:
              //  if (checkValidation()) {
                Utils.showAlert(mContext, "Work in progress", "Work in progress");
              //  }
                break;
            case R.id.ll_nav_checkups:
                Intent a =new Intent(mContext,Activity_Healthcheckup.class);
                startActivity(a);
                break;
            case R.id.ll_nav_test:
                Intent i =new Intent(mContext,Activity_Testresult.class);
                startActivity(i);
              //  Utils.showToast(mContext,"Work in progress");
                break;
            case R.id.ll_nav_opd:
                Intent in =new Intent(mContext,Activity_Opd_Consult.class);
                startActivity(in);
                break;
            case R.id.ll_nav_online:
                Intent ip =new Intent(mContext,OnlineOpinionActivity.class);
                startActivity(ip);
                break;
            case R.id.ll_nav_tools:
                Intent io =new Intent(mContext,Tools_tracker_main_Activity.class);
                startActivity(io);
                break;
            case R.id.ll_corss:
                finish();
                break;
            case R.id.btn_logout:
                Utils.showToast(mContext,"Logout successfully");
                break;
            case R.id.ll_nav_setting:

                Intent ipo =new Intent(mContext,Test.class);
                startActivity(ipo);
              //  Utils.showAlert(mContext, "Work in progress", "Work in progress");
                break;
            case R.id.img_profile:
                Intent p=new Intent(mContext, Activity_Main_Profile.class);
                //Intent p=new Intent(mContext, Profile_Details_fragment.class);
                startActivity(p);
               // Utils.showAlert(mContext, "Work in progress", "Work in progress");
                break;

        }
    }
}
