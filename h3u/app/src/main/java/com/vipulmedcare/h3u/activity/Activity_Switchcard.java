package com.vipulmedcare.h3u.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.utils.Utils;


/**
 * Created by Andrew on 8/11/2017.
 */

public class Activity_Switchcard extends Activity {
    LinearLayout ll_corss;
    ImageView img_Logo;
    private Context mContext;
    RelativeLayout rl_personl;
    LinearLayout ll_corporate;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switchcard);

        mContext=Activity_Switchcard.this;
        ll_corporate= (LinearLayout) findViewById(R.id.ll_corporate);
        ll_corporate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showAlert(mContext,"Work in progress","Work in progress");
            }
        });
        rl_personl= (RelativeLayout) findViewById(R.id.rl_personl);
        rl_personl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Activity_Switchcard.this,CardDetail.class);
                startActivity(i);
            }
        });
        img_Logo= (ImageView) findViewById(R.id.img_Logo);
        ll_corss= (LinearLayout) findViewById(R.id.ll_corss);
        ll_corss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        img_Logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showToast(mContext," Successfully");
            }
        });

    }
}
