package com.vipulmedcare.h3u.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.myTestAdapter;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.pojo.listitem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vipul on 8/22/2017.
 */

public class Activity_Healthcheckupsecond extends BaseActivity {

    TextView tv_healthcross;
    RecyclerView rv_mytest;
    ProgressDialog pd;
    Context mContext;
    private myTestAdapter adapter;
    TextView tv_share;
    private List<listitem> movieList = new ArrayList<>();
    Button btn_recharge;
   ImageView img_pay;
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.health_checkupsecond);

        mContext = Activity_Healthcheckupsecond.this;
        initViews();

    }

    private void initViews() {

        btn_recharge= (Button) findViewById(R.id.btn_recharge);
        img_pay= (ImageView) findViewById(R.id.img_pay);
        tv_healthcross= (TextView) findViewById(R.id.tv_healthcross);
        tv_share= (TextView) findViewById(R.id.tv_share);
        rv_mytest= (RecyclerView) findViewById(R.id.rv_mytest);

        adapter = new myTestAdapter(mContext,movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_mytest.setLayoutManager(mLayoutManager);
        rv_mytest.setItemAnimator(new DefaultItemAnimator());
        rv_mytest.setAdapter(adapter);

        img_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              //  CustomDialogPay();
                Intent i =new Intent(mContext,Activity_Pay.class);
                startActivity(i);
            }
        });
        tv_healthcross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btn_recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(Activity_Healthcheckupsecond.this,Activity_Pay.class);
                startActivity(i);
            }
        });


        prepareMovieData();

      /*  pd = new ProgressDialog(Activity_Testresult.this,
                AlertDialog.THEME_HOLO_LIGHT);
        pd.setMessage("Loading...");
        pd.setIndeterminate(false);
        pd.setCancelable(false);
        pd.show();

        if (NetworkUtil.isNetworkAvailable(mContext)) {
                pd.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.APP_TESTRESULT,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                System.out.println(response);
                                //   Log.d("Response volley", response);
                                pd.dismiss();
                                try {
                                    JSONObject json = new JSONObject(response);
                                    System.out.println(response);
                                    String check = json.getString("success");

                                    if (check.toLowerCase().equals("true")) {
                                        JSONArray pm = json.getJSONArray("data");
                                        JSONObject c = pm.getJSONObject(0);
                                        String id = c.getString("Id");

                                       *//* List<TestData> list = response.body().getData();
                                        adapter = new myTestAdapter(mContext, getActivity(), list);
                                        adapter.notifyDataSetChanged();*//*

                                    } else {
                                        Utils.showToast(mContext, json.getString("message"));

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //  mProgressDialog.dismiss();
                                pd.dismiss();
                                Toast.makeText(Activity_Testresult.this, "Error, Please try again" + error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        })

                {
                    //This is for Headers If You Needed
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        String creds = String.format("%s:%s", "VIL1XuqUVnkq31L2PdCtjw==", "zxdwT819BSOhT9pmnadjeg==");
                        String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                        params.put("Authorization", auth);
                        return params;
                    }

                    //Pass Your Parameters here
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("UserName", "andrew@gmail.com");
                        System.out.println("Login SendData" + params);
                        return params;

                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(stringRequest);
            }
*/

    }

    private void prepareMovieData() {
        listitem movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        movie = new listitem("Complete Blood Count", "Complete Blood Count", "17 JUNE 2017");
        movieList.add(movie);

        adapter.notifyDataSetChanged();
    }

    /*private void CustomDialogPay() {

        Dialog alertDialog = new Dialog(this);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.activity_pay);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        alertDialog.show();

    }*/
}

