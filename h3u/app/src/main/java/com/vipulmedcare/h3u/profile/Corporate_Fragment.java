package com.vipulmedcare.h3u.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.db.DatabaseHelper;
import com.vipulmedcare.h3u.pojo.CorporateDataNew;
import com.vipulmedcare.h3u.utils.Utils;
import com.vipulmedcare.h3u.utils.Validation;

import java.util.List;

import jp.wasabeef.recyclerview.animators.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.adapters.ScaleInAnimationAdapter;

/**
 * Created by Andrew Pj on 9/5/2017.
 */

public class Corporate_Fragment extends Fragment implements View.OnClickListener {
   Button btn_linkcorporate;
    LinearLayout ll_corporate;
    Boolean isShowing = false;
    private Boolean ll_corporateBoolean = false;
    RecyclerView recyclerView;
    TextView text_add;
    AddMemberAdapter adapter;
   TextView tv_add_member;
    EditText et_corporate_name,et_corporate_email,et_corporate_empid;
    private Context mContext;
    Button btn_add_member,btn_cancel_member;
    private LinearLayout ll_addmemberhide;
    TextInputLayout til_corporate_empid,til_corporate_name,til_corporate_email;
    public static Corporate_Fragment newInstance() {
        Corporate_Fragment fragment = new Corporate_Fragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.link_corporate, container, false);

        mContext = getContext();


        recyclerView = (RecyclerView) rootView.findViewById(R.id.rv_addmember);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);
        recyclerView.setAdapter(adapter);

        btn_cancel_member = (Button) rootView.findViewById(R.id.btn_cancel_member);
        btn_cancel_member.setOnClickListener(this);
        ll_addmemberhide = (LinearLayout) rootView.findViewById(R.id.ll_addmember_hide);
        ll_addmemberhide.setVisibility(View.GONE);

        btn_add_member = (Button)rootView.findViewById(R.id.btn_add_member);
        btn_add_member.setOnClickListener(this);
        tv_add_member = (TextView) rootView.findViewById(R.id.tv_add_member);
        tv_add_member.setOnClickListener(this);

        et_corporate_name= (EditText) rootView.findViewById(R.id.et_corporate_name);
        et_corporate_email= (EditText) rootView.findViewById(R.id.et_corporate_email);
        et_corporate_empid= (EditText) rootView.findViewById(R.id.et_corporate_empid);

        til_corporate_empid = (TextInputLayout) rootView.findViewById(R.id.til_corporate_empid);
        til_corporate_email= (TextInputLayout) rootView.findViewById(R.id.til_corporate_email);
        til_corporate_name= (TextInputLayout) rootView.findViewById(R.id.til_corporate_name);

        // offline save data

        String  select_profile  ="Select * from tb1_corporate";
        DatabaseHelper db=new DatabaseHelper(mContext);
        Cursor cur =  db.getAllRecord(select_profile);
        int count=cur.getCount();
        int totalcount=0;
        if (count>0) {
            if (cur.moveToFirst()) {
                do {
                    cur.getString(0);

                    et_corporate_name.setText(cur.getString(cur.getColumnIndex("CorporateName")));
                    et_corporate_email.setText(cur.getString(cur.getColumnIndex("CorporateEmail")));
                    et_corporate_empid.setText(cur.getString(cur.getColumnIndex("EmpID")));

                    /*String FileName = cur.getString(cur.getColumnIndex("profileimagepath"));
                    String Imgbase64_1 = "";
                    if (FileName.equals("")) {

                    } else {
                        try {
                            Imgbase64_1 = Utils.Createbase64Image("/sdcard/H3u" + "/" + FileName);
                        } catch (Exception e) {
                            Imgbase64_1 = "";
                        }
                    }*/
                }

                while (cur.moveToNext());

            }
        }

        return rootView;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_add_member: {
                toggleShowAddMember();
            }
            break;

            case R.id.btn_add_member: {
                if (checkValidations()) {

                   // String  select_profile  ="Select * from tb1_corporate where Userid='"+Userid +"'";
                    String  select_profile  ="Select * from tb1_corporate ";
                    DatabaseHelper db=new DatabaseHelper(mContext);
                    Cursor cur =  db.getAllRecord(select_profile);
                    int count=cur.getCount();
                    int totalcount=0;
                    if (count>0) {
                        String select_profile1 = "update tb1_corporate set CorporateName ='" + et_corporate_name.getText()+ "',CorporateEmail='" + et_corporate_email.getText() + "',EmpID='" + et_corporate_empid.getText()+"'";
                        Cursor cur1= db.getAllRecord(select_profile1);
                        Utils.showToast(mContext,"successfully ");
                    }
                    else {

                        String select_profile1 = "INSERT INTO tb1_corporate(CorporateName,CorporateEmail,EmpID)values('"+et_corporate_name.getText()+"','"+et_corporate_email.getText()+"','"+et_corporate_empid.getText()+"')";
                        db.InsertRecord(select_profile1);
                        Utils.showToast(mContext,"successfully  insert");

//
//                        adapter = new AddMemberAdapter(mContext, select_profile1, mContext.this);
//                        adapter.notifyDataSetChanged();
//                        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
//                        ScaleInAnimationAdapter scaleAdapter = new ScaleInAnimationAdapter(alphaAdapter);
                    }

                  //  Utils.hideSoftKeyboard(mContext, mView);
                   // saveMember();
                   // AddMemberAdapter();
                }
            }
            break;
            case R.id.btn_cancel_member: {
              //  clearFields();
                toggleShowAddMember();
            }
            break;
        }
    }

    private boolean checkValidations() {

        Boolean result = true;
        if (!Validation.isName(til_corporate_name, et_corporate_name, true)) {
            result = false;
        }
        else if (!Validation.isEmailAddress(til_corporate_email, et_corporate_email, true)) {
            result = false;
        }
        else if (!Validation.isName(til_corporate_empid, et_corporate_empid, true)) {
            result = false;
        }
       /* else if (spin_relation.getSelectedItem().toString().equalsIgnoreCase("Select Relation")) {
            Utils.showErrorAlert(mContext, "Select Relation Type");
            result = false;
        }*/
        return result;
    }


    public class AddMemberAdapter extends RecyclerView.Adapter<AddMemberAdapter.ViewHolder>{
        private static final int UNSELECTED = -1;
        Context mContext;
        List<CorporateDataNew> mlist;
        private int selectedItem = UNSELECTED;
        private Fragment fragmentTarget;
        private int pos;
        private ProgressDialog pd;
        
        public AddMemberAdapter(Context mContext, List<CorporateDataNew> list, Fragment fragmentTarget) {
            this.mContext = mContext;
            this.mlist = list;
            this.fragmentTarget = fragmentTarget;

        }
        @Override
        public AddMemberAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.corporate_view, parent, false);
            pd = new ProgressDialog(mContext);
            pd.setIndeterminate(true);
            pd.setMessage("Please wait...");
            pd.setCancelable(false);

            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            String CoName = mlist.get(position).getCORPORATENAME() + "\n[" + mlist.get(position).getCORPORATENAME() + "]";
            holder.tv_corporatename.setText(CoName);

            String CoEmail = mlist.get(position).getCORPORATEEMAIL() + "\n[" + mlist.get(position).getCORPORATEEMAIL() + "]";
            holder.tv_corporateemail.setText(CoEmail);

            String CoEMPID= mlist.get(position).getEMPID() + "\n[" + mlist.get(position).getEMPID() + "]";
            holder.tv_empid.setText(CoEMPID);

            final String CorporateName = mlist.get(position).getCORPORATENAME();
            final String CorporateEmail = mlist.get(position).getCORPORATEEMAIL();
            final String EMPId = mlist.get(position).getEMPID();

        }

        @Override
        public int getItemCount() {
            return 0;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private TextView tv_corporatename,tv_corporateemail,tv_empid;
            private int position;

            public ViewHolder(View itemView) {
                super(itemView);

                tv_corporatename= (TextView) itemView.findViewById(R.id.tv_corporatename);
                tv_corporateemail= (TextView) itemView.findViewById(R.id.tv_corporateemail);
                tv_empid= (TextView) itemView.findViewById(R.id.tv_empid);

            }
        }
    }

    //.................................
    private void toggleShowAddMember() {
        if (isShowing) {
            ll_addmemberhide.setVisibility(View.GONE);
           // ll_btn_continue.setVisibility(View.VISIBLE);
            isShowing = false;
        } else {
//        clearFields();
            ll_addmemberhide.setVisibility(View.VISIBLE);
          //  ll_btn_continue.setVisibility(View.GONE);
//            focusOnView(scroll_document);
            isShowing = true;
        }

    }
}
