package com.vipulmedcare.h3u.ui;

public class myListItem implements Listable {

    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getLabel() {
        return name;
    }
}
