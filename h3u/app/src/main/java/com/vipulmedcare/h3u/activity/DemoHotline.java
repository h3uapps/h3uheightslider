package com.vipulmedcare.h3u.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.freshdesk.hotline.Hotline;
import com.freshdesk.hotline.HotlineConfig;
import com.freshdesk.hotline.HotlineUser;
import com.vipulmedcare.h3u.R;

/**
 * Created by vipul on 9/12/2017.
 */

public class DemoHotline  extends AppCompatActivity {

    private Hotline hotlineInstance;
    private Button btnShowConversations, btnShowFAQs;
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 4329;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);

        //init
        HotlineConfig hotlineConfig=new HotlineConfig("e7d225f1-91ba-465f-a596-4483feb57a5a","97867475-f25d-4ceb-91a7-ac6c8e5481c6");
        Hotline.getInstance(getApplicationContext()).init(hotlineConfig);

        //Update user information
        HotlineUser user = Hotline.getInstance(getApplicationContext()).getUser();
        user.setName("John Doe").setEmail("john@john.doe").setPhone("001", "2542542544");
        Hotline.getInstance(getApplicationContext()).updateUser(user);

        btnShowFAQs = (Button) findViewById(R.id.btnShowFAQs);
        btnShowConversations = (Button) findViewById(R.id.btnShowConversations);
        btnShowFAQs.setOnClickListener(viewClickListener);
        btnShowConversations.setOnClickListener(viewClickListener);


    }

    View.OnClickListener viewClickListener = new View.OnClickListener() {
        @Override
        public void onClick (View v) {
            if(v.getId() == R.id.btnShowFAQs) {

                Hotline.showFAQs(DemoHotline.this);

            } else if(v.getId() == R.id.btnShowConversations) {

                Hotline.showConversations(DemoHotline.this);

            }
        }
    };

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */


}
