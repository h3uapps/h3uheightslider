package com.vipulmedcare.h3u.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;


import com.vipulmedcare.h3u.R;

import java.util.List;

/**
 * Created by vipul on 8/24/2017.
 */

public class HealthHorizonatalAdapter  extends RecyclerView.Adapter<HealthHorizonatalAdapter.MyViewHolder> {

    private List<String> horizontalList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtView;
        public ImageButton cross_ver_rv;


        public MyViewHolder(View view) {
            super(view);
            txtView = (TextView) view.findViewById(R.id.tv_ver_rv);
            cross_ver_rv = (ImageButton) view.findViewById(R.id.cross_ver_rv);

        }
    }


    public HealthHorizonatalAdapter(List<String> horizontalList) {
        this.horizontalList = horizontalList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.health_map_hor_reclr_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.txtView.setText(horizontalList.get(position));

        holder.txtView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }
}
