package com.vipulmedcare.h3u.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.vipulmedcare.h3u.fragment.FirstOPDCard;
import com.vipulmedcare.h3u.fragment.SecondOPDCard;
import com.vipulmedcare.h3u.fragment.ThirdOPDCard;


/**
 * Created by vipul on 9/1/2017.
 */

public class OPDCardsPagerAdapter   extends FragmentPagerAdapter {
    final int PAGE_COUNT = 3;
    private String tabTitles[] = new String[] { "CORPORATE", "PERSONAL","PERSONAL"};
    private Context context;

    public OPDCardsPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                // Top Rated fragment activity
                return new FirstOPDCard();
            case 1:
                // Games fragment activity
                return new SecondOPDCard();
            case 2:
                // Games fragment activity
                return new ThirdOPDCard();

        }

        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return PAGE_COUNT;
    }

    public CharSequence getPageTitle(int position) {
        // Generate title based on item position

        return tabTitles[position];
    }

}
