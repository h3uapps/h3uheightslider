package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.freshdesk.hotline.Hotline;
import com.freshdesk.hotline.HotlineConfig;
import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.registration.Activity_Signup;
import com.vipulmedcare.h3u.utils.PreferenceKeys;
import com.vipulmedcare.h3u.utils.Utils;


public class SplashScreenActivity extends AppCompatActivity {

    private static final int SPLASH_TIME_OUT = 1000; // time out for the splash in ms
    private Handler mHandlerSplash;
    private Context mContext;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        HotlineConfig hlConfig=new HotlineConfig("e7d225f1-91ba-465f-a596-4483feb57a5a","97867475-f25d-4ceb-91a7-ac6c8e5481c6");
        Hotline.getInstance(getApplicationContext()).init(hlConfig);

        mContext = this;

        mHandlerSplash = new Handler();
        mHandlerSplash.postDelayed(new Runnable() {

            @Override
            public void run() {

               // Utils.addPreferenceBoolean(mContext, PreferenceKeys.IsPromocode, false);
                if (Utils.getPreferenceBoolean(mContext, PreferenceKeys.UserRegistered, false)) {
                    Intent mIntent;
                    mIntent = new Intent(mContext, Activity_Signup.class);
                    startActivity(mIntent);
                    finish();

                }else {
                    Intent mIntent;
                    mIntent = new Intent(mContext, Activity_Signup.class);
                    startActivity(mIntent);
                    finish();

                }

            }
        }, SPLASH_TIME_OUT);
        if (checkPlayServices()) {
           /* Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);*/
        }
    }

    // update code app play store
    private boolean checkPlayServices() {
       /* GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if(result != ConnectionResult.SUCCESS) {
            if(googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            return false;
        }*/
        return true;
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();

        if (mHandlerSplash != null) {
            // Stops the handler
            mHandlerSplash.removeCallbacksAndMessages(null);
            finish();
        }
    }

    /*@Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }*/
}