package com.vipulmedcare.h3u.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.activity.Provider_detail_Activity;
import com.vipulmedcare.h3u.pojo.BmiResult;
import com.vipulmedcare.h3u.view.text.CustomEditText;

import java.util.List;

/**
 * Created by vipul on 9/9/2017.
 */

public class BmiResultAdapter  extends RecyclerView.Adapter<BmiResultAdapter.MyViewHolder> {

    private List<BmiResult> BmiHistory;
    private Context mContext;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private CustomEditText rv_bmi_height, rv_bmi_weight, rv_bmi;

        public MyViewHolder(View view) {
            super(view);
            rv_bmi_height= (CustomEditText) view.findViewById(R.id.rv_bmi_height);
            rv_bmi_weight= (CustomEditText) view.findViewById(R.id.rv_bmi_weight);
            rv_bmi= (CustomEditText) view.findViewById(R.id.rv_bmi);
        }
    }


    public BmiResultAdapter(List<BmiResult> BmiHistory, Context mContext) {
        this.BmiHistory = BmiHistory;
        this.mContext=mContext;
    }

    @Override
    public BmiResultAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bmi_result, parent, false);

        return new BmiResultAdapter.MyViewHolder(itemView);
    }




    @Override
    public void onBindViewHolder(final BmiResultAdapter.MyViewHolder holder, final int position) {
        holder.rv_bmi_height.setText(BmiHistory.get(position).getHeight().toString());
        holder.rv_bmi_weight.setText(BmiHistory.get(position).getWeight().toString());
        holder.rv_bmi.setText(BmiHistory.get(position).getBmi().toString());




    }

    @Override
    public int getItemCount() {
        return BmiHistory.size();
    }
}