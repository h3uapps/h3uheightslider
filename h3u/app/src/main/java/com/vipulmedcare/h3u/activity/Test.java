package com.vipulmedcare.h3u.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.utils.Utils;

import java.text.DateFormatSymbols;
import java.util.Calendar;

/**
 * Created by vipul on 9/8/2017.
 */

public class Test extends Activity {
    EditText buttn_click;
    private Context mContext;
    private int cStartMonth, cStartDay, cStartYear;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.text);

        mContext = Test.this;
        buttn_click= (EditText) findViewById(R.id.buttn_click);

        buttn_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();

                cStartYear = c.get(Calendar.YEAR);
                cStartMonth = c.get(Calendar.MONTH);
                cStartDay = c.get(Calendar.DAY_OF_MONTH);

                int mStartYear = cStartYear - 18;
                int mStartMonth = cStartMonth;
                int mStartDay = cStartDay;
                Calendar calendar = Calendar.getInstance();
                calendar.set(mStartYear, mStartMonth, mStartDay, 0, 0, 0);

                //For Maximum Date Picker Logic
                int maxStartYear = cStartYear - 18;
                int maxStartMonth = cStartMonth;
                int maxStartDay = cStartDay;
                Calendar calendar1 = Calendar.getInstance();
                calendar1.set(maxStartYear, maxStartMonth, maxStartDay, 0, 0, 0);
                DatePickerDialog _date;
//                _date = new DatePickerDialog(mContext, datePickerListener, cStartYear, cStartMonth, cStartDay);

                _date = new DatePickerDialog(mContext, datePickerListener, maxStartYear, maxStartMonth, maxStartDay);
                _date.getDatePicker().setMaxDate(calendar1.getTimeInMillis());
                _date.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            //et_to_date.setText("");
                            buttn_click.setText("");
                        }
                    }
                });
                _date.show();
            }
        });


    }

    private final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            buttn_click.setText(new StringBuilder().append(Utils.checkDigit(selectedDay)).append("-")
                    .append(new DateFormatSymbols().getShortMonths()[selectedMonth]).append("-").append(selectedYear));
        }
    };
}
