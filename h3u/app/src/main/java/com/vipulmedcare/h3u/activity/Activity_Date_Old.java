package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.utils.Utils;

/**
 * Created by vipul on 8/4/2017.
 */

public class Activity_Date_Old extends BaseActivity implements View.OnClickListener {
    Button btn_next;
    TextView tv_back;
    private Context mContext;
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_date_old);
        mContext = Activity_Date_Old.this;

        initviews();

    }

    private void initviews() {

        tv_back= (TextView)findViewById(R.id.tv_back);
        btn_next= (Button) findViewById(R.id.btn_next);
        btn_next.setOnClickListener(this);
        tv_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
               /* Intent I = new Intent(mContext,Activity_Date_Old.class);
                startActivity(I);*/
                Utils.showToast(mContext," Work In Progress");

                break;
            case R.id.tv_back:
                finish();
                break;
        }
    }
}