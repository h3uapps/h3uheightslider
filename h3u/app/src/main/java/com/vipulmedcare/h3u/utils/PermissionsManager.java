package com.vipulmedcare.h3u.utils;

import android.Manifest;
import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.base.AppContext;


/**
 * Created by vjangra on 14/08/16.
 *
 *
 * How to use it
 *

 PermissionRequest request = PermissionsManager.WRITE_STORAGE_REQUEST;
 request.title = getString(R.string.perm_phone_state_title);
 request.message = getString(R.string.perm_phone_state_desc);
 request.drawable = R.drawable.perm_write_state;

 if(PermissionsManager.checkPermissionIfAllowed(this,request,true)){
        //Do your stuff here, if permissions all ready allowed
        // last param, true - is if you want to show explanation dialog before asking.
 }

 // Define these two function in to your base class, so that common functionality will
 // be inherited in to all classes and overide the onPermissionResponse function
 @Override
 public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
     switch (requestCode) {
         case PermissionsManager.MULTI_PERMISSIONS_REQUEST_CODE:

         break;
         default:
            onPermissionResponse(requestCode, (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED));
     }
     return;
 }

 protected void onPermissionResponse(int requestCode, boolean denied){
     switch (requestCode) {
         case PERMISSIONS_REQUEST_CODE:
         break;

    }
}

 */
public class PermissionsManager {

    private static PermissionsManager manager = new PermissionsManager();

    public static PermissionsManager getInstance() {
        return manager;
    }

    private PermissionsManager() {}

    private static AlertDialog dialog;

    public static boolean checkPermissionIfAllowed(Activity activity, PermissionRequest request, boolean showExplanationBeforeRequest) {
        if(Build.VERSION.SDK_INT >= 23) {
            String[] list = request.getDeniedPermissionsList(activity);
            boolean isAllowed = list.length == 0;
            if(!isAllowed){
                if(showExplanationBeforeRequest){
                    showRequiredPermissionExplanationDialog(activity,request,false);
                }else{
                    requestPermission(activity,request);
                }
            }
            return isAllowed;
        }else{
            return true;
        }
    }

    private static void requestPermission(Activity activity, PermissionRequest request) {
        String[] list = request.getDeniedPermissionsList(activity);
        ActivityCompat.requestPermissions(activity, list ,request.requestCode);
    }

    public static void showRequiredPermissionExplanationDialog(final Activity activity, final PermissionRequest request, final boolean isDenied) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();
        final View view = inflater.inflate(R.layout.permission_explanation_dialog, null);
        dialogBuilder.setView(view);

        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(request.title);

        TextView desc = (TextView) view.findViewById(R.id.desc);
        desc.setText(request.message);

        ImageView header = (ImageView) view.findViewById(R.id.header);
        header.setImageDrawable(request.drawable);

        TextView warnMsg = (TextView) view.findViewById(R.id.warnMsg);
        TextView noteMsg = (TextView) view.findViewById(R.id.noteMsg);
        if(isDenied){
            warnMsg.setVisibility(View.VISIBLE);
            noteMsg.setVisibility(View.VISIBLE);
        }else{
            warnMsg.setVisibility(View.GONE);
            noteMsg.setVisibility(View.GONE);
        }

        dialog = dialogBuilder.create();
        dialog.setCancelable(false);

        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;


        LinearLayout btnOk = (LinearLayout) view.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestPermission(activity,request);
                if(dialog != null){
                    dialog.dismiss();
                    dialog = null;
                }
            }
        });

        //dialog.show();
        //this will delayed the call 2 seconds, so user can see, the dialog animation
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.show();

                Rect rect = new Rect();
                Window window = activity.getWindow();
                window.getDecorView().getWindowVisibleDisplayFrame(rect);

                dialog.getWindow().setLayout(Math.round(rect.width() * 0.9f), Math.round(rect.height() * 0.7f));

                if(isDenied){
                    ScrollView scrollView = (ScrollView) view.findViewById(R.id.scrollView);
                    scrollView.fullScroll(View.FOCUS_DOWN);
                }
            }
        },1000);
    }







    static private Resources getResources(){
        return AppContext.getContext().getResources();
    }

    static final private int INITIAL_REQUEST = 111; // any number to start requests numbering

    static final public int MULTI_PERMISSIONS_REQUEST_CODE = INITIAL_REQUEST;

    static final public int GET_ACCOUNTS_REQUEST_CODE = INITIAL_REQUEST + 1;
    static final public int FINE_LOCATION_REQUEST_CODE = INITIAL_REQUEST + 2;
    static final public int COARSE_LOCATION_REQUEST_CODE = INITIAL_REQUEST + 3;
    static final public int WRITE_STORAGE_REQUEST_CODE = INITIAL_REQUEST + 4;
    static final public int PHONE_STATE_REQUEST_CODE = INITIAL_REQUEST + 5;

    static final public PermissionRequest PHONE_STATE_REQUEST = new PermissionRequest(
            PHONE_STATE_REQUEST_CODE, Manifest.permission.READ_PHONE_STATE,
            getResources().getString(R.string.perm_phone_state_title),
            getResources().getString(R.string.perm_phone_state_desc),
            getResources().getDrawable(R.drawable.perm_phone_state));

    static final public PermissionRequest WRITE_STORAGE_REQUEST = new PermissionRequest(
            WRITE_STORAGE_REQUEST_CODE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
            getResources().getString(R.string.perm_write_storage_title),
            getResources().getString(R.string.perm_write_starage_desc),
            getResources().getDrawable(R.drawable.perm_write_state));

    static final public PermissionRequest GET_ACCOUNTS_REQUEST = new PermissionRequest(GET_ACCOUNTS_REQUEST_CODE, Manifest.permission.GET_ACCOUNTS);
    static final public PermissionRequest FINE_LOCATION_REQUEST = new PermissionRequest(FINE_LOCATION_REQUEST_CODE, Manifest.permission.ACCESS_FINE_LOCATION);
    static final public PermissionRequest COARSE_LOCATION_REQUEST = new PermissionRequest(COARSE_LOCATION_REQUEST_CODE, Manifest.permission.ACCESS_COARSE_LOCATION);


}
