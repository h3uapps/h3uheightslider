package com.vipulmedcare.h3u.base;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;

import java.io.File;

/**
 * Created by  on 31/12/15.
 */
public class AppContext {

    private static AppContext mInstance;

    private Context context;

    static final private String PREFS = "H3U";

    public static AppContext getInstance() {
        if (mInstance == null) mInstance = getSync();
        return mInstance;
    }

    private static synchronized AppContext getSync() {
        if (mInstance == null) mInstance = new AppContext();
        return mInstance;
    }

    public void initialize(Context context) {
        this.context = context;
    }

    public static Context getContext() {
        return getInstance().context;
    }

    public String getPackageName(){
        return context.getPackageName();
    }

    public SharedPreferences getSharedPrefs() {
        return getContext().getSharedPreferences(PREFS, Context.MODE_PRIVATE);
    }

    private Activity activity;

    public void setCurrentActivity(Activity activity){
        this.activity = activity;
    }

    public Activity getCurrentActivity(){
        return this.activity;
    }

    public String getStorageDirectory(){
        String path = "/offline/";
        if(isExternalStorageWritable()) {
            path = "/sdcard/Android/data/" + getPackageName() + path;
            //return context.getExternalFilesDir(null) + "/";
        }
        File file = new File(path);
        if(!file.exists()) file.mkdir();

        return path;
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

}