package com.vipulmedcare.h3u.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.Scroller;
import android.widget.TextView;

import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.utils.Utils;
import com.vipulmedcare.h3u.view.text.CustomTextView;

import java.util.List;

import static com.vipulmedcare.h3u.R.id.view;

/**
 * Created by vipul on 9/13/2017.
 */

public class HeightSliderAdapter   extends RecyclerView.Adapter<HeightSliderAdapter.MyViewHolder> {


    public interface OnItemClickListener {
        void onItemClick(String item);
    }
    private List<String> horizontalList;
    int ACTUAL_SIZE_OF_LIST;
    int width;
    Context mContext;
    private final OnItemClickListener listener;

    public HeightSliderAdapter(List<String> horizontalList,  Context mContext,OnItemClickListener listener) {
        this.horizontalList = horizontalList;
        this.width = width;
        this.mContext = mContext;
        ACTUAL_SIZE_OF_LIST =  horizontalList.size();
        this.listener = listener;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.height_slider_rv, parent, false);

        return new HeightSliderAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
//        holder.bind(horizontalList.get(position), listener);
        position = position % ACTUAL_SIZE_OF_LIST;
        holder.tv_ht.setText(horizontalList.get(position).toString());


    }

    @Override
    public int getItemCount() {
        //return horizontalList.size();
        return Integer.MAX_VALUE;
}


    public class MyViewHolder extends RecyclerView.ViewHolder {
       private CustomTextView tv_ht;


        public MyViewHolder(View view) {
            super(view);
            tv_ht = (CustomTextView) view.findViewById(R.id.tv_ht);


        }
        public void bind(final String item, final OnItemClickListener listener) {
            tv_ht.setText(item);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }


}

}
