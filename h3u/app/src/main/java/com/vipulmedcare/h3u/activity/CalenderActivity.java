package com.vipulmedcare.h3u.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


import com.imanoweb.calendarview.CalendarListener;
import com.imanoweb.calendarview.CustomCalendarView;
import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.adapter.CalenderHorizonatalAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by vipul on 8/24/2017.
 */

public class CalenderActivity extends AppCompatActivity implements View.OnClickListener{
   private CustomCalendarView calendarView;

    private List<String> list;
    private RecyclerView CalenderHorizontalRV;
    private CalenderHorizonatalAdapter calenderhorizontalAdapter;
    private ImageButton cal_arrow_back,  cal_cross_btn, cal_chat;
    private Context mContext;
    private TextView tv_Calender_note;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calender);
        mContext =CalenderActivity.this;


        list=new ArrayList<String>();


        list.add("9:00");
        list.add("10:00");
        list.add("11:00");
        list.add("12:00");
        list.add("13:00");
        list.add("14:00");
        list.add("15:00");
        list.add("16:00");
        list.add("17:00");
        list.add("18:00");

        CalenderHorizontalRV = (RecyclerView) findViewById(R.id.CalenderHorizontalRV);
        calenderhorizontalAdapter=new CalenderHorizonatalAdapter(list);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(CalenderActivity.this, LinearLayoutManager.HORIZONTAL, false);
        CalenderHorizontalRV.setLayoutManager(horizontalLayoutManagaer);
        CalenderHorizontalRV.setAdapter(calenderhorizontalAdapter);




        //Initialize CustomCalendarView from layout
        calendarView = (CustomCalendarView) findViewById(R.id.calendar_view);

        //Initialize calendar with date
        Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());

        //Show monday as first date of week
        calendarView.setFirstDayOfWeek(Calendar.MONDAY);

        //Show/hide overflow days of a month
        calendarView.setShowOverflowDate(false);

        //call refreshCalendar to update calendar the view
        calendarView.refreshCalendar(currentCalendar);

        //Handling custom calendar events
        calendarView.setCalendarListener(new CalendarListener() {
            @Override
            public void onDateSelected(Date date) {
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                Toast.makeText(CalenderActivity.this, df.format(date), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onMonthChanged(Date date) {
                SimpleDateFormat df = new SimpleDateFormat("MM-yyyy");
                Toast.makeText(CalenderActivity.this, df.format(date), Toast.LENGTH_SHORT).show();
            }
        });


        initViews();

    }
    public void initViews(){
        cal_arrow_back = (ImageButton) findViewById(R.id.cal_arrow_back);
                cal_cross_btn = (ImageButton) findViewById(R.id.cal_cross_btn);
        cal_chat = (ImageButton) findViewById(R.id.cal_chat);
        cal_arrow_back.setOnClickListener(this);
        cal_cross_btn.setOnClickListener(this);
                cal_chat.setOnClickListener(this);

        tv_Calender_note= (TextView) findViewById(R.id.tv_Calender_note);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cal_arrow_back:
                Intent intent1 = new Intent(mContext,HealthProviderDetail.class );
                startActivity(intent1);
                break;

            case R.id.cal_cross_btn:
                finish();
                break;
            case R.id.cal_chat:






                break;
        }
    }
}
