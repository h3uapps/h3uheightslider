package com.vipulmedcare.h3u.pojo;

/**
 * Created by vipul on 9/9/2017.
 */

public class BmiResult {

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getBmi() {
        return Bmi;
    }

    public void setBmi(String bmi) {
        Bmi = bmi;
    }

    private String Height;
    private  String Weight;
    private String Bmi;


    public BmiResult(String Height, String Weight, String Bmi) {

        this.Height = Height;
        this.Weight = Weight;
        this.Bmi = Bmi;
    }
    
}
