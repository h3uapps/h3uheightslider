package com.vipulmedcare.h3u.view.text;

/**
 * Created by vjangra on 04/10/15.
 */

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.vipulmedcare.h3u.model.Fonts;


public class CustomTextViewItalic extends TextView {
    public CustomTextViewItalic(Context context) {
        super(context);
        setFont();
    }

    public CustomTextViewItalic(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public CustomTextViewItalic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    protected void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), Fonts.TYPO_ITALIC);
        setTypeface(font, Typeface.ITALIC);
    }


}
