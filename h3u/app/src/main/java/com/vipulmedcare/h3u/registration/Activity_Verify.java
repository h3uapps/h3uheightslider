package com.vipulmedcare.h3u.registration;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.vipulmedcare.h3u.R;
import com.vipulmedcare.h3u.activity.Activity_Deshboard;
import com.vipulmedcare.h3u.base.BaseActivity;
import com.vipulmedcare.h3u.utils.ApiConstants;
import com.vipulmedcare.h3u.utils.NetworkUtil;
import com.vipulmedcare.h3u.utils.Utils;
import com.vipulmedcare.h3u.utils.Validation;
import com.vipulmedcare.h3u.view.text.CustomEditText;
import com.vipulmedcare.h3u.view.text.CustomTextView;
import com.vipulmedcare.h3u.view.text.CustomTextViewMed;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andrew pj on 8/3/2017.
 */

public class Activity_Verify extends BaseActivity {
    Button btn_verify;
    private Context mContext;
    EditText edit_otp;
    TextView tv_resend;
    TextInputLayout til_otp;
    TextView tv_pheading,tv_wheading,til_first,tv_icase,tv_back;
    ProgressDialog mProgressDialog;
    String otp_code="";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);

        mContext = Activity_Verify.this;
        initviews();

    }

    private void initviews() {

        otp_code= getIntent().getStringExtra("otp");
        Log.e("", "" + otp_code);

        tv_back= (TextView) findViewById(R.id.tv_back);
        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_pheading= (CustomTextViewMed) findViewById(R.id.tv_pheading);
       /* Typeface m1= Typeface.createFromAsset(getAssets(),"fonts/HelveticaLTStd-Bold.otf");
        tv_pheading.setTypeface(m1);*/

        tv_wheading=(CustomTextView) findViewById(R.id.tv_wheading);
        Typeface m2= Typeface.createFromAsset(getAssets(),"fonts/HelveticaLTStd-Light.otf");
        tv_wheading.setTypeface(m2);

        til_first= (CustomTextView) findViewById(R.id.til_first);

       /* Typeface m3= Typeface.createFromAsset(getAssets(),"fonts/HelveticaLTStd-Light.otf");
        til_first.setTypeface(m3);*/

        til_otp= (TextInputLayout) findViewById(R.id.til_otp);

        edit_otp= (CustomEditText) findViewById(R.id.edit_otp);
      /*  Typeface m4= Typeface.createFromAsset(getAssets(),"fonts/HelveticaLTStd-Bold.otf");
        edit_otp.setTypeface(m4);*/

        tv_icase= (CustomTextView) findViewById(R.id.tv_icase);
       /* Typeface m5= Typeface.createFromAsset(getAssets(),"fonts/HelveticaLTStd-Light.otf");
        tv_icase.setTypeface(m5);*/

      /*  Typeface m6= Typeface.createFromAsset(getAssets(),"fonts/HelveticaLTStd-Bold.otf");
        tv_resend.setTypeface(m6);*/
        tv_resend= (CustomTextView) findViewById(R.id.tv_resend);



        edit_otp.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                Validation.isName(til_otp, edit_otp, false);
            }
        });



        btn_verify= (Button) findViewById(R.id.btn_verify);
        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                 //   verificationApi();
                    Utils.showToast(mContext,"Verify Successfully");
                    //Utils.showToast(mContext,"Deshbord");
                    Intent I = new Intent(mContext,Activity_Deshboard.class);
                    startActivity(I);
                }

             /*   edit_otp.getText().toString();

                Log.e("", "" + edit_otp.getText().toString());

                if (edit_otp.getText().toString().equals("")) {
                    til_otp.setError("OTP required");
                }
                else if (edit_otp.getText().toString().equals(otp_code)) {
                    Intent i = new Intent(Activity_Verify.this, Activity_Deshboard.class);
                    startActivity(i);
                    finish();

                } else {
                    til_otp.setError("OTP not matched");
                   // Toast.makeText(Activity_Verify.this, "OTP not matched", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

        tv_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showAlert(mContext, "Work in progress", "Work in progress");
            }
        });
    }

    private void verificationApi() {

        mProgressDialog = new ProgressDialog(Activity_Verify.this,
                AlertDialog.THEME_HOLO_LIGHT);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        showProgressDialog("Please wait...");

        String otp = edit_otp.getText().toString().trim();


        if (NetworkUtil.isNetworkAvailable(mContext)) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.APP_usewisecard,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            System.out.println(response);
                            //   Log.d("Response volley", response);
                            Toast.makeText(Activity_Verify.this,response,Toast.LENGTH_LONG).show();
                            mProgressDialog.dismiss();
                            try {
                                JSONObject json = new JSONObject(response);
                                System.out.println(response);

                                if (json.getInt("success") == 1) {
                                    JSONArray pm = json.getJSONArray("data");
                                    for (int i = 0; i < pm.length(); i++) {
                                    }
                                    // String FirstName = json.getJSONObject("data").getString("FirstName");

                                    Intent I = new Intent(mContext,Activity_Deshboard.class);
                                    startActivity(I);
                                  //  Toast.makeText(Activity_Verify.this, "Verification Successful!", Toast.LENGTH_LONG).show();

                                } else {
                                    Toast.makeText(Activity_Verify.this, "Invalid  Otp", Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //  mProgressDialog.dismiss();
                            Toast.makeText(Activity_Verify.this, "Error, Please try again"+error.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    String creds = String.format("%s:%s","VIL1XuqUVnkq31L2PdCtjw==","zxdwT819BSOhT9pmnadjeg==");
                    String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                    params.put("Authorization",auth);

                    return params;
                }
                //Pass Your Parameters here
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();;

                    params.put("OTP",edit_otp.getText().toString().trim());

                    System.out.println("Verification SendData" + params);
                    return params;


                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }}


    private boolean checkValidation() {
        boolean result = true;

        if (!Validation.isNull(til_otp, edit_otp, true)) result = false;
        return result;
    }
}
